package com.volive.osha.helperclasses;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;
import com.volive.osha.R;

import java.util.regex.Matcher;
import java.util.regex.Pattern;



public class Validation_Class {


    Context context;


    public Validation_Class(Context context) {
        this.context = context;
    }


    public static boolean validateEmail(String email) {
        Pattern pattern;
        Matcher matcher;
        String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }


    public boolean checkInternetConnection(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();

        if (activeNetworkInfo == null || !activeNetworkInfo.isConnected()) {
            return false;
        }
        return true;
    }
    public void snackBar(String msg, View v) {
        Snackbar.make(v, msg, Snackbar.LENGTH_LONG).setAction("Action", null).show();
    }
    public void displaySnackbar(View view, String stng) {
       // Snackbar.make(view, stng, Snackbar.LENGTH_LONG).setAction("Action", null).show();
        Snackbar snack = Snackbar.make(view, stng, Snackbar.LENGTH_SHORT);
        View snackView = snack.getView();
        snackView.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
        TextView snackViewText = (TextView) snackView.findViewById(android.support.design.R.id.snackbar_text);
        snackViewText.setTextColor(context.getResources().getColor(R.color.white));
        snackViewText.setTextSize(15f);
        snackViewText.setGravity(snackView.TEXT_ALIGNMENT_CENTER);
       // snackViewText.setTextAlignment(snackView.TEXT_ALIGNMENT_CENTER);

        snack.show();
    }

    public void hideSoftKeyboard(Activity activity) {
        try {
            InputMethodManager inputMethodManager =
                    (InputMethodManager) activity.getSystemService(
                            Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(
                    activity.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    //freelancer-a5713
    // Show alert dialog


    public void showToast(Context context, String msg) {
        Toast toast = Toast.makeText(context, msg, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }
    public void loadImage(boolean isPicasso, Context context, String url, ImageView imageView)
    {
        try {
            if (isPicasso) {
                Picasso.get()
                        .load(url)
                        .placeholder(R.drawable.profile_image)
                        .error(R.drawable.profile_image)
                        .into(imageView);
            } else {
                try {
                    Glide.with(context)
                            .load(url)
                            .into(imageView);
                } catch (Exception e) {
                    e.printStackTrace();
                    Glide.with(context)
                            .load("file://" + url)
                            .into(imageView);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}