package com.volive.osha.helperclasses;


import android.annotation.SuppressLint;

@SuppressLint("Registered")
public class Offer_status {

    public static boolean isActivityVisible() {
        return activityVisible;
    }

    public static void activityResumed() {
        activityVisible = true;
    }

    public static void activityPaused() {
        activityVisible = false;
    }

    private static boolean activityVisible;
}


