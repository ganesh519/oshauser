package com.volive.osha.helperclasses;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class HelperClass {

    Context context;

    public HelperClass(Context context) {
        this.context = context;
    }

    public boolean checkInternetConnection(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();

        if (activeNetworkInfo == null || !activeNetworkInfo.isConnected()) {
            return false;
        }
        return true;
    }

    public boolean validateEmail(String email) {
        Pattern pattern;
        Matcher matcher;
        String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

//    // Show alert dialog
//    public void showAlertDialog(Context context, int type, String title, String message, String confirmText, SweetAlertDialog.OnSweetClickListener listener) {
//        try {
//            new SweetAlertDialog(context, type)
//                    .setTitleText(title)
//                    .setContentText(URLDecoder.decode(message))
//                    .setConfirmText(confirmText)
//                    .showCancelButton(false)
//                    .setConfirmClickListener(listener)
//                    .show();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    public void showProgress(Context context) {
//        try {
//            if (progressDialog != null)
//                progressDialog.dismiss();
//            progressDialog = null;
//            progressDialog = new SweetAlertDialog(context, SweetAlertDialog.PROGRESS_TYPE);
//            progressDialog.getProgressHelper().setBarColor(Color.parseColor("#FC4041"));
//            progressDialog.setTitleText(context.getResources().getString(R.string.loading));
//            progressDialog.setContentText(context.getResources().getString(R.string.please_wait));
//            progressDialog.setCancelable(false);
//            progressDialog.show();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    public void hideProgress() {
//        try {
//            if (progressDialog != null && progressDialog.isShowing())
//                progressDialog.dismiss();
//        } catch (Exception e) {
//            e.printStackTrace();
//            if (progressDialog != null && progressDialog.getProgressHelper().isSpinning())
//                progressDialog.getProgressHelper().stopSpinning();
//        }
//    }
//
//    public void showToast(Context context, String msg) {
//        Toast toast = Toast.makeText(context, msg, Toast.LENGTH_SHORT);
//        toast.setGravity(Gravity.CENTER, 0, 0);
//        toast.show();
//    }
//
//    public void showErrorDialog(Context context, String message) {
//        try {
//            hideProgress();
//            showAlertDialog(context, SweetAlertDialog.ERROR_TYPE, "", URLDecoder.decode(message), context.getResources().getString(R.string.ok), new SweetAlertDialog.OnSweetClickListener() {
//                @Override
//                public void onClick(SweetAlertDialog sweetAlertDialog) {
//                    sweetAlertDialog.dismissWithAnimation();
//                }
//            });
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    public void loadImage(boolean isPicasso, Context context, String url, ImageView imageView) {
//        try {
//            if (isPicasso) {
//                Picasso.get()
//                        .load(url)
//                        .placeholder(android.R.drawable.progress_indeterminate_horizontal)
//                        .error(R.drawable.image)
//                        .into(imageView);
//            } else {
//                try {
//                    Glide.with(context)
//                            .load(url)
//                            .into(imageView);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                    Glide.with(context)
//                            .load("file://" + url)
//                            .into(imageView);
//                }
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    public void showError(Context context, ANError anError) {
//        try {
//            JSONObject errorObject = new JSONObject();
//            try {
//                errorObject = new JSONObject(anError.getErrorBody());
//            } catch (Exception e) {
//                errorObject = new JSONObject(anError.getErrorDetail());
//            }
//            String errorMsg = errorObject.getString("message");
//            showErrorDialog(context, errorMsg);
//        } catch (Exception e) {
//            hideProgress();
//            showErrorDialog(context, context.getResources().getString(R.string.server_error));
//            e.printStackTrace();
//        }
//    }
//
//    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
//    public void showImageAlert(Context context, String imageUri) {
//        try {
//            if (imageUri != null) {
//                this.context = context;
//                Dialog dialog = new Dialog(context, android.R.style.Theme_DeviceDefault_NoActionBar);
//                dialog.setCancelable(true);
//                dialog.getWindow().setBackgroundDrawable(
//                        new ColorDrawable(Color.TRANSPARENT));
//                final PhotoView imageView = new PhotoView(context);
//                imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
//                imageView.setCropToPadding(true);
//                loadImage(false, context, String.valueOf(imageUri), imageView);
//                dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
//                dialog.setContentView(imageView);
//                dialog.show();
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            hideProgress();
//        }
//    }
//
//    // Get Date & Time as required format from CurrentTimeMillisec
//    public String setDateAndTime(Context context, String timemiles) {
//        if (timemiles == null) {
//            return null;
//        } else {
//            Date d2 = new Date();
//            Date d1 = new Date(Long.valueOf(timemiles));
//
//            long diffInDays = d2.getDate() - d1.getDate();
//
//            if (diffInDays >= 2 || diffInDays < 0) {
//                String date = d1.getDate() + "/" + (d1.getMonth() + 1) + "/" + (d1.getYear() + 1900);
//                try {
//                    SimpleDateFormat inFormat = new SimpleDateFormat("dd/MM/yyyy");
//                    SimpleDateFormat outFormat = new SimpleDateFormat("dd MMM yyyy");
//                    String dateFormat = outFormat.format(inFormat.parse(date));
//                    return dateFormat;
//                } catch (Exception e) {
//                    e.printStackTrace();
//                    return date;
//                }
//            } else if (diffInDays == 1)
//                return context.getResources().getString(R.string.yesterday);
//            else
//                return d1.getHours() + ":" + new DecimalFormat("00").format(d1.getMinutes());
//        }
//    }
//
//    public String convertToTimemillisec(String time) {
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        try {
//            Date mDate = sdf.parse(time);
//            long timeInMilliseconds = mDate.getTime();
//            return String.valueOf(timeInMilliseconds);
//        } catch (ParseException e) {
//            e.printStackTrace();
//            return time;
//        }
//    }
//
//    public void stopShimmerAnimation(final ShimmerFrameLayout shimmer) {
//        try {
//            if (shimmer != null) {
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        if (shimmer != null) {
//                            shimmer.stopShimmerAnimation();
//                            shimmer.setVisibility(View.GONE);
//                        }
//                    }
//                }, 2000);
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }


}
