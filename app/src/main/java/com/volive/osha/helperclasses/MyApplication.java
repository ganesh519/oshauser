package com.volive.osha.helperclasses;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;

public class MyApplication extends Application implements Application.ActivityLifecycleCallbacks {

    private static boolean isActive;

    public static boolean isActivityVisible() {
        return isActive;
    }

    public static boolean activityResumed() {
        return isActive;
    }

    public static boolean activityPaused() {
        return isActive = false;

    }

    @Override
    public void onCreate() {
        super.onCreate();
        //TypefaceUtil.overrideFont(getApplicationContext(), "SERIF", "fonts/reguler.ttf"); // font from assets: "assets/fonts/Roboto-Regular.ttf

        TypefaceUtil.overrideFont(this, "DEFAULT", "fonts/reguler.ttf");
        TypefaceUtil.overrideFont(this, "MONOSPACE", "fonts/reguler.ttf");
        TypefaceUtil.overrideFont(this, "SERIF", "fonts/reguler.ttf");
        TypefaceUtil.overrideFont(this, "SANS_SERIF", "fonts/reguler.ttf");

    }

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {

    }

    @Override
    public void onActivityStarted(Activity activity) {

    }

    @Override
    public void onActivityResumed(Activity activity) {
        isActive = true;
    }

    @Override
    public void onActivityPaused(Activity activity) {
        isActive = false;
    }

    @Override
    public void onActivityStopped(Activity activity) {
        isActive = false;
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

    }

    @Override
    public void onActivityDestroyed(Activity activity) {
        isActive = false;
    }


}
