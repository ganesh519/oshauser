package com.volive.osha.models;

public class SpOrderListModel {
    String txt_order_id;
    String txt_loaction;
    String txt_date;
    String txt_issue;

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    int image;

    public String getTxt_order_id() {
        return txt_order_id;
    }

    public void setTxt_order_id(String txt_order_id) {
        this.txt_order_id = txt_order_id;
    }

    public String getTxt_loaction() {
        return txt_loaction;
    }

    public void setTxt_loaction(String txt_loaction) {
        this.txt_loaction = txt_loaction;
    }

    public String getTxt_date() {
        return txt_date;
    }

    public void setTxt_date(String txt_date) {
        this.txt_date = txt_date;
    }

    public String getTxt_issue() {
        return txt_issue;
    }

    public void setTxt_issue(String txt_issue) {
        this.txt_issue = txt_issue;
    }
}
