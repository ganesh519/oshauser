package com.volive.osha.models;

/**
 * Created by volive on 7/23/2019.
 */

public class WalletModels {

    public String wallet_date, wallet_amount, wallet_order_id, paid,wallet_type,wallet_user_id;
            int image;

    public String getPaid() {
        return paid;
    }

    public void setPaid(String paid) {
        this.paid = paid;
    }

    public String getWallet_date() {
        return wallet_date;
    }

    public void setWallet_date(String wallet_date) {
        this.wallet_date = wallet_date;
    }

    public String getWallet_amount() {
        return wallet_amount;
    }

    public void setWallet_amount(String wallet_amount) {
        this.wallet_amount = wallet_amount;
    }

    public String getWallet_order_id() {
        return wallet_order_id;
    }

    public void setWallet_order_id(String wallet_order_id) {
        this.wallet_order_id = wallet_order_id;
    }

    public String getWallet_type() {
        return wallet_type;
    }

    public void setWallet_type(String wallet_type) {
        this.wallet_type = wallet_type;
    }

    public String getWallet_user_id() {
        return wallet_user_id;
    }

    public void setWallet_user_id(String wallet_user_id) {
        this.wallet_user_id = wallet_user_id;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
