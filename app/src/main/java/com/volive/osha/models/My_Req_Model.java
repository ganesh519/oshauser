package com.volive.osha.models;

public class My_Req_Model {

    String  My_req_ID,My_req_order_ID,
            My_req_pickup,My_req_drop,
            My_req_passenger_num,My_req_car_type,
            My_req_deliveryItem,My_req_desc,
            My_req_add,My_req_lat_pic,My_req_lang_pick,
            My_req_lat_drop,My_req_lang_drop,My_req_request_type,My_re_payment_status,
            My_req_status,My_req_tracking_sts,My_req_offer_price,My_req_rating,MY_req_rating_sts,
            My_req_comment,My_req_date;

    public String getMY_req_rating_sts() {
        return MY_req_rating_sts;
    }

    public void setMY_req_rating_sts(String MY_req_rating_sts) {
        this.MY_req_rating_sts = MY_req_rating_sts;
    }

    public String getMy_re_payment_status() {
        return My_re_payment_status;
    }

    public void setMy_re_payment_status(String my_re_payment_status) {
        My_re_payment_status = my_re_payment_status;
    }

    public String getMy_req_rating() {
        return My_req_rating;
    }

    public void setMy_req_rating(String my_req_rating) {
        My_req_rating = my_req_rating;
    }

    public String getMy_req_ID() {
        return My_req_ID;
    }

    public void setMy_req_ID(String my_req_ID) {
        My_req_ID = my_req_ID;
    }

    public String getMy_req_order_ID() {
        return My_req_order_ID;
    }

    public void setMy_req_order_ID(String my_req_order_ID) {
        My_req_order_ID = my_req_order_ID;
    }

    public String getMy_req_pickup() {
        return My_req_pickup;
    }

    public void setMy_req_pickup(String my_req_pickup) {
        My_req_pickup = my_req_pickup;
    }

    public String getMy_req_drop() {
        return My_req_drop;
    }

    public void setMy_req_drop(String my_req_drop) {
        My_req_drop = my_req_drop;
    }

    public String getMy_req_passenger_num() {
        return My_req_passenger_num;
    }

    public void setMy_req_passenger_num(String my_req_passenger_num) {
        My_req_passenger_num = my_req_passenger_num;
    }

    public String getMy_req_car_type() {
        return My_req_car_type;
    }

    public void setMy_req_car_type(String my_req_car_type) {
        My_req_car_type = my_req_car_type;
    }

    public String getMy_req_deliveryItem() {
        return My_req_deliveryItem;
    }

    public void setMy_req_deliveryItem(String my_req_deliveryItem) {
        My_req_deliveryItem = my_req_deliveryItem;
    }

    public String getMy_req_desc() {
        return My_req_desc;
    }

    public void setMy_req_desc(String my_req_desc) {
        My_req_desc = my_req_desc;
    }

    public String getMy_req_add() {
        return My_req_add;
    }

    public void setMy_req_add(String my_req_add) {
        My_req_add = my_req_add;
    }

    public String getMy_req_lat_pic() {
        return My_req_lat_pic;
    }

    public void setMy_req_lat_pic(String my_req_lat_pic) {
        My_req_lat_pic = my_req_lat_pic;
    }

    public String getMy_req_lang_pick() {
        return My_req_lang_pick;
    }

    public void setMy_req_lang_pick(String my_req_lang_pick) {
        My_req_lang_pick = my_req_lang_pick;
    }

    public String getMy_req_lat_drop() {
        return My_req_lat_drop;
    }

    public void setMy_req_lat_drop(String my_req_lat_drop) {
        My_req_lat_drop = my_req_lat_drop;
    }

    public String getMy_req_lang_drop() {
        return My_req_lang_drop;
    }

    public void setMy_req_lang_drop(String my_req_lang_drop) {
        My_req_lang_drop = my_req_lang_drop;
    }

    public String getMy_req_request_type() {
        return My_req_request_type;
    }

    public void setMy_req_request_type(String my_req_request_type) {
        My_req_request_type = my_req_request_type;
    }

    public String getMy_req_status() {
        return My_req_status;
    }

    public void setMy_req_status(String my_req_status) {
        My_req_status = my_req_status;
    }

    public String getMy_req_tracking_sts() {
        return My_req_tracking_sts;
    }

    public void setMy_req_tracking_sts(String my_req_tracking_sts) {
        My_req_tracking_sts = my_req_tracking_sts;
    }

    public String getMy_req_offer_price() {
        return My_req_offer_price;
    }

    public void setMy_req_offer_price(String my_req_offer_price) {
        My_req_offer_price = my_req_offer_price;
    }

    public String getMy_req_comment() {
        return My_req_comment;
    }

    public void setMy_req_comment(String my_req_comment) {
        My_req_comment = my_req_comment;
    }

    public String getMy_req_date() {
        return My_req_date;
    }

    public void setMy_req_date(String my_req_date) {
        My_req_date = my_req_date;
    }
}
