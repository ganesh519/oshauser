package com.volive.osha.models;

public class ReqSPModel_list {
    String name,location,currency;
    String st_offer_id,st_provider_id,st_request_id,st_offer_price,st_offer_status,st_offer_date,st_provider_name,st_provider_phone
            ,st_driver_pic,st_driver_address,st_provider_rating;
    int image;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getSt_offer_id() {
        return st_offer_id;
    }

    public void setSt_offer_id(String st_offer_id) {
        this.st_offer_id = st_offer_id;
    }

    public String getSt_provider_id() {
        return st_provider_id;
    }

    public void setSt_provider_id(String st_provider_id) {
        this.st_provider_id = st_provider_id;
    }

    public String getSt_request_id() {
        return st_request_id;
    }

    public void setSt_request_id(String st_request_id) {
        this.st_request_id = st_request_id;
    }

    public String getSt_offer_price() {
        return st_offer_price;
    }

    public void setSt_offer_price(String st_offer_price) {
        this.st_offer_price = st_offer_price;
    }

    public String getSt_offer_status() {
        return st_offer_status;
    }

    public void setSt_offer_status(String st_offer_status) {
        this.st_offer_status = st_offer_status;
    }

    public String getSt_offer_date() {
        return st_offer_date;
    }

    public void setSt_offer_date(String st_offer_date) {
        this.st_offer_date = st_offer_date;
    }

    public String getSt_provider_name() {
        return st_provider_name;
    }

    public void setSt_provider_name(String st_provider_name) {
        this.st_provider_name = st_provider_name;
    }

    public String getSt_provider_phone() {
        return st_provider_phone;
    }

    public void setSt_provider_phone(String st_provider_phone) {
        this.st_provider_phone = st_provider_phone;
    }

    public String getSt_driver_pic() {
        return st_driver_pic;
    }

    public void setSt_driver_pic(String st_driver_pic) {
        this.st_driver_pic = st_driver_pic;
    }

    public String getSt_driver_address() {
        return st_driver_address;
    }

    public void setSt_driver_address(String st_driver_address) {
        this.st_driver_address = st_driver_address;
    }

    public String getSt_provider_rating() {
        return st_provider_rating;
    }

    public void setSt_provider_rating(String st_provider_rating) {
        this.st_provider_rating = st_provider_rating;
    }
}
