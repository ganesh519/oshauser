package com.volive.osha.models;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.volive.osha.R;


/**
 * Created by volive on 7/18/2017.
 */

public class ResideMenuItem extends LinearLayout
{

    /** menu item  icon  */
    private ImageView iv_icon;
    /** menu item  title */
    private TextView tv_title;
    private  TextView tv_notification;
    String s;
    String notificationtitle;
    Context context;

    public ResideMenuItem(Context context, String home, String s)
    {
        super(context);
        initViews(context,s, "");

        tv_title.setText(home);
        // tv_notification.setText("10");
    }

   /* public ResideMenuItem(Context context, int icon, int title)
    {
        super(context);
        initViews(context, s, "");
        iv_icon.setImageResource(icon);
        tv_title.setText(title);
    }*/

    public ResideMenuItem(Context context, String home, String s, String title)
    {
        super(context);
        // tv_title.setText(home);
        initViews(context, s,title);
        //  this.notificationtitle=title;
        // iv_icon.setImageResource(icon);
        tv_title.setText(home);
        // tv_title.setBackground(context.getResources().getDrawable(R.drawable.itembg));
    }

    public  void initViews(Context context, String s, String title)
    {
        View view;
        if(s.equalsIgnoreCase("0")){
            LayoutInflater inflater=(LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view=inflater.inflate(R.layout.residemenu_item, this);

        }else {
            LayoutInflater inflater=(LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view= inflater.inflate(R.layout.bg_item, this);
            /*iv_icon = (ImageView) findViewById(R.id.iv_icon);
            tv_title = (TextView) findViewById(R.id.tv_title);*/
        }
        iv_icon = (ImageView) view.findViewById(R.id.iv_icon);
        tv_title = (TextView) view.findViewById(R.id.tv_title);
        tv_notification=(TextView) view.findViewById(R.id.tv_notification);
        if(title.equalsIgnoreCase("0")){
            tv_notification.setVisibility(GONE);
        }else {
            tv_notification.setVisibility(VISIBLE);
        }
    }

    /**
     * set the icon color;
     *
     * @param icon
     */
    public void setIcon(int icon)
    {
        iv_icon.setImageResource(icon);
    }

    /**
     * set the title with resource
     * ;
     * @param title
     */
    public void setTitle(int title){
        tv_title.setText(title);
    }

    /**
     * set the title with string;
     *
     * @param title
     */
    public void setTitle(String title){
        tv_title.setText(title);
    }
}
