package com.volive.osha.firebase;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.volive.osha.R;
import com.volive.osha.activities.ChatActivity;
import com.volive.osha.activities.InvoiceDetailsActivity;
import com.volive.osha.activities.MyRequestActivity;
import com.volive.osha.activities.RequestDetailsActivity;
import com.volive.osha.activities.Splash_screen;
import com.volive.osha.activities.TrackActivity;
import com.volive.osha.helperclasses.Offer_status;
import com.volive.osha.util_classes.Chating_screen_status;
import com.volive.osha.util_classes.PreferenceUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.Map;
import java.util.Random;


public class MyFirebaseMessagingService extends FirebaseMessagingService {
    int m = 0;
    PreferenceUtils preferenceUtils;
    String language = "", thread_id = "", type = "", title = "";
    JSONObject object;
    String  request_id, order_date, user_id, message, image, auth_level, sender_id, receiver_Id;
    Context ctx;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        preferenceUtils = new PreferenceUtils(this);
     //  String data_msg = remoteMessage.getData().toString();
     // String notification_msg = remoteMessage.getNotification().getBody();
      //Log.e("onMessageReceived: ", data_msg);
     // Log.e("onMessage: ", notification_msg);

        try {
            if (remoteMessage != null) {
                preferenceUtils = new PreferenceUtils(this);
                Random random = new Random();
                m = random.nextInt(9999 - 1000) + 1000;
                if (remoteMessage.getData().size() > 0) {
                    String data = remoteMessage.getData().toString();
                    Log.e("data", data);
                    Map<String, String> params = remoteMessage.getData();
                    object = new JSONObject(params);
                    type = object.getString("notification_type");
                    title = object.getString("notification_title_en");
                   // message = object.getString("description_en");
                    // String body = object.getString("body");
                    Log.d("JSON_OBJECT", object.toString());
                    sendNotification(object.getString("notification_title_en"));
                }

                if (remoteMessage.getNotification() != null) {
                    Map<String, String> params = remoteMessage.getData();
                    object = new JSONObject(params);
                    Log.d("JSON_OBJECT", object.toString());
                    type = object.getString("notification_type");
                    title = object.getString("notification_title_en");
                  //  message = object.getString("description_en");
                    notification_handle();
                }


            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("onMessageReceived: ", e.getMessage());
        }
    }


    private void notification_handle() {
        try {
            if (type.equalsIgnoreCase("new_request"))
            {
                title = object.getString("notification_title_en");
                Intent intent = new Intent(getApplicationContext(), MyRequestActivity.class);
                handleNotification(intent, object.getString("notification_title_en"));
            }

            else if (type.equalsIgnoreCase("offer_accepted")) {
                title = object.getString("notification_title_en");
                if (Offer_status.isActivityVisible()) {
                    Intent intent;
                    intent = new Intent("track");
                    intent.putExtra("req_id", object.getString("request_id"));
                    intent.putExtra("action", "0");
                    LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(this);
                    localBroadcastManager.sendBroadcast(intent);
                }
                else {
                    Intent intent = new Intent(getApplicationContext(), TrackActivity.class);
                    intent.putExtra("req_id", object.getString("request_id"));
                    intent.putExtra("action", "0");
                    handleNotification(intent, object.getString("notification_title_en"));
                }
            }

            else if (type.equalsIgnoreCase("cancel_request")) {
                Intent intent = new Intent(getApplicationContext(), MyRequestActivity.class);
                handleNotification(intent, object.getString("notification_title_en"));
            }

            else if (object.getString("notification_type").equalsIgnoreCase("new_offer")) {
                if (Offer_status.isActivityVisible()) {
                    Intent intent;
                    intent = new Intent("com.volive.osha_FCM-OFFER");
                    intent.putExtra("reqID", object.getString("request_id"));
                    LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(this);
                    localBroadcastManager.sendBroadcast(intent);
                }
                else {
                    Intent intent = new Intent(getApplicationContext(), RequestDetailsActivity.class);
                    intent.putExtra("reqID", object.getString("request_id"));
                    handleNotification(intent, object.getString("description_en"));
                }
            }
            else if (type.equalsIgnoreCase("work_started")) {
                if (Offer_status.isActivityVisible()) {
                    Intent intent;
                    intent = new Intent("track");
                    intent.putExtra("req_id", object.getString("request_id"));
                    intent.putExtra("action", "0");
                    LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(this);
                    localBroadcastManager.sendBroadcast(intent);
                } else {
                    Intent intent = new Intent(getApplicationContext(), TrackActivity.class);
                    intent.putExtra("req_id", object.getString("request_id"));
                    intent.putExtra("action", "0");
                    handleNotification(intent, object.getString("notification_title_en"));
                }
            }
            else if (type.equalsIgnoreCase("invoice_generated")) {
                Intent intent = new Intent(getApplicationContext(), InvoiceDetailsActivity.class);
                intent.putExtra("req_id", object.getString("request_id"));
                handleNotification(intent, object.getString("description_en"));
            }
            else if (type.equalsIgnoreCase("request_completed")) {
                Intent intent = new Intent(getApplicationContext(), MyRequestActivity.class);
                handleNotification(intent, object.getString("description_en"));
            }
            else if (object.getString("notification_type").equalsIgnoreCase("new_message"))
            {
                title = object.getString("notification_title_en");
                if (Offer_status.isActivityVisible()) {
                    Intent intent;
                    intent = new Intent("chat_message");
                    intent.putExtra("sender", object.getString("sender_id"));
                    intent.putExtra("rec_id", object.getString("receiver_id"));
                    intent.putExtra("request", object.getString("request_id"));
                    intent.putExtra("rcv_image",object.getString("image"));
                    LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(this);
                    localBroadcastManager.sendBroadcast(intent);
                }
                else {
                    Intent intent = new Intent(getApplicationContext(), ChatActivity.class);
                    intent.putExtra("sender", object.getString("receiver_id"));
                    intent.putExtra("rec_id", object.getString("sender_id"));
                    intent.putExtra("request", object.getString("request_id"));
                    intent.putExtra("rcv_image",object.getString("image"));
                    handleNotification(intent, object.getString("notification_type"));
                }


            }



        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void handleNotification(Intent intent, String message) {
        if (isAppIsInBackground(this)) {
            createNotification(intent, message);
            // sendNotification(message);
        } else {
            createElseNotification(intent, message);
        }
    }

    private void createElseNotification(Intent intent, String message) {
        intent.putExtra("message", message);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, m, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        final NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getApplicationContext());

        NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
        inboxStyle.addLine(message);

        Notification notification;
        String CHANNEL_ID = "my_channel_01";
        CharSequence name = getApplicationContext().getString(R.string.app_name);
        int importance = NotificationManager.IMPORTANCE_HIGH;
        NotificationManager notificationManager =
                (NotificationManager)
                        getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            notificationManager.createNotificationChannel(mChannel);
        }

        notification = mBuilder.setSmallIcon(R.mipmap.ic_launcher).setTicker("").setWhen(0)
                .setAutoCancel(true)
                .setChannelId(CHANNEL_ID)
                .setContentTitle(title)
                .setStyle(inboxStyle)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentText(message)
                .setContentIntent(pendingIntent)
                .setDefaults(Notification.DEFAULT_ALL)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .build();


        notificationManager.notify(m, notification);

    }

    private void createNotification(Intent intent, String message) {
        intent.putExtra("message", message);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, m, intent, PendingIntent.FLAG_ONE_SHOT);

        final NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                getApplicationContext());

        NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
        inboxStyle.addLine(message);

        Notification notification;
        String CHANNEL_ID = "my_channel_01";
        CharSequence name = getApplicationContext().getString(R.string.app_name);
        int importance = NotificationManager.IMPORTANCE_HIGH;
        NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            notificationManager.createNotificationChannel(mChannel);

        }

        notification = mBuilder.setSmallIcon(R.mipmap.ic_launcher).setTicker("").setWhen(0)
                .setAutoCancel(true)
                .setChannelId(CHANNEL_ID)
                .setContentTitle(title)
                .setStyle(inboxStyle)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentText(message)
                .setContentIntent(pendingIntent)
                .setDefaults(Notification.DEFAULT_ALL)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .build();

        notificationManager.notify(m, notification);

    }

    public boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            assert am != null;
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = false;
                        }
                    }
                }
            }
        } else {
            assert am != null;
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = false;
            }
        }

        return isInBackground;
    }

    private void sendNotification(String messageBody) {
        Intent intent = new Intent(this, Splash_screen.class);
        intent.putExtra("body", type);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);
        final NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                getApplicationContext());
        Notification notification;
        NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
        inboxStyle.addLine(message);
        //String CHANNEL_ID = "my_channel_01";

        CharSequence name = getApplicationContext().getString(R.string.app_name);
        String channelId = getString(R.string.default_notification_channel_id);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        /*NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId).setTicker("").setWhen(0)
                        .setSmallIcon(R.mipmap.app_icon_final)
                        .setContentTitle(title)
                        .setContentText(message)
                        .setContentText(messageBody)
                        .setAutoCancel(true)
                        .setStyle(inboxStyle)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent)
                        .build();*/

        notification = mBuilder.setSmallIcon(R.mipmap.ic_launcher).setTicker("").setWhen(0)
                .setAutoCancel(true)
                .setContentTitle(title)
                .setStyle(inboxStyle)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentText(messageBody)
                .setContentIntent(pendingIntent)
                .setDefaults(Notification.DEFAULT_ALL)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .build();

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }
        notificationManager.notify(m, notification);
        // notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }

}










