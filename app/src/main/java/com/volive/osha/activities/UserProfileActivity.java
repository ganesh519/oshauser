package com.volive.osha.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.bumptech.glide.Glide;
import com.google.gson.JsonElement;
import com.volive.osha.R;
import com.volive.osha.helperclasses.Constant_keys;
import com.volive.osha.helperclasses.GalleryUriToPath;
import com.volive.osha.helperclasses.Validation_Class;
import com.volive.osha.util_classes.API_Services;
import com.volive.osha.util_classes.PreferenceUtils;
import com.volive.osha.util_classes.Retrofit_fun;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.FileNameMap;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.provider.MediaStore.ACTION_IMAGE_CAPTURE;


public class UserProfileActivity extends BaseActivity implements View.OnClickListener {
    public static boolean isProfile = false;
    ImageView back_img_details, user_profile_add, user_profile_add_img;
    TextView toolbar_title, edit_card_txt, txt_edit, txt_save, txt_change_password,txt_password,txt_confirm_password;
    CircleImageView user_profile_image;
    EditText txt_profile_name, txt_profile_email, txt_profile_phn_number
            /* ,txt_profile_password, txt_profile_conf_pwd*/;
    View name_view, email_view, number_view, password_view, conf_pwd_view;
    String activityName = "",stUserID,language;
    PreferenceUtils preferenceUtils;
    Validation_Class validate;

    static final int OPEN_MEDIA_PICKER = 1;
    int CAMERA_CAPTURE = 2;
    int PICK_IMAGE = 3 ;
    public static List<String> select_img_list = new ArrayList<>();
    String multi_image_path = "empty",Picked_profile = "empty";;

    String st_email,st_mobile,st_name,st_password,st_cnf_pwd,stDob = " ",str_latitude = "",st_username,str_longitude = "",st_Profile_image = " ";



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);
        preferenceUtils = new PreferenceUtils(UserProfileActivity.this);
        validate = new Validation_Class(UserProfileActivity.this);
        stUserID = preferenceUtils.getStringFromPreference(PreferenceUtils.USER_ID,"");
        language = preferenceUtils.getStringFromPreference(PreferenceUtils.LANGUAGE,"");
        initializeUi();
        initializeValues();
        set_values();
    }
    private void set_values()
       {
        if (preferenceUtils.getStringFromPreference(PreferenceUtils.IMAGE,"").isEmpty())
        {
            user_profile_image.setImageResource(R.drawable.guest_user);
        }
        else
         {
             Glide.with(getApplicationContext())
                  .load(Constant_keys.imagebaseurl + preferenceUtils.getStringFromPreference(PreferenceUtils.IMAGE,""))
                  .into(user_profile_image);
         }
        txt_profile_name .setText(preferenceUtils.getStringFromPreference(PreferenceUtils.UserName,""));
        txt_profile_email.setText(preferenceUtils.getStringFromPreference(PreferenceUtils.Email,""));
        txt_profile_phn_number.setText(preferenceUtils.getStringFromPreference(PreferenceUtils.Mobile,""));
        txt_confirm_password .setText(preferenceUtils.getStringFromPreference(PreferenceUtils.password_new,""));
        txt_password.setText(preferenceUtils.getStringFromPreference(PreferenceUtils.password_new,""));
     }

    private void initializeUi() {

        //ImageView
        back_img_details = findViewById(R.id.back_img_details);
        user_profile_add = findViewById(R.id.user_profile_add);
        user_profile_add_img = findViewById(R.id.user_profile_add_img);
        //TextView
        toolbar_title = findViewById(R.id.toolbar_title);
        txt_edit = findViewById(R.id.txt_edit);
        edit_card_txt = findViewById(R.id.edit_card_txt);
        txt_save = findViewById(R.id.txt_save);
        txt_change_password = findViewById(R.id.txt_change_password);
        txt_password = findViewById(R.id.txt_password);
        txt_confirm_password = findViewById(R.id.txt_confirm_password);
        //EditText
        txt_profile_name = findViewById(R.id.txt_profile_name);
        txt_profile_email = findViewById(R.id.txt_profile_email);
        txt_profile_phn_number = findViewById(R.id.txt_profile_phn_number);
//        txt_profile_password = findViewById(R.id.txt_profile_password);
//        txt_profile_conf_pwd = findViewById(R.id.txt_profile_conf_pwd);
        //CircleImageView
        user_profile_image = findViewById(R.id.user_profile_image);

        //View
        name_view = findViewById(R.id.name_view);
        email_view = findViewById(R.id.email_view);
        number_view = findViewById(R.id.number_view);
//        password_view = findViewById(R.id.password_view);
//        conf_pwd_view = findViewById(R.id.conf_pwd_view);

    }

    private void initializeValues() {

        isProfile = false;
        txt_profile_name.setEnabled(false);
        txt_profile_email.setEnabled(false);
        txt_profile_phn_number.setEnabled(false);
//        txt_profile_password.setEnabled(false);
//        txt_profile_conf_pwd.setEnabled(false);
        txt_edit.setVisibility(View.VISIBLE);
        txt_save.setVisibility(View.GONE);
        user_profile_add_img.setVisibility(View.GONE);
        user_profile_add_img.setOnClickListener(this);
        back_img_details.setOnClickListener(this);
        txt_edit.setOnClickListener(this);
        txt_save.setOnClickListener(this);
        edit_card_txt.setOnClickListener(this);
        txt_change_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialogOffer = new Dialog(UserProfileActivity.this, R.style.MyAlertDialogTheme);
                dialogOffer.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialogOffer.setContentView(R.layout.change_pwd_dialog);
                dialogOffer.getWindow().setBackgroundDrawableResource(R.drawable.bg_dialog);
                dialogOffer.setCanceledOnTouchOutside(true);
                dialogOffer.setCancelable(true);
                dialogOffer.show();
                dialogOffer.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

                ImageView back_img_details = dialogOffer.findViewById(R.id.back_img_details);
                final EditText pswrd_edit_text = dialogOffer.findViewById(R.id.pswrd_edit_text);
                final EditText conform_pwd_edit_text = dialogOffer.findViewById(R.id.conform_pwd_edit_text);
                Button btn_save = dialogOffer.findViewById(R.id.btn_save);
                back_img_details.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogOffer.dismiss();
                    }
                });
                btn_save.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String st_pwd,st_cnf_pwd;
                        st_pwd = pswrd_edit_text.getText().toString();
                        st_cnf_pwd = conform_pwd_edit_text.getText().toString();
                        if (st_pwd.isEmpty())
                        {
                            pswrd_edit_text.setError(getString(R.string.enter_password));
                        }
                        else if (st_cnf_pwd.isEmpty())
                        {
                            conform_pwd_edit_text.setError(getString(R.string.conf_pwd_enter));
                        }
                        else if (!st_pwd.equals(st_cnf_pwd)){
                            showToast(getString(R.string.pwd_not_matching));
                        }
                        else {
                            dialogOffer.dismiss();
                            change_pwd(st_pwd,st_cnf_pwd);
                        }
                    }
                });
            }
        });

    }

    private void change_pwd(final String st_pwd, String st_cnf_pwd)
    {

            final API_Services service = Retrofit_fun.getClient().create(API_Services.class);
            Call<JsonElement> call_logout = null;
            call_logout = service.user_password_change(Constant_keys.API_KEY, language, stUserID,st_pwd,st_cnf_pwd);
            final ProgressDialog progressdialog;
            progressdialog = new ProgressDialog(UserProfileActivity.this);
            progressdialog.setCancelable(false);
            progressdialog.setMessage(getString(R.string.loading));
            progressdialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressdialog.show();
            call_logout.enqueue(new Callback<JsonElement>() {
                @Override
                public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                    progressdialog.dismiss();
                    Log.e("ajkldhn", response.body().toString());
                    if (response.isSuccessful()) {
                        try {
                            JSONObject jsonObject = new JSONObject(response.body().toString());
                            int status = jsonObject.getInt("status");
                            if (status == 1) {

                                showToast(jsonObject.getString("message"));
                                preferenceUtils.saveString(PreferenceUtils.password_new,st_pwd);
                          /*      Intent intent = new Intent(User_Home_activity.this, LoginActivty.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                drawerLayout.closeDrawers();*/
                            }
                            else {
                                showToast(jsonObject.getString("message"));
                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }

                @Override
                public void onFailure(Call<JsonElement> call, Throwable t) {

                }
            });





    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back_img_details:
                finish();
                break;
            case R.id.txt_edit:
                isProfile = true;
                txt_profile_name.setEnabled(true);
                txt_profile_email.setEnabled(true);
                txt_profile_phn_number.setEnabled(true);
//                txt_profile_password.setEnabled(true);
//                txt_profile_conf_pwd.setEnabled(true);
                txt_edit.setVisibility(View.GONE);
                txt_save.setVisibility(View.VISIBLE);
                user_profile_add.setVisibility(View.GONE);
                user_profile_add_img.setVisibility(View.VISIBLE);

                name_view.setVisibility(View.VISIBLE);
                email_view.setVisibility(View.VISIBLE);
                number_view.setVisibility(View.VISIBLE);
//                password_view.setVisibility(View.VISIBLE);
//                conf_pwd_view.setVisibility(View.VISIBLE);
                toolbar_title.setText(getResources().getString(R.string.edit_profile));
                break;
            case R.id.user_profile_add_img:
                selectImage();
                break;

            case R.id.txt_save:
                isProfile = false;
                txt_profile_name.setEnabled(false);
                txt_profile_email.setEnabled(false);
                txt_profile_phn_number.setEnabled(false);
//                txt_profile_password.setEnabled(false);
//                txt_profile_conf_pwd.setEnabled(false);
                txt_edit.setVisibility(View.VISIBLE);
                txt_save.setVisibility(View.GONE);
                user_profile_add.setVisibility(View.VISIBLE);
                user_profile_add_img.setVisibility(View.GONE);

                name_view.setVisibility(View.INVISIBLE);
                email_view.setVisibility(View.INVISIBLE);
                number_view.setVisibility(View.INVISIBLE);
//                password_view.setVisibility(View.INVISIBLE);
//                conf_pwd_view.setVisibility(View.INVISIBLE);
                toolbar_title.setText(getResources().getString(R.string.profile));
                st_email = txt_profile_email.getText().toString();
                st_password = txt_password.getText().toString();
                st_cnf_pwd = txt_confirm_password.getText().toString();
                st_mobile = txt_profile_phn_number.getText().toString();
                st_username = txt_profile_name.getText().toString();
                st_name = txt_profile_name.getText().toString();
               // stDob = edit_profile_birth_date.getText().toString();
                if (st_name.isEmpty())
                {
                    txt_profile_name.getText().toString();
                }
                else if (st_email.isEmpty())
                {
                    txt_profile_email.setError(getString(R.string.enter_your_email));
                }
                else if (st_mobile.isEmpty())
                {
                    txt_profile_phn_number.setError(getString(R.string.enter_number));
                }
                else if (!isNetworkAvailable()) {
                    showToast(getResources().getString(R.string.please_check_your_network_connection));
                    return;
                } else {
                    edit_profile_part();
                }



                break;
            case R.id.edit_card_txt:

                Intent intent=new Intent(UserProfileActivity.this,AddCardActivity.class);
                startActivity(intent);
                break;

        }

    }


    private void selectImage() {

        final CharSequence[] options = {getResources().getString(R.string.takeaphoto), getResources().getString(R.string.choosefrmgallery)};

        AlertDialog.Builder builder = new AlertDialog.Builder(UserProfileActivity.this);
        builder.setTitle(getResources().getString(R.string.photos));
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item)
            {
                if (options[item].toString().equalsIgnoreCase(getResources().getString(R.string.takeaphoto))) {
                    cameraIntent();
                } else if (options[item].toString().equalsIgnoreCase(getResources().getString(R.string.choosefrmgallery))) {
                    choosefromgallery();

                }
                else if (options[item].toString().equalsIgnoreCase(getResources().getString(R.string.cancel))) {
                    dialog.dismiss();
                }

            }
        });

        builder.show();
    }

    private void cameraIntent() {
        Intent intent = new Intent(ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, CAMERA_CAPTURE);
    }

    private void choosefromgallery() {
        try {
            Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(galleryIntent, PICK_IMAGE);
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
    }

    public static String getMimeType(String url) {
        String type = null;
        String extension = MimeTypeMap.getFileExtensionFromUrl(url);
        Log.d("MIME_TYPE_EXT", extension);
        if (extension != null && extension != "") {
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
            //  Log.d("MIME_TYPE", type);
        } else {
            FileNameMap fileNameMap = URLConnection.getFileNameMap();
            type = fileNameMap.getContentTypeFor(url);
        }
        return type;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CAMERA_CAPTURE) {
            if (resultCode == RESULT_OK) {
                onCaptureImageResult(data);
            }
        }

        else if (requestCode == PICK_IMAGE)
        {

            if (resultCode == RESULT_OK)
            {
                Uri picUri = data.getData();
                String[] filePath = {MediaStore.Images.Media.DATA};
                Cursor c = getApplicationContext().getContentResolver().query(picUri, filePath, null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePath[0]);

                Picked_profile = GalleryUriToPath.getPath(getApplicationContext(), picUri);

                Picked_profile = GalleryUriToPath.getPath(getApplicationContext(), picUri);
                String file_profile = Picked_profile.substring(Picked_profile.lastIndexOf("/") + 1);
                //txt_image_only.setText(filename);
                //  txt_profile_photo.setText(file_profile);

                try {
                    Bitmap bm = BitmapFactory.decodeStream(getApplicationContext().getContentResolver().openInputStream(picUri));
                    user_profile_image.setImageBitmap(bm);
                }
                catch (FileNotFoundException e) {
                    e.printStackTrace();
                }


                //  String filename = PickedImgPath.substring(PickedImgPath.lastIndexOf("/") + 1);



                // txt_image_only.setText("photo Id has selected");
                c.close();
            }
        }



    }


    private void onCaptureImageResult(Intent data)
    {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");
        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
            Picked_profile = destination.getAbsolutePath();
            String file_profile = Picked_profile.substring(Picked_profile.lastIndexOf("/") + 1);
            //txt_profile_photo.setText(file_profile);
            Log.e("Camera Path", destination.getAbsolutePath());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        user_profile_image.setImageBitmap(thumbnail);
        //String filename = PickedImgPath.substring(PickedImgPath.lastIndexOf("/") + 1);
        // txt_image_only.setText(filename);
        // txt_image_only.setText("photo Id has selected");

    }



    private void edit_profile_part()
    {
        File file = null;
        //document_list = new ArrayList<>();
        MultipartBody.Part image_profile = null;
        if (!Picked_profile.equals("empty")) {
            file = new File(Picked_profile);
            RequestBody requestBody = RequestBody.create(MediaType.parse(getMimeType(Picked_profile)), file);
            image_profile = MultipartBody.Part.createFormData("profile_pic", file.getName(), requestBody);
            Log.d(" profile_pic", ">>>>>>>>>>" + image_profile);
            Log.e("alm",Picked_profile);
            //preferenceUtils.saveString(PreferenceUtils.IMAGE,PickedImgPath);
        }

        RequestBody r_api_key = RequestBody.create(MediaType.parse("multipart/form-data"), Constant_keys.API_KEY);
        RequestBody r_lang = RequestBody.create(MediaType.parse("multipart/form-data"), language);
        RequestBody r_user_id = RequestBody.create(MediaType.parse("multipart/form-data"), stUserID);
        RequestBody r_username= RequestBody.create(MediaType.parse("multipart/form-data"), st_username);
        RequestBody r_Mail = RequestBody.create(MediaType.parse("multipart/form-data"), st_email);
        RequestBody r_phone = RequestBody.create(MediaType.parse("multipart/form-data"), st_mobile);
        RequestBody r_name = RequestBody.create(MediaType.parse("multipart/form-data"), st_name);
        RequestBody r_password = RequestBody.create(MediaType.parse("multipart/form-data"), st_password);
        RequestBody r_confirm_password = RequestBody.create(MediaType.parse("multipart/form-data"), st_cnf_pwd);
        RequestBody r_dob = RequestBody.create(MediaType.parse("multipart/form-data"), stDob);
        RequestBody r_Latitude = RequestBody.create(MediaType.parse("multipart/form-data"), "");
        RequestBody r_Longitude = RequestBody.create(MediaType.parse("multipart/form-data"), "");

        RequestBody r_idProof = RequestBody.create(MediaType.parse("multipart/form-data"), "");
        RequestBody r_ins = RequestBody.create(MediaType.parse("multipart/form-data"), "");
        RequestBody r_licence = RequestBody.create(MediaType.parse("multipart/form-data"), "");
        RequestBody r_images = RequestBody.create(MediaType.parse("multipart/form-data"), "");

        final API_Services service = Retrofit_fun.getClient().create(API_Services.class);
        Call<JsonElement> callRetrofit = null;
        callRetrofit = service.edit_profile(r_api_key, r_lang, r_user_id,
                r_username,r_name, r_Mail, r_phone, r_password, r_confirm_password,
                r_dob,image_profile,r_idProof,r_ins,r_licence, r_images,
                r_Latitude, r_Longitude);


        Log.e("data", callRetrofit.toString());
        final ProgressDialog progressDoalog = new ProgressDialog(UserProfileActivity.this, R.style.AppCompatAlertDialogStyle);
        progressDoalog.setCancelable(false);
        progressDoalog.setMessage(getResources().getString(R.string.please_wait));
        progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDoalog.show();
        callRetrofit.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                progressDoalog.dismiss();
                if (response.isSuccessful()) {
                    String searchResponse = response.body().toString();
                    Log.d("Registration", "response  >>" + searchResponse.toString());
                    try {
                        JSONObject regObject = new JSONObject(searchResponse);
                        int status = regObject.getInt("status");
                        String message = regObject.getString("message");
                        //    String base_url = regObject.getString("base_url");

                        if (status ==1 )
                        {

                            //  Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                            JSONObject user_json = regObject.getJSONObject("details");
                            String name = user_json.getString("name");
                            String username = user_json.getString("username");
                            String email = user_json.getString("email");
                            String phone = user_json.getString("phone");
                            String profile_pic = user_json.getString("profile_pic");
                            //   String reeferal_code = user_json.getString("")
                            preferenceUtils.saveString(PreferenceUtils.User, name);
                            preferenceUtils.saveString(PreferenceUtils.UserName, username);
                            preferenceUtils.saveString(PreferenceUtils.Email, email);
                            preferenceUtils.saveString(PreferenceUtils.Mobile, phone);
                             preferenceUtils.saveString(PreferenceUtils.IMAGE,  profile_pic);
                            Glide.with(getApplicationContext()).load(Constant_keys.imagebaseurl + profile_pic).into(user_profile_image);

                            showToast(message);
                           // get_profile();


                        }
                        else {
                            Toast.makeText(getApplicationContext(),message,Toast.LENGTH_SHORT).show();
                        }
                    }
                    catch (JSONException e)
                    {
                        Log.e("error", e.getMessage());


                    }
                }
            }


            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                progressDoalog.dismiss();
                Log.d("Error Call", ">>>>" + call.toString());
                Log.d("Error", ">>>>" + t.toString());
            }
        });



    }


    @Override
    public void onBackPressed() {
        finish();

    }
}
