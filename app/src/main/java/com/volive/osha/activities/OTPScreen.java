package com.volive.osha.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.JsonElement;
import com.volive.osha.R;
import com.volive.osha.helperclasses.Constant_keys;
import com.volive.osha.helperclasses.Validation_Class;
import com.volive.osha.util_classes.API_Services;
import com.volive.osha.util_classes.PreferenceUtils;
import com.volive.osha.util_classes.Retrofit_fun;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class OTPScreen extends BaseActivity  implements  View.OnClickListener{
    Button btn_login;
    ImageView back_img_details;
    EditText edt_one, edt_two, edt_three, edt_four;
    TextView txt_resend_code;
    String activityName = "",mobile_str,language;
    PreferenceUtils preferenceUtils;
    Validation_Class validate;
    String otp,strOtp,User_Id;
    RelativeLayout rel_otp_layout;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otpscreen);
        Intent i = getIntent();
        mobile_str = i.getStringExtra("mobile");
        otp = i.getStringExtra("otp");
        preferenceUtils = new PreferenceUtils(OTPScreen.this);

        validate = new Validation_Class(OTPScreen.this);
        language = preferenceUtils.getStringFromPreference(PreferenceUtils.LANGUAGE,"");
        User_Id = preferenceUtils.getStringFromPreference(PreferenceUtils.USER_ID,"");
        initiializeUI();
        initiializeValues();
    }

    private void initiializeUI()
    {
        //Button
        btn_login = findViewById(R.id.btn_login);
        //ImageView
        back_img_details = findViewById(R.id.back_img_details);
        //EditText
        edt_one = findViewById(R.id.edt_one);
        edt_two = findViewById(R.id.edt_two);
        edt_three = findViewById(R.id.edt_three);
        edt_four = findViewById(R.id.edt_four);
        //TextView
        txt_resend_code = findViewById(R.id.txt_resend_code);
        rel_otp_layout = findViewById(R.id.rel_otp_layout);
    }

    private void initiializeValues()
    {

        rel_otp_layout.setOnClickListener(this);
        btn_login.setOnClickListener(this);
        back_img_details.setOnClickListener(this);
        txt_resend_code.setOnClickListener(this);

        edt_one.addTextChangedListener(new GenericTextWatcher(edt_one));
        edt_two.addTextChangedListener(new GenericTextWatcher(edt_two));
        edt_three.addTextChangedListener(new GenericTextWatcher(edt_three));
        edt_four.addTextChangedListener(new GenericTextWatcher(edt_four));
    }


    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.btn_login:
                String etOne = edt_one.getText().toString();
                String etTwo = edt_two.getText().toString();
                String etThree = edt_three.getText().toString();
                String etFour = edt_four.getText().toString();
                if (!etOne.isEmpty() && !etTwo.isEmpty() && !etThree.isEmpty() && !etFour.isEmpty())
                {
                    strOtp = etOne + etTwo + etThree + etFour;
                    if (validate.checkInternetConnection(OTPScreen.this))
                    {
                        otpService(strOtp);
                        // Log.e("jkdvhs",strOtp);
                    } else {
                        validate.displaySnackbar(rel_otp_layout, getResources().getString(R.string.please_check_your_network_connection));
                    }
                } else {
                    validate.displaySnackbar(rel_otp_layout, getResources().getString(R.string.please_enter_otp_number));
                }
                break;

            case  R.id.back_img_details:
                onBackPressed();
                break;

            case  R.id.txt_resend_code:
                otpResendService();
                break;


        }


    }

    public void otpService(String strOtp) {
        final API_Services service = Retrofit_fun.getClient().create(API_Services.class);
        Call<JsonElement> callRetrofit = null;
        callRetrofit = service.otp_service(Constant_keys.API_KEY, language, preferenceUtils.getStringFromPreference(PreferenceUtils.USER_ID, ""), strOtp);
        final ProgressDialog progressDoalog = new ProgressDialog(this, R.style.AppCompatAlertDialogStyle);
        progressDoalog.setCancelable(false);
        progressDoalog.setMessage(getResources().getString(R.string.please_wait));
        progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDoalog.show();

        callRetrofit.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                progressDoalog.dismiss();


                if (response.isSuccessful())
                {
                    String searchResponse = response.body().toString();
                    Log.e("jzhdfno",searchResponse);
                    try {
                        JSONObject loginObject = new JSONObject(searchResponse);
                        int status = loginObject.getInt("status");
                        String message = loginObject.getString("message");
                        Log.e("fdghuj",message);

                        if (status == 1)
                        {
                            showToast(message);
                            /*Intent i = new Intent(OTPScreen.this, User_Home_activity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);*/
                            Intent i = new Intent(OTPScreen.this, LoginActivty.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(i);
                            finish();
                            validate.displaySnackbar(rel_otp_layout, message);
                           // finish();
                        }
                        else
                            if (status == 0)
                        {
                            showToast(message);
                            validate.displaySnackbar(rel_otp_layout, message);
                            Intent i = new Intent(OTPScreen.this, LoginActivty.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(i);
                            finish();
                            edt_one.setText("");
                            edt_two.setText("");
                            edt_three.setText("");
                            edt_four.setText("");
//                            vaildate_object.displaySnackbar(ll_otp_layout, message);
                            edt_one.requestFocus();
                        }

                    } catch (JSONException e) {

                        Log.e("error", e.getMessage());

                    }
                }
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                progressDoalog.dismiss();
                Log.d("Error Call", ">>>>" + call.toString());
                Log.d("Error", ">>>>" + t.toString());
            }
        });
    }

    public void otpResendService() {
        final API_Services service = Retrofit_fun.getClient().create(API_Services.class);
        Call<JsonElement> callRetrofit = null;
        // callRetrofit = service.resend_otp(Constants.API_KEY, "en", preferenceUtils.getStringFromPreference(PreferenceUtils.USER_ID, ""));
        callRetrofit = service.resend_otp(Constant_keys.API_KEY, language,preferenceUtils.getStringFromPreference(PreferenceUtils.USER_ID, ""));

        final ProgressDialog progressDoalog = new ProgressDialog(this, R.style.AppCompatAlertDialogStyle);
        progressDoalog.setCancelable(false);
        progressDoalog.setMessage(getResources().getString(R.string.please_wait));
        progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDoalog.show();

        callRetrofit.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                progressDoalog.dismiss();

                System.out.println("----------------------------------------------------");
                Log.d("Call request", call.request().toString());
                Log.d("Call request header", call.request().headers().toString());
                Log.d("Response raw header", response.headers().toString());
                Log.d("Response raw", String.valueOf(response.raw().body()));
                Log.d("Response code", String.valueOf(response.code()));
                System.out.println("----------------------------------------------------");

                if (response.isSuccessful()) {
                    String searchResponse = response.body().toString();
                    Log.d("Login", "response  >>" + searchResponse.toString());
                    try {
                        JSONObject loginObject = new JSONObject(searchResponse);
                        int status = loginObject.getInt("status");
                        String message = loginObject.getString("message");
                        // String base_url = loginObject.getString("base_url");
                        if (status == 1) {
                            validate.displaySnackbar(rel_otp_layout, message);
                            edt_one.requestFocus();
                            edt_one.setText("");
                            edt_two.setText("");
                            edt_three.setText("");
                            edt_four.setText("");

                        } else {
                            validate.displaySnackbar(rel_otp_layout, message);
                        }

                    } catch (JSONException e) {
                        Log.e("error", e.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                progressDoalog.dismiss();
                Log.d("Error Call", ">>>>" + call.toString());
                Log.d("Error", ">>>>" + t.toString());
            }
        });
    }

    public class GenericTextWatcher implements TextWatcher {
        private View view;

        private GenericTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void afterTextChanged(Editable editable) {
            // TODO Auto-generated method stub
            String text = editable.toString();
            switch (view.getId()) {
                case R.id.edt_one:
                    if (text.length() == 1)
                        edt_two.requestFocus();
                    edt_two.setSelection(edt_two.getText().length());
                    break;
                case R.id.edt_two:
                    if (text.length() == 1)
                        edt_three.requestFocus();
                    edt_three.setSelection(edt_three.getText().length());
                    break;
                case R.id.edt_three:
                    if (text.length() == 1)
                        edt_four.requestFocus();
                    edt_four.setSelection(edt_four.getText().length());
                    break;
                case R.id.edt_four:
                    if (text.length() == 1)
                        btn_login.requestFocus();
                    break;

            }
        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            // TODO Auto-generated method stub
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int arg1, int arg2, int arg3) {
            // TODO Auto-generated method stub
            String text = charSequence.toString();
            switch (view.getId()) {
                case R.id.edt_one:
                    if (text.length() == 1)
                        edt_two.requestFocus();
                    edt_two.setSelection(edt_two.getText().length());
                    break;
                case R.id.edt_two:
                    if (text.length() == 1) {
                        edt_three.requestFocus();
                        edt_three.setSelection(edt_three.getText().length());
                    } else if (text.length() == 0) {
                        edt_one.requestFocus();
                        edt_one.setSelection(edt_one.getText().length());
                    }
                    break;
                case R.id.edt_three:
                    if (text.length() == 1) {
                        edt_four.requestFocus();
                        edt_four.setSelection(edt_four.getText().length());
                    } else if (text.length() == 0) {
                        edt_two.requestFocus();
                        edt_two.setSelection(edt_two.getText().length());
                    }
                    break;
                case R.id.edt_four:
                    if (text.length() == 1) {
                        btn_login.requestFocus();
                    } else if (text.length() == 0) {
                        edt_three.requestFocus();
                        edt_three.setSelection(edt_three.getText().length());
                    }
                    break;
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i=new Intent(Intent.ACTION_MAIN);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        finish();
    }
}
