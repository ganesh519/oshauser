package com.volive.osha.activities;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.volive.osha.R;


public class CreateInvoiceActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView back_img_details;
    TextView toolbar_title, txt_orderId, txt_customerName, txt_invoiceNo, txt_invoice_date, txt_subTotal, txt_coupon_price, txt_total_price;
    EditText txt_service_name, txt_estimated_price, txt_input_num, txt_input_num1, et_desc;
    LinearLayout add_service_layout, adding_service_layout;
    Button btn_send_invoice;
    AutoCompleteTextView issue_actv;
    LinearLayout issue_layout;
    String  stReqId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_invoice);
        initializeUi();
        initializeValues();
        stReqId =  getIntent().getStringExtra("req_id");
        get_invoice_details();
    }

    private void get_invoice_details() {


    }

    private void initializeUi() {
        //TextView
        toolbar_title = findViewById(R.id.toolbar_title);
        txt_orderId = findViewById(R.id.txt_orderId);
        txt_customerName = findViewById(R.id.txt_customerName);
        txt_invoiceNo = findViewById(R.id.txt_invoiceNo);
        txt_invoice_date = findViewById(R.id.txt_invoice_date);
        txt_subTotal = findViewById(R.id.txt_subTotal);
        txt_coupon_price = findViewById(R.id.txt_coupon_price);
        txt_total_price = findViewById(R.id.txt_total_price);

        //ImageView
        back_img_details = findViewById(R.id.back_img_details);

        //Button
        btn_send_invoice = findViewById(R.id.btn_send_invoice);
        //EditText
        txt_service_name = findViewById(R.id.txt_service_name);
        txt_estimated_price = findViewById(R.id.txt_estimated_price);
        txt_input_num = findViewById(R.id.txt_input_num);
        txt_input_num1 = findViewById(R.id.txt_input_num1);
        et_desc = findViewById(R.id.et_desc);

        //LinearLayout
        add_service_layout = findViewById(R.id.add_service_layout);
        adding_service_layout = findViewById(R.id.adding_service_layout);
        issue_layout = findViewById(R.id.issue_layout);
        //AutoCompleteTextView
        issue_actv = findViewById(R.id.issue_actv);

    }

    private void initializeValues() {
        back_img_details.setOnClickListener(this);
        add_service_layout.setOnClickListener(this);
        btn_send_invoice.setOnClickListener(this);


        final ArrayAdapter<String> modelArray = new ArrayAdapter<String>(CreateInvoiceActivity.this, android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.issues_array));
        modelArray.setDropDownViewResource(android.R.layout.simple_list_item_1);
        issue_actv.setAdapter(modelArray);
        issue_actv.setCursorVisible(false);
        issue_actv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                issue_actv.showDropDown();
//                warrenty = String.valueOf(position);
//                System.out.println("djfdsgv " + warrenty);
            }
        });

        issue_actv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View arg0) {
                issue_actv.showDropDown();
            }
        });

        issue_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View arg0) {
                issue_actv.showDropDown();
            }
        });

    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.back_img_details:
                finish();
                break;
            case R.id.add_service_layout:
                adding_service_layout.setVisibility(View.VISIBLE);
                add_service_layout.setVisibility(View.GONE);
//                intent = new Intent(CreateInvoiceActivity.this, PaymentActivity.class);
//                startActivity(intent);
                break;
            case R.id.btn_send_invoice:
//                successAlertDialog();
                alerteDialog();

//                intent = new Intent(CreateInvoiceActivity.this, Sp_Mainactivity.class);
//                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                intent.putExtra("Activity", Sp_Mainactivity.activityName);
//                startActivity(intent);
//                finish();
                break;

        }
    }

    private void alerteDialog() {
        final Dialog dialog = new Dialog(CreateInvoiceActivity.this, R.style.MyAlertDialogTheme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.suceess_alert_dialog);
        dialog.getWindow().setBackgroundDrawableResource(
                R.drawable.bg_dialog);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);
        dialog.show();

//        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        TextView text = dialog.findViewById(R.id.text);
        text.setText("Your Order has completed  successfully and Invoice sent to client");
        TextView ok_txt = dialog.findViewById(R.id.ok_txt);
        ok_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent intent = new Intent(CreateInvoiceActivity.this, User_Home_activity.class);
                startActivity(intent);
                finish();
            }
        });

    }


    private void successAlertDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final AlertDialog alert;
        builder.setMessage("Your Order has completed  successfully and Invoice sent to client")
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        Intent intent = new Intent(CreateInvoiceActivity.this, User_Home_activity.class);
                        startActivity(intent);
                        finish();
                    }
                })/*.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        })*/;

        //Creating dialog box
        alert = builder.create();
        //Setting the title manually
//        alert.setTitle("");
        alert.show();
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}