package com.volive.osha.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.JsonElement;
import com.squareup.picasso.Picasso;
import com.volive.osha.Fragments.ContactUsFragment;
import com.volive.osha.Fragments.HomeFragment;
import com.volive.osha.R;
import com.volive.osha.helperclasses.Constant_keys;
import com.volive.osha.util_classes.API_Services;
import com.volive.osha.util_classes.PreferenceUtils;
import com.volive.osha.util_classes.Retrofit_fun;
import org.json.JSONObject;

import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class User_Home_activity extends BaseActivity implements View.OnClickListener {

    public static FragmentManager mFragmentManager;
    FragmentTransaction mFragmentTransaction;

    public static String activityName = "";

    DrawerLayout drawerLayout;
    ImageView img_menu,track_image,notify_img;
    RelativeLayout rlHOMEMenu,rl_TermsCondition,
            rl_lang,rl_MyProfileMenu,rl_MyWallet,rl_MyRequestMenu,rl_MyNotification,rl_LogoutMenu,rl_Contactus;

    TextView txt_homeMenuH,txt_language,txt_user_name,txt_user_email,txt_MyRequest,txt_MyWallet,txt_MyProfile,txt_MyNotification,count_notification,txt_Terms,txt_logoutt,txt_Contactus;
    private Intent i;
    FrameLayout containerView;
    String userId,language;
    PreferenceUtils preferenceUtils;
    public static String Map_action;
    CircleImageView ivMenuProfileImage;

    public  static TextView toolbar_title,txt_not_header_count;
    public  static  RelativeLayout rel_notification;

    String pending_request = "",pending_request_type = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_home);

        preferenceUtils = new PreferenceUtils(User_Home_activity.this);
        userId = preferenceUtils.getStringFromPreference(PreferenceUtils.USER_ID,"");
        language = preferenceUtils.getStringFromPreference(PreferenceUtils.LANGUAGE,"");
        containerView=findViewById(R.id.containerView);

//        if(getIntent().getExtras()!=null&&getIntent().getStringExtra("Action")!=null&&getIntent().getStringExtra("Action").equalsIgnoreCase("pick"))
//        {
//            mFragmentManager = getSupportFragmentManager();
//            mFragmentTransaction = mFragmentManager.beginTransaction();
//            mFragmentTransaction.replace(R.id.containerView, new HomeFragment()).commit();
//            Map_action = getIntent().getStringExtra("Action");
//            Log.e("wdsajb",Map_action);
//        }

        mFragmentManager = getSupportFragmentManager();
        mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction.replace(R.id.containerView, new HomeFragment()).commit();

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            activityName = extras.getString("Activity");
        }

        initilizeUI();
        intilizeViews();
        set_values();
        //user_check_pending();
        if (!isNetworkAvailable()) {
            showToast(getString(R.string.please_check_your_network_connection));
            return;
        } else {

            user_check_pending(userId);


        }


    }

    private void set_values()
    {

        Picasso.get().load(Constant_keys.imagebaseurl + preferenceUtils.getStringFromPreference(PreferenceUtils.IMAGE,""))
                .placeholder(android.R.drawable.progress_indeterminate_horizontal)
                .error(R.drawable.guest_user)
                .into(ivMenuProfileImage);

       /* if (preferenceUtils.getStringFromPreference(PreferenceUtils.IMAGE,"").isEmpty())
        {
            ivMenuProfileImage.setImageResource(R.drawable.guest_user);
        }
        else {
            Glide.with(getApplicationContext()).
                    load(Constant_keys.imagebaseurl + preferenceUtils.getStringFromPreference(PreferenceUtils.IMAGE,"")).into(ivMenuProfileImage);

        }*/

        txt_user_name.setText(preferenceUtils.getStringFromPreference(PreferenceUtils.UserName,""));
        txt_user_email.setText(preferenceUtils.getStringFromPreference(PreferenceUtils.Email,""));
    }

    private void initilizeUI() {

        toolbar_title =findViewById(R.id.toolbar_title);
        rel_notification = findViewById(R.id.rel_notification);
        txt_not_header_count = findViewById(R.id.txt_not_header_count);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        img_menu=findViewById(R.id.title_bar_left_menu);

        notify_img=findViewById(R.id.notify_img);
        ivMenuProfileImage = findViewById(R.id.ivMenuProfileImage);
        txt_user_email = findViewById(R.id.txt_user_email);
        txt_user_name = findViewById(R.id.txt_user_name);
        //Relative Layouts of Navigation Items
        rlHOMEMenu = (RelativeLayout) findViewById(R.id.rl_HOMEMenu);
        rl_MyProfileMenu=(RelativeLayout) findViewById(R.id.rl_MyProfileMenu);
        rl_lang = findViewById(R.id.rl_lang);
        rl_MyRequestMenu=findViewById(R.id.rl_MyRequest);
        rl_MyWallet=findViewById(R.id.rl_MyWallet);
        rl_MyNotification=findViewById(R.id.rl_MyNotification);
        rl_TermsCondition=(RelativeLayout)findViewById(R.id.rl_TermsCondition);
        rl_LogoutMenu=findViewById(R.id.rl_LogoutMenu);
        rl_Contactus=findViewById(R.id.rl_Contactus);

        //All Navigation TextViews
        txt_homeMenuH=findViewById(R.id.txt_homeMenuH);
        txt_language = findViewById(R.id.txt_language);


        txt_MyRequest=findViewById(R.id.txt_MyRequest);
        txt_MyProfile=findViewById(R.id.txt_MyProfile);
        txt_MyNotification=findViewById(R.id.txt_MyNotifiction);
        count_notification=findViewById(R.id.count_notification);
        txt_Terms=findViewById(R.id.txt_TermCondition);
        txt_logoutt=findViewById(R.id.txt_logoutt);

        txt_MyWallet=findViewById(R.id.txt_MyWallet);
        txt_Contactus=findViewById(R.id.txt_Contactus);


    }

    private void intilizeViews() {
        rel_notification.setVisibility(View.VISIBLE);
        img_menu.setOnClickListener(this);
        notify_img.setOnClickListener(this);
        rl_lang.setOnClickListener(this);
        rlHOMEMenu.setOnClickListener(this);
        rl_MyProfileMenu.setOnClickListener(this);
        rl_MyRequestMenu.setOnClickListener(this);
        rl_MyNotification.setOnClickListener(this);
        rl_TermsCondition.setOnClickListener(this);
        rl_LogoutMenu.setOnClickListener(this);
        rl_MyWallet.setOnClickListener(this);
        rl_Contactus.setOnClickListener(this);
        rl_lang.setOnClickListener(this);
    }

    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.notify_img:

                Intent intent = new Intent(User_Home_activity.this, NotificationsActivity.class);
                startActivity(intent);
                finish();

                break;

            case R.id.title_bar_left_menu:

                drawerLayout.openDrawer(GravityCompat.START);

                break;
                
            case R.id.rl_HOMEMenu:

                txt_homeMenuH.setTextColor(getResources().getColor(R.color.colorPrimary));

                txt_language.setTextColor(getResources().getColor(R.color.colorWhite));
                txt_MyRequest.setTextColor(getResources().getColor(R.color.colorWhite));
                txt_MyProfile.setTextColor(getResources().getColor(R.color.colorWhite));
                txt_MyNotification.setTextColor(getResources().getColor(R.color.colorWhite));
                txt_Terms.setTextColor(getResources().getColor(R.color.colorWhite));
                txt_logoutt.setTextColor(getResources().getColor(R.color.colorWhite));
                txt_MyWallet.setTextColor(getResources().getColor(R.color.colorWhite));
                txt_Contactus.setTextColor(getResources().getColor(R.color.colorWhite));

                rlHOMEMenu.setBackground(getResources().getDrawable(R.drawable.button_bg_white));
                rl_MyProfileMenu.setBackground(getResources().getDrawable(R.drawable.button_background));
                rl_MyRequestMenu.setBackground(getResources().getDrawable(R.drawable.button_background));
                rl_MyNotification.setBackground(getResources().getDrawable(R.drawable.button_background));
                rl_TermsCondition.setBackground(getResources().getDrawable(R.drawable.button_background));
                rl_LogoutMenu.setBackground(getResources().getDrawable(R.drawable.button_background));
                rl_MyWallet.setBackground(getResources().getDrawable(R.drawable.button_background));
                rl_Contactus.setBackground(getResources().getDrawable(R.drawable.button_background));
                toolbar_title.setText(getString(R.string.osha));

                drawerLayout.closeDrawers();

                break;

            case R.id.rl_lang:
                txt_language.setTextColor(getResources().getColor(R.color.colorPrimary));

                txt_homeMenuH.setTextColor(getResources().getColor(R.color.colorWhite));
                txt_MyRequest.setTextColor(getResources().getColor(R.color.colorWhite));
                txt_MyProfile.setTextColor(getResources().getColor(R.color.colorWhite));
                txt_MyNotification.setTextColor(getResources().getColor(R.color.colorWhite));
                txt_Terms.setTextColor(getResources().getColor(R.color.colorWhite));
                txt_logoutt.setTextColor(getResources().getColor(R.color.colorWhite));
                txt_MyWallet.setTextColor(getResources().getColor(R.color.colorWhite));
                txt_Contactus.setTextColor(getResources().getColor(R.color.colorWhite));

                rl_lang.setBackground(getResources().getDrawable(R.drawable.button_bg_white));

                rlHOMEMenu.setBackground(getResources().getDrawable(R.drawable.button_background));
                rl_MyProfileMenu.setBackground(getResources().getDrawable(R.drawable.button_background));
                rl_MyRequestMenu.setBackground(getResources().getDrawable(R.drawable.button_background));
                rl_MyNotification.setBackground(getResources().getDrawable(R.drawable.button_background));
                rl_TermsCondition.setBackground(getResources().getDrawable(R.drawable.button_background));
                rl_LogoutMenu.setBackground(getResources().getDrawable(R.drawable.button_background));
                rl_MyWallet.setBackground(getResources().getDrawable(R.drawable.button_background));
                rl_Contactus.setBackground(getResources().getDrawable(R.drawable.button_background));


                drawerLayout.closeDrawers();
                changelanguageDialog();

                break;

            case R.id.rl_MyProfileMenu:

                txt_homeMenuH.setTextColor(getResources().getColor(R.color.colorWhite));
                txt_language.setTextColor(getResources().getColor(R.color.colorWhite));
                txt_MyRequest.setTextColor(getResources().getColor(R.color.colorWhite));

                txt_MyProfile.setTextColor(getResources().getColor(R.color.colorPrimary));
                txt_MyNotification.setTextColor(getResources().getColor(R.color.colorWhite));
                txt_Terms.setTextColor(getResources().getColor(R.color.colorWhite));
                txt_logoutt.setTextColor(getResources().getColor(R.color.colorWhite));
                txt_MyWallet.setTextColor(getResources().getColor(R.color.colorWhite));
                txt_Contactus.setTextColor(getResources().getColor(R.color.colorWhite));

                drawerLayout.closeDrawers();

                rlHOMEMenu.setBackground(getResources().getDrawable(R.drawable.button_background));
                rl_lang.setBackground(getResources().getDrawable(R.drawable.button_background));
                rl_MyProfileMenu.setBackground(getResources().getDrawable(R.drawable.button_bg_white));
                rl_MyRequestMenu.setBackground(getResources().getDrawable(R.drawable.button_background));
                rl_MyNotification.setBackground(getResources().getDrawable(R.drawable.button_background));
                rl_TermsCondition.setBackground(getResources().getDrawable(R.drawable.button_background));
                rl_LogoutMenu.setBackground(getResources().getDrawable(R.drawable.button_background));
                rl_MyWallet.setBackground(getResources().getDrawable(R.drawable.button_background));
                rl_Contactus.setBackground(getResources().getDrawable(R.drawable.button_background));

                 i = new Intent(User_Home_activity.this, UserProfileActivity.class);
                 startActivity(i);


                break;

            case R.id.rl_MyWallet:
                txt_language.setTextColor(getResources().getColor(R.color.colorWhite));
                txt_homeMenuH.setTextColor(getResources().getColor(R.color.colorWhite));
                txt_MyRequest.setTextColor(getResources().getColor(R.color.colorWhite));
                txt_MyProfile.setTextColor(getResources().getColor(R.color.colorWhite));
                txt_MyNotification.setTextColor(getResources().getColor(R.color.colorWhite));
                txt_Terms.setTextColor(getResources().getColor(R.color.colorWhite));
                txt_logoutt.setTextColor(getResources().getColor(R.color.colorWhite));
                txt_MyWallet.setTextColor(getResources().getColor(R.color.colorPrimary));
                txt_Contactus.setTextColor(getResources().getColor(R.color.colorWhite));

                drawerLayout.closeDrawers();

                rlHOMEMenu.setBackground(getResources().getDrawable(R.drawable.button_background));
                rl_lang.setBackground(getResources().getDrawable(R.drawable.button_background));
                rl_MyProfileMenu.setBackground(getResources().getDrawable(R.drawable.button_background));
                rl_MyRequestMenu.setBackground(getResources().getDrawable(R.drawable.button_background));
                rl_MyNotification.setBackground(getResources().getDrawable(R.drawable.button_background));
                rl_TermsCondition.setBackground(getResources().getDrawable(R.drawable.button_background));
                rl_LogoutMenu.setBackground(getResources().getDrawable(R.drawable.button_background));
                rl_MyWallet.setBackground(getResources().getDrawable(R.drawable.button_bg_white));
                rl_Contactus.setBackground(getResources().getDrawable(R.drawable.button_background));

                i = new Intent(User_Home_activity.this, WalletActivity.class);
                startActivity(i);


                break;

            case R.id.rl_Contactus:
                txt_language.setTextColor(getResources().getColor(R.color.colorWhite));
                txt_homeMenuH.setTextColor(getResources().getColor(R.color.colorWhite));
                txt_MyRequest.setTextColor(getResources().getColor(R.color.colorWhite));
                txt_MyProfile.setTextColor(getResources().getColor(R.color.colorWhite));
                txt_MyNotification.setTextColor(getResources().getColor(R.color.colorWhite));
                txt_Terms.setTextColor(getResources().getColor(R.color.colorWhite));
                txt_logoutt.setTextColor(getResources().getColor(R.color.colorWhite));
                txt_MyWallet.setTextColor(getResources().getColor(R.color.colorWhite));
                txt_Contactus.setTextColor(getResources().getColor(R.color.colorPrimary));

                drawerLayout.closeDrawers();

                rlHOMEMenu.setBackground(getResources().getDrawable(R.drawable.button_background));
                rl_lang.setBackground(getResources().getDrawable(R.drawable.button_background));
                rl_MyProfileMenu.setBackground(getResources().getDrawable(R.drawable.button_background));
                rl_MyRequestMenu.setBackground(getResources().getDrawable(R.drawable.button_background));
                rl_MyNotification.setBackground(getResources().getDrawable(R.drawable.button_background));
                rl_TermsCondition.setBackground(getResources().getDrawable(R.drawable.button_background));
                rl_LogoutMenu.setBackground(getResources().getDrawable(R.drawable.button_background));
                rl_MyWallet.setBackground(getResources().getDrawable(R.drawable.button_background));
                rl_Contactus.setBackground(getResources().getDrawable(R.drawable.button_bg_white));
                /* i = new Intent(User_Home_activity.this, ContactUsActivity.class);
                startActivity(i);*/
                mFragmentManager = getSupportFragmentManager();
                mFragmentTransaction = mFragmentManager.beginTransaction();
                mFragmentTransaction.replace(R.id.containerView, new ContactUsFragment()).commit();
                break;
            case R.id.rl_MyRequest:
                txt_language.setTextColor(getResources().getColor(R.color.colorWhite));
                txt_homeMenuH.setTextColor(getResources().getColor(R.color.colorWhite));
                txt_MyRequest.setTextColor(getResources().getColor(R.color.colorPrimary));
                rl_MyRequestMenu.setBackground(getResources().getDrawable(R.drawable.button_bg_white));
                txt_MyProfile.setTextColor(getResources().getColor(R.color.colorWhite));
                txt_MyNotification.setTextColor(getResources().getColor(R.color.colorWhite));
                txt_Terms.setTextColor(getResources().getColor(R.color.colorWhite));
                txt_logoutt.setTextColor(getResources().getColor(R.color.colorWhite));
                txt_MyWallet.setTextColor(getResources().getColor(R.color.colorWhite));
                txt_Contactus.setTextColor(getResources().getColor(R.color.colorWhite));

                rl_lang.setBackground(getResources().getDrawable(R.drawable.button_background));
                rlHOMEMenu.setBackground(getResources().getDrawable(R.drawable.button_background));
                rl_MyProfileMenu.setBackground(getResources().getDrawable(R.drawable.button_background));
                rl_MyRequestMenu.setBackground(getResources().getDrawable(R.drawable.button_bg_white));
                rl_MyNotification.setBackground(getResources().getDrawable(R.drawable.button_background));
                rl_TermsCondition.setBackground(getResources().getDrawable(R.drawable.button_background));
                rl_LogoutMenu.setBackground(getResources().getDrawable(R.drawable.button_background));
                rl_MyWallet.setBackground(getResources().getDrawable(R.drawable.button_background));
                rl_Contactus.setBackground(getResources().getDrawable(R.drawable.button_background));


                drawerLayout.closeDrawers();

                 i = new Intent(User_Home_activity.this, MyRequestActivity.class);
                startActivity(i);
                finish();

                break;

            case R.id.rl_MyNotification:
                txt_language.setTextColor(getResources().getColor(R.color.colorWhite));
                txt_homeMenuH.setTextColor(getResources().getColor(R.color.colorWhite));
                txt_MyRequest.setTextColor(getResources().getColor(R.color.colorWhite));
                txt_MyProfile.setTextColor(getResources().getColor(R.color.colorWhite));
                txt_MyNotification.setTextColor(getResources().getColor(R.color.colorPrimary));
                rl_MyNotification.setBackground(getResources().getDrawable(R.drawable.button_bg_white));
                txt_Terms.setTextColor(getResources().getColor(R.color.colorWhite));
                txt_logoutt.setTextColor(getResources().getColor(R.color.colorWhite));
                txt_MyWallet.setTextColor(getResources().getColor(R.color.colorWhite));
                txt_Contactus.setTextColor(getResources().getColor(R.color.colorWhite));

                rl_lang.setBackground(getResources().getDrawable(R.drawable.button_background));
                rlHOMEMenu.setBackground(getResources().getDrawable(R.drawable.button_background));
                rl_MyProfileMenu.setBackground(getResources().getDrawable(R.drawable.button_background));
                rl_MyRequestMenu.setBackground(getResources().getDrawable(R.drawable.button_background));
                rl_MyNotification.setBackground(getResources().getDrawable(R.drawable.button_bg_white));
                rl_TermsCondition.setBackground(getResources().getDrawable(R.drawable.button_background));
                rl_LogoutMenu.setBackground(getResources().getDrawable(R.drawable.button_background));
                rl_MyWallet.setBackground(getResources().getDrawable(R.drawable.button_background));
                rl_Contactus.setBackground(getResources().getDrawable(R.drawable.button_background));


                drawerLayout.closeDrawers();

                i = new Intent(User_Home_activity.this, NotificationsActivity.class);
                startActivity(i);
                finish();

                break;
            case R.id.rl_TermsCondition:
                txt_language.setTextColor(getResources().getColor(R.color.colorWhite));
                txt_homeMenuH.setTextColor(getResources().getColor(R.color.colorWhite));
                txt_MyRequest.setTextColor(getResources().getColor(R.color.colorWhite));
                txt_MyProfile.setTextColor(getResources().getColor(R.color.colorWhite));
                txt_MyNotification.setTextColor(getResources().getColor(R.color.colorWhite));
                txt_Terms.setTextColor(getResources().getColor(R.color.colorPrimary));
                rl_TermsCondition.setBackground(getResources().getDrawable(R.drawable.button_bg_white));
                txt_logoutt.setTextColor(getResources().getColor(R.color.colorWhite));
                txt_MyWallet.setTextColor(getResources().getColor(R.color.colorWhite));
                txt_Contactus.setTextColor(getResources().getColor(R.color.colorWhite));

                rl_lang.setBackground(getResources().getDrawable(R.drawable.button_background));
                rlHOMEMenu.setBackground(getResources().getDrawable(R.drawable.button_background));
                rl_MyProfileMenu.setBackground(getResources().getDrawable(R.drawable.button_background));
                rl_MyRequestMenu.setBackground(getResources().getDrawable(R.drawable.button_background));
                rl_MyNotification.setBackground(getResources().getDrawable(R.drawable.button_background));
                rl_TermsCondition.setBackground(getResources().getDrawable(R.drawable.button_bg_white));
                rl_LogoutMenu.setBackground(getResources().getDrawable(R.drawable.button_background));
                rl_MyWallet.setBackground(getResources().getDrawable(R.drawable.button_background));
                rl_Contactus.setBackground(getResources().getDrawable(R.drawable.button_background));

                drawerLayout.closeDrawers();

                i = new Intent(User_Home_activity.this, TermsAndConditionsActivity.class);
                startActivity(i);
                finish();

                break;
                
            case R.id.rl_LogoutMenu:
                txt_language.setTextColor(getResources().getColor(R.color.colorWhite));
                txt_homeMenuH.setTextColor(getResources().getColor(R.color.colorWhite));
                txt_MyRequest.setTextColor(getResources().getColor(R.color.colorWhite));
                txt_MyProfile.setTextColor(getResources().getColor(R.color.colorWhite));
                txt_MyNotification.setTextColor(getResources().getColor(R.color.colorWhite));
                txt_Terms.setTextColor(getResources().getColor(R.color.colorWhite));
                txt_logoutt.setTextColor(getResources().getColor(R.color.colorPrimary));
                rl_LogoutMenu.setBackground(getResources().getDrawable(R.drawable.button_bg_white));
                txt_MyWallet.setTextColor(getResources().getColor(R.color.colorWhite));
                txt_Contactus.setTextColor(getResources().getColor(R.color.colorWhite));

                rl_lang.setBackground(getResources().getDrawable(R.drawable.button_background));
                rlHOMEMenu.setBackground(getResources().getDrawable(R.drawable.button_background));
                rl_MyProfileMenu.setBackground(getResources().getDrawable(R.drawable.button_background));
                rl_MyRequestMenu.setBackground(getResources().getDrawable(R.drawable.button_background));
                rl_MyNotification.setBackground(getResources().getDrawable(R.drawable.button_background));
                rl_TermsCondition.setBackground(getResources().getDrawable(R.drawable.button_background));
                rl_LogoutMenu.setBackground(getResources().getDrawable(R.drawable.button_bg_white));
                rl_Contactus.setBackground(getResources().getDrawable(R.drawable.button_background));


                preferenceUtils.logOut();
                preferenceUtils.saveBoolean(PreferenceUtils.LOGIN_TYPE, false);
                preferenceUtils.saveString(PreferenceUtils.USER_ID, "");

                drawerLayout.closeDrawers();
                logout_service();
               /* i = new Intent(User_Home_activity.this, LoginActivty.class);
                startActivity(i);
                finish();*/

                break;

        }
    }

    private void logout_service() {


            final API_Services service = Retrofit_fun.getClient().create(API_Services.class);
            Call<JsonElement> call_logout = null;
            call_logout = service.logout(Constant_keys.API_KEY, language, userId);
            final ProgressDialog progressdialog;
            progressdialog = new ProgressDialog(User_Home_activity.this);
            progressdialog.setCancelable(false);
            progressdialog.setMessage(getString(R.string.loading));
            progressdialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressdialog.show();
            call_logout.enqueue(new Callback<JsonElement>() {
                @Override
                public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                    progressdialog.dismiss();
                    Log.e("ajkldhn", response.body().toString());
                    if (response.isSuccessful()) {
                        try {
                            JSONObject jsonObject = new JSONObject(response.body().toString());
                            int status = jsonObject.getInt("status");
                            String message = jsonObject.getString("message");
                            if (status == 1) {

                                showToast(message);
                                Intent intent = new Intent(User_Home_activity.this, LoginActivty.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                drawerLayout.closeDrawers();
                            }
                            else {
                                showToast(message);
                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }

                @Override
                public void onFailure(Call<JsonElement> call, Throwable t) {
                    Log.e("Eror:" ,call.toString());
                    Log.e("Eror:",t.toString());

                }
            });


        }


    private void changelanguageDialog() {
        final Dialog dialog = new Dialog(User_Home_activity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.change_langauge_dialog);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);

        Button btncancel = (Button) dialog.findViewById(R.id.btncancel);
        Button btnok = (Button) dialog.findViewById(R.id.btnapply);
        final RadioGroup radioGroup = (RadioGroup) dialog.findViewById(R.id.language_group);
        final RadioButton engbtn = (RadioButton) dialog.findViewById(R.id.englishButton);
        final RadioButton arabicbtn = (RadioButton) dialog.findViewById(R.id.arabicButton);
        String getLang = preferenceUtils.getStringFromPreference(PreferenceUtils.LANGUAGE, "");

        if (getLang.equalsIgnoreCase("en")) {
            engbtn.setChecked(true);
            arabicbtn.setChecked(false);

        } else {
            engbtn.setChecked(false);
            arabicbtn.setChecked(true);

        }

        btnok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int selectedId = radioGroup.getCheckedRadioButtonId();
                RadioButton radioButton = (RadioButton) dialog.findViewById(selectedId);
                if (radioButton != null) {
                    if (engbtn.isChecked()) {
//                        Resources res = getApplicationContext().getResources();
//                        DisplayMetrics dm = res.getDisplayMetrics();
//                        android.content.res.Configuration conf = res.getConfiguration();
                       /* Locale locale = new Locale("en");
                        Locale.setDefault(locale);
                        Configuration config = new Configuration();
                        config.locale = locale;
                        getApplicationContext().getResources().updateConfiguration(config,
                                getApplicationContext().getResources().getDisplayMetrics());*/
                        String languageToLoad = "en"; // your language
                        Locale locale = new Locale(languageToLoad);
                        Locale.setDefault(locale);
                        Configuration config = new Configuration();
                        config.locale = locale;
                        getBaseContext().getResources().updateConfiguration(config,
                                getBaseContext().getResources().getDisplayMetrics());
                        preferenceUtils.setLanguage("en");
                       // preferenceUtils.saveString(PreferenceUtils.LANGUAGE, "en");
                       /* String lang = "en";
                        Intent intent = new Intent(getApplicationContext(), User_Home_activity.class);
                        startActivity(intent);
                        dialog.dismiss();*/

                        preferenceUtils.saveString(PreferenceUtils.LANGUAGE, "en");
                        String lang = "en";

                        if (preferenceUtils.getStringFromPreference(PreferenceUtils.USER_ID, "").isEmpty()) {
                            userId = "";
                            Intent intent = new Intent(getApplicationContext(), User_Home_activity.class);
                            startActivity(intent);
                            dialog.dismiss();
                        } else {
                          /*  strUser_Id = preferenceUtils.getStringFromPreference(PreferenceUtils.USER_ID, "");
                            Log.e("kfds", strUser_Id);*/
                            languge_change(lang);
                            Intent intent = new Intent(getApplicationContext(), User_Home_activity.class);
                            startActivity(intent);
                            dialog.dismiss();
                        }

                        /*if (preferenceUtils.getStringFromPreference(PreferenceUtils.USER_ID, "").isEmpty()) {
                           // strUser_Id = "";
                            Intent intent = new Intent(getApplicationContext(), Sp_HomeActivity.class);
                            startActivity(intent);
                            dialog.dismiss();
                        } else {
                          *//*  strUser_Id = preferenceUtils.getStringFromPreference(PreferenceUtils.USER_ID, "");
                            Log.e("kfds", strUser_Id);*//*
                            languge_change(lang);
                            Intent intent = new Intent(getApplicationContext(), Sp_HomeActivity.class);
                            startActivity(intent);
                            dialog.dismiss();
                        }*/


                    } else if (arabicbtn.isChecked()) {
//                        Resources res = getApplicationContext().getResources();
//                        DisplayMetrics dm = res.getDisplayMetrics();
//                        android.content.res.Configuration conf = res.getConfiguration();

                        String languageToLoad = "ar"; // your language
                        Locale locale = new Locale(languageToLoad);
                        Locale.setDefault(locale);
                        Configuration config = new Configuration();
                        config.locale = locale;
                        getBaseContext().getResources().updateConfiguration(config,
                                getBaseContext().getResources().getDisplayMetrics());
                        preferenceUtils.setLanguage("ar");
                      /*  Locale locale = new Locale("ar");
                        Locale.setDefault(locale);
                        Configuration config = new Configuration();
                        config.locale = locale;
                        getApplicationContext().getResources().updateConfiguration(config,
                                getApplicationContext().getResources().getDisplayMetrics());*/

                        preferenceUtils.saveString(PreferenceUtils.LANGUAGE, "ar");
                        String lang = "ar";

                        if (preferenceUtils.getStringFromPreference(PreferenceUtils.USER_ID, "").isEmpty()) {
                            userId = "";
                            Intent intent = new Intent(getApplicationContext(), User_Home_activity.class);
                            startActivity(intent);
                            dialog.dismiss();
                        } else {
                          /*  strUser_Id = preferenceUtils.getStringFromPreference(PreferenceUtils.USER_ID, "");
                            Log.e("kfds", strUser_Id);*/
                            languge_change(lang);
                            Intent intent = new Intent(getApplicationContext(), User_Home_activity.class);
                            startActivity(intent);
                            dialog.dismiss();
                        }


                        /*preferenceUtils.saveString(PreferenceUtils.LANGUAGE, "ar");
                        String lang = "ar";
                        Intent intent = new Intent(getApplicationContext(), User_Home_activity.class);
                        startActivity(intent);
                        dialog.dismiss();
                        if (preferenceUtils.getStringFromPreference(PreferenceUtils.USER_ID, "").isEmpty()) {
                            userId = "";
                            Intent intent = new Intent(getApplicationContext(), User_Home_activity.class);
                            startActivity(intent);
                            dialog.dismiss();
                        } else {
                            userId = preferenceUtils.getStringFromPreference(PreferenceUtils.USER_ID, "");
                            Log.e("kfds", userId);
                            languge_change(lang);
                            Intent intent = new Intent(getApplicationContext(), User_Home_activity.class);
                            startActivity(intent);
                            dialog.dismiss();
                        }

                        dialog.dismiss();*/

                    }
                }
                dialog.dismiss();
            }
        });
        btncancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    private void languge_change(String lang) {
        final API_Services service = Retrofit_fun.getClient().create(API_Services.class);
        Call<JsonElement> call_logout = null;
        call_logout = service.change_lang(Constant_keys.API_KEY, lang, userId);
        final ProgressDialog progressdialog;
        progressdialog = new ProgressDialog(User_Home_activity.this);
        progressdialog.setCancelable(false);
        progressdialog.setMessage(getString(R.string.loading));
        progressdialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressdialog.show();
        call_logout.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                progressdialog.dismiss();

                if (response.isSuccessful()) {
                    Log.e("ajkldhn", response.body().toString());
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().toString());
                        int status = jsonObject.getInt("status");
                        if (status == 1) {

                            //showToast(jsonObject.getString("message"));
                        }
                        else {
                            showToast(jsonObject.getString("message"));
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {

            }
        });


    }
    private void user_check_pending(String stUserId)
    {
        final API_Services services = Retrofit_fun.getClient().create(API_Services.class);
        Call<JsonElement> call_req_details = null;
        call_req_details = services.get_pending(Constant_keys.API_KEY,preferenceUtils.getStringFromPreference(PreferenceUtils.LANGUAGE,""), stUserId);
       /* final ProgressDialog progressDialog = new ProgressDialog(User_Home_activity.this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(getResources().getString(R.string.loading));
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();*/
        call_req_details.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(@NonNull Call<JsonElement> call, @NonNull Response<JsonElement> response)
            {
               // progressDialog.dismiss();

                if (response.isSuccessful()) {
                    try {
                        Log.e("data_eror",response.body().toString());
                        JSONObject jsonObject = new JSONObject(response.body().toString());

                        int status = jsonObject.getInt("status");

                        if (status == 1)
                        {
                            // String mesaage = jsonObject.getString("message");
                            //JSONObject json_data  = jsonObject.getJSONObject("request_details");
                            if (jsonObject.has("request_id"))
                            {
                                pending_request = jsonObject.getString("request_id");
                                pending_request_type = jsonObject.getString("pending_request_type");
                            }
                            else {
                                pending_request = "0";
                            }

                            if (!pending_request.equalsIgnoreCase("0"))
                            {
                                Intent  intent = new Intent(User_Home_activity.this, RequestDetailsActivity.class);
                                intent.putExtra("reqID",pending_request);
                                intent.putExtra("req_type",pending_request_type);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);

                            }
                            else {

                              /*  Intent a = new Intent(getActivity(), User_Home_activity.class);
                                Bundle bundle = new Bundle();
                                bundle.putString("Activity", "login");
                                a.putExtras(bundle);
                                //  a.putExtra("Activity","login");
                                startActivity(a);*/
                            }

                        }

                        else {

                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public void onFailure(@NonNull Call<JsonElement> call, @NonNull Throwable t) {
               // progressDialog.dismiss();
                Log.e("Error:" ,call.toString());
                Log.e("Eror:",t.toString());

            }
        });

    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();
        finishAffinity();
       /* Intent i=new Intent(Intent.ACTION_MAIN);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        finish();*/
    }

}
