package com.volive.osha.activities;

import android.Manifest;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;
import com.volive.osha.R;
import com.volive.osha.helperclasses.Offer_status;
import com.volive.osha.util_classes.Chating_screen_status;
import com.volive.osha.util_classes.PreferenceUtils;
import com.volive.osha.util_classes.SessionManagement;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;
import static android.os.Build.VERSION_CODES.M;

public class Splash_screen extends BaseActivity {
    private static final int MY_PERMISSIONS_REQUEST = 1;
    private static int SPLASH_TIME_OUT = 3000;
    PreferenceUtils preferenceUtils;
    SessionManagement sessionManagement;
    String langauage;
    ImageView image_splash;
    //ViewPager img_splash;
    SharedPreferences prefs = null;
    private Boolean isFirstRun;
    Handler handler;
    String  strSelectLanguage;
    JSONObject object = new JSONObject();
    String freelancer_id, reciever_image, request_id, order_date, user_id,provider_id, message, image, auth_level, sender_id, receiver_Id;
    String type = "", title = "";


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        preferenceUtils = new PreferenceUtils(this);
        sessionManagement = new SessionManagement(this);
        strSelectLanguage = preferenceUtils.getStringFromPreference(PreferenceUtils.LANGUAGE, "");
       // preferenceUtils.saveString(PreferenceUtils.LANGUAGE,"en");
        setLanguageConversion();
        handler=new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (M <= Build.VERSION.SDK_INT) {
                    if (checkPermissions()) {

                        if(!preferenceUtils.getbooleanFromPreference(PreferenceUtils.LOGIN_TYPE,false)  )
                        {
                            Intent intent = new Intent(getApplicationContext(), LoginActivty.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                        }

                        else
                         {
                            Intent intent = new Intent(getApplicationContext(), User_Home_activity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                         }

                        // TODO Auto-generated method stub
                    }
                }
                else
                    {

                        Toast.makeText(getApplicationContext(),getString(R.string.please_take_permission),Toast.LENGTH_SHORT).show();
                  /*  Intent intent = new Intent(getApplication(), Splash_screen.class);
                    //  intent.putExtra("array", itemList);
                    startActivity(intent);*/
                }


                //data
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    // Create channel to show notifications.
                    String channelId = getString(R.string.default_notification_channel_id);
                    String channelName = getString(R.string.default_notification_channel_name);
                    NotificationManager notificationManager =
                            getSystemService(NotificationManager.class);
                    notificationManager.createNotificationChannel(new NotificationChannel(channelId,
                            channelName, NotificationManager.IMPORTANCE_LOW));
                }
                if (getIntent().getExtras() != null) {
                    for (String key : getIntent().getExtras().keySet()) {
                        Object value = getIntent().getExtras().get(key);
                        Log.e("notification dta", "Key: " + key + " Value: " + value);
                        try {
                            object.put(key, value);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    try {
                       type = object.getString("notification_type");
                        if (type.equalsIgnoreCase("new_request")) {
                            title = object.getString("notification_title_en");
                            Intent intent = new Intent(getApplicationContext(), MyRequestActivity.class);
                            startActivity(intent);
                        }
                        else if (type.equalsIgnoreCase("offer_accepted")) {
                            title = object.getString("notification_title_en");

                            if (Offer_status.isActivityVisible()) {
                                Intent intent;
                                intent = new Intent("track");
                                intent.putExtra("req_id", object.getString("request_id"));
                                intent.putExtra("action", "0");
                                LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(Splash_screen.this);
                                localBroadcastManager.sendBroadcast(intent);
                            }
                            else {
                                Intent intent = new Intent(getApplicationContext(), TrackActivity.class);
                                intent.putExtra("req_id", object.getString("request_id"));
                                intent.putExtra("action", "0");
                                startActivity(intent);
                            }
                        }
                        else if (type.equalsIgnoreCase("cancel_request")) {
                            Intent intent = new Intent(getApplicationContext(), MyRequestActivity.class);
                            startActivity(intent);
                        }
                        else if (type.equalsIgnoreCase("new_offer")) {
                            title = object.getString("notification_title_en");
                            if (Offer_status.isActivityVisible()) {
                                Intent intent = new Intent();
                                intent = new Intent("com.volive.osha_FCM-OFFER");
                                intent.putExtra("reqID", object.getString("request_id"));
                                intent.putExtra("req_type", object.getString("request_type"));
                                LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(Splash_screen.this);
                                localBroadcastManager.sendBroadcast(intent);

                            }else {
                                Intent intent = new Intent(getApplicationContext(), RequestDetailsActivity.class);
                                intent.putExtra("reqID", object.getString("request_id"));
                                intent.putExtra("req_type", object.getString("request_type"));
                                startActivity(intent);
                            }
                        }
                        else if (type.equalsIgnoreCase("work_started")) {
                            if (Offer_status.isActivityVisible()) {
                                Intent intent;
                                intent = new Intent("track");
                                intent.putExtra("req_id", object.getString("request_id"));
                                intent.putExtra("action", "0");
                                LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(Splash_screen.this);
                                localBroadcastManager.sendBroadcast(intent);
                            } else {
                                Intent intent = new Intent(getApplicationContext(), TrackActivity.class);
                                intent.putExtra("req_id", object.getString("request_id"));
                                intent.putExtra("action", "0");
                                startActivity(intent);
                            }
                        }
                        else if (type.equalsIgnoreCase("invoice_generated")) {
                            Intent intent = new Intent(getApplicationContext(), InvoiceDetailsActivity.class);
                            intent.putExtra("req_id",object.getString("request_id"));
                            startActivity(intent);
                        }
                        else if (type.equalsIgnoreCase("request_completed")) {

                            Intent intent = new Intent(getApplicationContext(), MyRequestActivity.class);
                            startActivity(intent);
                        }
                        else if (object.getString("notification_type").equalsIgnoreCase("new_message")) {
                            title = object.getString("notification_title_en");
                            sender_id = object.getString("sender_id");
                            receiver_Id = object.getString("receiver_id");
                            request_id = object.getString("request_id");
                            if (Chating_screen_status.isActivityVisible()) {
                                Intent intent = new Intent();
                                intent = new Intent("chat_message");
                                intent.putExtra("sender", sender_id);
                                intent.putExtra("rec_id", receiver_Id);
                               // intent.putExtra("rcv_image", preferenceUtils.getStringFromPreference(PreferenceUtils.IMAGE, ""));
                                intent.putExtra("rcv_image",object.getString("image"));
                                intent.putExtra("request", request_id);
                                LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(Splash_screen.this);
                                localBroadcastManager.sendBroadcast(intent);

                            } else {
                                Intent intent = new Intent(getApplicationContext(), ChatActivity.class);
                                intent.putExtra("sender", receiver_Id);
                                intent.putExtra("rec_id", sender_id);
                                intent.putExtra("rcv_image",object.getString("image"));
                               // intent.putExtra("rcv_image", preferenceUtils.getStringFromPreference(PreferenceUtils.IMAGE, ""));
                                intent.putExtra("request", request_id);
                                startActivity(intent);
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }



                return;
            }

        },3000);

    }




    private boolean checkPermissions() {
        final Context context = Splash_screen.this;
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_NETWORK_STATE) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(context, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(Splash_screen.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CALL_PHONE, Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_NETWORK_STATE, Manifest.permission.SEND_SMS}, MY_PERMISSIONS_REQUEST);
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST:
                int i = 0;
                if (grantResults != null && grantResults.length > 0) {
                    for (i = 0; i < grantResults.length; i++) {
                        if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                            break;
                        }
                    }
                    if (i == grantResults.length) {
                        Intent intent = new Intent(getApplication(), LoginActivty.class);
                        //  intent.putExtra("array", itemList);
                        startActivity(intent);
                    } else {
                        checkPermissions();
                    }
                }
                break;
        }
    }

    public void setLanguageConversion() {
        if (strSelectLanguage == null || strSelectLanguage.isEmpty()) {
            String languageToLoad = "ar"; // your language
            Locale locale = new Locale(languageToLoad);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());
            preferenceUtils.setLanguage("ar");
            preferenceUtils.saveString(PreferenceUtils.LANGUAGE,"ar");

        } else if (strSelectLanguage.equalsIgnoreCase("en")) {
            String languageToLoad = "en"; // your language
            Locale locale = new Locale(languageToLoad);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());
            preferenceUtils.setLanguage("en");
            preferenceUtils.saveString(PreferenceUtils.LANGUAGE,"en");

        } else if (strSelectLanguage.equalsIgnoreCase("ar")) {
            String languageToLoad = "ar"; // your language
            Locale locale = new Locale(languageToLoad);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());
            preferenceUtils.setLanguage("ar");
            preferenceUtils.saveString(PreferenceUtils.LANGUAGE,"ar");

        } else {
            String languageToLoad = "en"; // your language
            Locale locale = new Locale(languageToLoad);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());
            preferenceUtils.setLanguage("en");
            preferenceUtils.saveString(PreferenceUtils.LANGUAGE,"en");
        }
    }







































    /*public class Splash_adapter extends RecyclerView.Adapter<Splash_adapter.ViewHolder> {
        Context context;

        ArrayList<ParentListModel> parentListModels;
        int row_index ;

        public Splash_adapter(Context context, ArrayList<ParentListModel> parentListModels) {
            this.context = context;
            this.parentListModels = parentListModels;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_splash, parent, false);
            return new ViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {


            final ParentListModel cat_model = parentListModels.get(position);
            Glide.with(context).load(parentListModels.get(position).getImages_splash()).into(holder.img_splash_item);

        }
        @Override
        public int getItemCount() {
            return parentListModels.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            ImageView img_splash_item;


            public ViewHolder(View view) {
                super(view);


                img_splash_item = view.findViewById(R.id.img_splash_item);


            }
        }
    }*/

}
















/*if (!isNetworkAvailable()) {
            showToast(getString(R.string.please_check_your_network_connection));
            return;
        } else {
            ServiceCall();
            // sessionManagement.setApp_runFirst("NO");
            //ServiceCall();
            //  }
            //  status_user_id = preferenceUtils.getStringFromPreference(PreferenceUtils.USERID, "");
            //setLanguageConversion();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (sessionManagement.getApp_runFirst().equalsIgnoreCase("first")) {
                        if (M <= Build.VERSION.SDK_INT) {
                                if (checkPermissions()) {
                                    Intent intent = new Intent(getApplication(), Slider_activity.class);
                                    intent.putExtra("array", itemList);
                                    startActivity(intent);
                                    // TODO Auto-generated method stub
                                }
                            } else {
                                Intent intent = new Intent(getApplication(), Slider_activity.class);
                                intent.putExtra("array", itemList);
                                startActivity(intent);
                            }
                            return;
                        } else {
                            sessionManagement.setApp_runFirst("NO");

                        Intent intent = new Intent(getApplication(), HomeActivity.class);
                        startActivity(intent);
                        finish();

                    }




                }
            }, SPLASH_TIME_OUT);

        }
    }*/
    /*@Override
    protected void onResume() {
        super.onResume();

        if (prefs.getBoolean("firstrun", true)) {
            // Do first run stuff here then set 'firstrun' as false
            //strat  DataActivity beacuase its your app first run
            // using the following line to edit/commit prefs
            prefs.edit().putBoolean("firstrun", false).commit();
            startActivity(new Intent(Splash_screen.this , Slider_activity.class));
            finish();
        }
        else {
            startActivity(new Intent(Splash_screen.this , HomeActivity.class));
            finish();
        }
    }*/




































//runnable class



/*  for (int i = 0; i < 40; i++)
                    System.out.println("launch----" + i);*/
/* if (sessionManagement.getApp_runFirst().equalsIgnoreCase("first")) {*/

//                if (checkPermissions()) {
////                    if (!isNetworkAvailable()) {
////                        showToast(getString(R.string.please_check_your_network_connection));
////                        return;
////                    } else {
////                        // sessionManagement.setApp_runFirst("NO");
////                        ServiceCall();
////                    }
//                    Intent intent = new Intent(getApplication(), Slider_activity.class);
//                    intent.putExtra("array", itemList);
//                    startActivity(intent);
//
//                } else {
//                    checkPermissions();
//                }

                    /*if (status_user_id.isEmpty()) {
                        Intent reg = new Intent(Splash_screen.this, IntroSlider.class);
                        startActivity(reg);
                        finish();
                    } else {*/
                    /*Intent reg = new Intent(Splash_screen.this, HomeActivity.class);
                    startActivity(reg);
                    finish();*/
                    /*}

                }*/


//after at service list
/*   mAdapter=new OffersAdapter(itemList, MedicalOffers.this);

                                    recyclerView.setAdapter(mAdapter);
*/

//                            Intent i = new Intent(getApplication(), Slider_activity.class);
//                            i.putExtra("array", itemList);
//                            startActivity(i);

//   overridePendingTransition(R.anim.slidein, R.anim.slideout);
//                            finish();
                            /*Intent i = new Intent(SkipingActivity.this, AddressManagerActivity.class);
                            startActivity(i);
                            finish();*/
















/* callRetrofit.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                progressdialog.dismiss();
                if (response.isSuccessful()) {
                    String searchResponse = response.body().toString();
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().toString());
                        Log.e("home response", "" + response.body());
                        int status = jsonObject.getInt("status");
//                        String message = jsonObject.optString("message");
//                        showToast(message);
                        if (status == 1) {
                            JSONArray datalistArray = jsonObject.optJSONArray("adds");
                            if (datalistArray != null && datalistArray.length() > 0) {
                                for (int i = 0; i < datalistArray.length(); i++) {
                                    JSONObject item = datalistArray.getJSONObject(i);
                                    parentListModel = new ParentListModel();
                                    parentListModel.setId(item.optString("id"));
                                    parentListModel.setTitle(item.optString("heading"));
                                    parentListModel.setImages(item.optString("image"));
                                    parentListModel.setDescription(item.optString("text"));
                                    parentListModel.setBase_url(jsonObject.optString("base_path"));
                                    itemList.add(parentListModel);
                                }
                            }
                        }*/