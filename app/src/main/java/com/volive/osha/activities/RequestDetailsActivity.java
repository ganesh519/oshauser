package com.volive.osha.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonElement;
import com.squareup.picasso.Picasso;
import com.volive.osha.R;
import com.volive.osha.adapters.ReqSpAdapter;
import com.volive.osha.helperclasses.Constant_keys;
import com.volive.osha.helperclasses.Offer_status;
import com.volive.osha.helperclasses.Validation_Class;
import com.volive.osha.models.ReqSPModel_list;
import com.volive.osha.util_classes.API_Services;
import com.volive.osha.util_classes.PreferenceUtils;
import com.volive.osha.util_classes.Retrofit_fun;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RequestDetailsActivity extends BaseActivity implements View.OnClickListener {

    public static boolean isRequest = false;
    LinearLayout linear_details, del_layout, desc_layout, items_layout, passenger_layout, ll_comments;
    ImageView image_up, image_down, back_img_request, img_car_pic;
    RecyclerView providers_Rview;
    LinearLayoutManager layoutManager;
    ArrayList<ReqSPModel_list> spModelArrayList = new ArrayList<>();
    ReqSpAdapter reqSpAdapter;
    PreferenceUtils preferenceUtils;
    String language, stUserID, stReqId, stReqType;
    Validation_Class validate;
    TextView txt_order_req, txt_comments, txt_pickUPlocation, txt_dropUPlocation, txt_items_desc, txt_delivery_items, txt_num_off_passengers, txt_type_car;

    Handler handler = new Handler();
    int delay = 10000; //milliseconds

    private BroadcastReceiver mHandler = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
//            if (!isNetworkAvailable()) {
//                showToast(getResources().getString(R.string.please_check_your_network_connection));
//                return;
//            } else {
            stReqId = getIntent().getStringExtra("reqID");
            get_req_details();

//            }
        }
    };


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_details);
        LocalBroadcastManager.getInstance(this).registerReceiver(mHandler, new IntentFilter("com.volive.osha_FCM-OFFER"));
        Intent i = getIntent();
        stReqId = i.getStringExtra("reqID");
        //stReqType = i.getStringExtra("req_type");
        Log.e("dsa;kn", stReqId);
        preferenceUtils = new PreferenceUtils(RequestDetailsActivity.this);
        validate = new Validation_Class(RequestDetailsActivity.this);
        language = preferenceUtils.getStringFromPreference(PreferenceUtils.LANGUAGE, "");
        stUserID = preferenceUtils.getStringFromPreference(PreferenceUtils.USER_ID, "");
        intializeUI();
        intilaizeValues();
        adapter_part();
        get_req_details();

       /* handler.postDelayed(new Runnable() {
            public void run() {
                //do something
               // get_details();
                handler.postDelayed(this, delay);
            }
        }, delay);*/


    }

    private void adapter_part() {
        layoutManager = new LinearLayoutManager(RequestDetailsActivity.this, LinearLayoutManager.VERTICAL, false);
        providers_Rview.setLayoutManager(layoutManager);
    }


    private void intializeUI() {
        ll_comments = findViewById(R.id.ll_comments);
        txt_comments = findViewById(R.id.txt_comments);
        img_car_pic = findViewById(R.id.img_car_pic);
        linear_details = findViewById(R.id.sub_layout);
        del_layout = findViewById(R.id.deliv_item_layout);
        desc_layout = findViewById(R.id.description_layout);
        back_img_request = findViewById(R.id.back_img_request);
        providers_Rview = findViewById(R.id.providers_rview);
        txt_order_req = findViewById(R.id.txt_order_req);
        txt_pickUPlocation = findViewById(R.id.txt_pickUPlocation);
        txt_dropUPlocation = findViewById(R.id.txt_dropUPlocation);
        passenger_layout = findViewById(R.id.passenger_layout);
        items_layout = findViewById(R.id.items_layout);

        txt_items_desc = findViewById(R.id.txt_items_desc);
        txt_delivery_items = findViewById(R.id.txt_delivery_items);
        txt_type_car = findViewById(R.id.txt_type_car);
        txt_num_off_passengers = findViewById(R.id.txt_num_off_passengers);
    }

    private void intilaizeValues() {
        ll_comments.setOnClickListener(this);
        img_car_pic.setOnClickListener(this);
        back_img_request.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancel_dialog();
                //onBackPressed();


            }
        });


    }

    private void get_req_details() {

        final API_Services services = Retrofit_fun.getClient().create(API_Services.class);
        Call<JsonElement> call_req_details = null;
        call_req_details = services.get_req_details(Constant_keys.API_KEY, language, stReqId);
        final ProgressDialog progressDialog = new ProgressDialog(RequestDetailsActivity.this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(getResources().getString(R.string.loading));
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();
        call_req_details.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(@NonNull Call<JsonElement> call, @NonNull Response<JsonElement> response) {
                progressDialog.dismiss();
                Log.e("dgjfhd", response.body().toString());
                if (response.isSuccessful()) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().toString());

                        int status = jsonObject.getInt("status");

                        if (status == 1) {
                            String message = jsonObject.getString("message");
                            //Toast.makeText(getApplicationContext(),message,Toast.LENGTH_SHORT).show();

                            JSONObject user_json = jsonObject.getJSONObject("request_details");

                            String pickup_from = user_json.getString("pickup_from");
                            String drop_to = user_json.getString("drop_to");
                            String description = user_json.getString("description");
                            String delivery_items = user_json.getString("delivery_items");
                            String passengers_num = user_json.getString("passengers_num");
                            String car_type = user_json.getString("car_type");
                            String profile_pic = user_json.getString("profile_pic");
                            String order_id = user_json.getString("order_id");
                            String comment = user_json.getString("comment");

                            Log.e("sdvkn", pickup_from + drop_to + description + delivery_items + passengers_num + car_type);

                            txt_pickUPlocation.setText(pickup_from);
                            txt_dropUPlocation.setText(drop_to);
                            txt_delivery_items.setText(delivery_items);
                            txt_items_desc.setText(description);
                            txt_num_off_passengers.setText(passengers_num);
                            txt_type_car.setText(car_type);
                            txt_order_req.setText(order_id);
                            txt_comments.setText(comment);

                            values_set(user_json.getString("request_type"));

                            spModelArrayList = new ArrayList<>();
                            JSONArray driver_array = user_json.getJSONArray("drivers_list");

                            for (int i = 0; i < driver_array.length(); i++) {
                                JSONObject json_driver = driver_array.getJSONObject(i);

                                ReqSPModel_list reqSPModel_list = new ReqSPModel_list();

                                reqSPModel_list.setSt_offer_id(json_driver.getString("offer_id"));
                                reqSPModel_list.setSt_provider_id(json_driver.getString("provider_id"));
                                reqSPModel_list.setSt_request_id(json_driver.getString("request_id"));
                                reqSPModel_list.setSt_offer_price(json_driver.getString("offer_price"));
                                reqSPModel_list.setSt_offer_status(json_driver.getString("offer_status"));
                                reqSPModel_list.setSt_offer_date(json_driver.getString("offer_date"));
                                reqSPModel_list.setSt_provider_name(json_driver.getString("provider_name"));
                                reqSPModel_list.setSt_provider_phone(json_driver.getString("provider_phone"));
                                reqSPModel_list.setSt_driver_address(json_driver.getString("address"));
                                reqSPModel_list.setSt_driver_pic(json_driver.getString("profile_pic"));
                                reqSPModel_list.setSt_provider_rating(json_driver.getString("provider_rating"));
                                spModelArrayList.add(reqSPModel_list);
                            }
                        } else {
                            String message = jsonObject.getString("message");
                            Toast.makeText(RequestDetailsActivity.this, message, Toast.LENGTH_SHORT).show();
                        }


                        reqSpAdapter = new ReqSpAdapter(RequestDetailsActivity.this, spModelArrayList);
                        providers_Rview.setAdapter(reqSpAdapter);


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public void onFailure(@NonNull Call<JsonElement> call, @NonNull Throwable t) {

                Log.e("Error:", call.toString());
                Log.e("Eror:", t.toString());

            }
        });

    }

    private void values_set(String stReqType) {
        if (stReqType.equals("1")) {
            passenger_layout.setVisibility(View.VISIBLE);
            items_layout.setVisibility(View.GONE);
            Picasso.get().load(R.drawable.req_item_image)
                    .error(R.drawable.req_item_image)
                    .into(img_car_pic);
        } else {
            passenger_layout.setVisibility(View.GONE);
            items_layout.setVisibility(View.VISIBLE);
            Picasso.get().load(R.drawable.req_item_image)
                    .error(R.drawable.req_item_image)
                    .into(img_car_pic);

        }

    }


    @Override
    public void onBackPressed() {
        cancel_dialog();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.img_car_pic:
               /* Intent intent = new Intent(RequestDetailsActivity.this, TrackActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.putExtra("req_id",stReqId);
                intent.putExtra("action","0");
                startActivity(intent);*/
                break;
        }

    }


    @Override
    protected void onResume() {
        super.onResume();
        Offer_status.activityResumed();

    }


    private void cancel_dialog() {

        final Dialog dialog = new Dialog(RequestDetailsActivity.this, R.style.MyAlertDialogTheme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_cancel_req);
        dialog.getWindow().setBackgroundDrawableResource(R.drawable.bg_dialog);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);
        dialog.show();
        TextView txt_cancel_order = dialog.findViewById(R.id.txt_cancel_order);
        txt_cancel_order.setText(getString(R.string.are_you_sure_you_want_to_cancel_this_order));
        Button btn_no = dialog.findViewById(R.id.btn_no);
        Button btn_yes = dialog.findViewById(R.id.btn_yes);
        btn_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btn_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                cancel_ride();

//                Toast.makeText(OrderDetailsActivity.this, "Your Ride has been cancelled", Toast.LENGTH_SHORT).show();
//                startActivity(new Intent(OrderDetailsActivity.this, MyRequestActivity.class));
            }
        });

    }

    private void cancel_ride() {

        final API_Services service = Retrofit_fun.getClient().create(API_Services.class);
        Call<JsonElement> call_retrofit = null;
        call_retrofit = service.cancel_request(Constant_keys.API_KEY, preferenceUtils.getStringFromPreference(PreferenceUtils.LANGUAGE, ""), stReqId, preferenceUtils.getStringFromPreference(PreferenceUtils.USER_ID, ""), "");
        final ProgressDialog progressDialog;
        progressDialog = new ProgressDialog(RequestDetailsActivity.this);
        progressDialog.setCancelable(true);
        progressDialog.setTitle(R.string.loading);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();
        call_retrofit.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                progressDialog.dismiss();
                if (response.isSuccessful()) {
                    String serach = response.body().toString();
                    Log.e("cancel", serach);
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().toString());
                        int status = jsonObject.getInt("status");

                        if (status == 1) {
                            String message = jsonObject.getString("message");
                            showToast(message);
                            Intent i = new Intent(RequestDetailsActivity.this, User_Home_activity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            finish();

                        } else {
                            showToast(jsonObject.getString("message"));
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                progressDialog.dismiss();
                Log.d("Error Call", ">>>>" + call.toString());
                Log.d("Error", ">>>>" + t.toString());
            }
        });

    }


    @Override
    protected void onPause() {
        super.onPause();
        Offer_status.activityPaused();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mHandler);
    }

}