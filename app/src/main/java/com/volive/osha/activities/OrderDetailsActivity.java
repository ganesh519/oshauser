package com.volive.osha.activities;


import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.JsonElement;
import com.squareup.picasso.Picasso;
import com.volive.osha.R;
import com.volive.osha.helperclasses.Constant_keys;
import com.volive.osha.helperclasses.GPSTracker;
import com.volive.osha.helperclasses.Validation_Class;
import com.volive.osha.util_classes.API_Services;
import com.volive.osha.util_classes.PreferenceUtils;
import com.volive.osha.util_classes.Retrofit_fun;

import org.json.JSONObject;

import java.text.DecimalFormat;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class OrderDetailsActivity extends BaseActivity implements View.OnClickListener, OnMapReadyCallback {
    ImageView back_img_details;
    TextView txt_order_num,txt_comments, txt__order_address, txt_cust_pickUP, txt_cust_drop, txt_order_del_items, txt_type_car, txt_pass_num, txt_order_desc;
    LinearLayout cancel_track_layout, ll_del_desc, ll_del_car, ll_track;
    Button btn_cancel_order, btn_track_order;

    String strGet_latitude = " ", strGet_longitude = " ", languge, st_orderID, str_reqType, stReqdID, stUserID, getReqSts;

    GoogleMap gMap;
    GPSTracker gpsTracker;
    FragmentManager fragmentManager;
    SupportMapFragment mapFragment;
    LocationManager mLocationManager;
    PreferenceUtils preferenceUtils;
    Validation_Class validate;
    GoogleApiClient client;
    ImageView img_pin_location;
    CircleImageView profile_icon;
    String request_status ="";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);

        preferenceUtils = new PreferenceUtils(OrderDetailsActivity.this);
        validate = new Validation_Class(OrderDetailsActivity.this);
        gpsTracker = new GPSTracker(OrderDetailsActivity.this);
        languge = preferenceUtils.getStringFromPreference(PreferenceUtils.LANGUAGE, "");
        stUserID = preferenceUtils.getStringFromPreference(PreferenceUtils.USER_ID, "");
        str_reqType = getIntent().getStringExtra("req_type");
        st_orderID = getIntent().getStringExtra("order");
        stReqdID = getIntent().getStringExtra("req_id");
        //getReqSts = getIntent().getStringExtra("req_sts");
        mLocationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);

        client = new GoogleApiClient.Builder(getApplicationContext())
                .addApi(LocationServices.API)
                .build();
        initializeUI();
        initializeValues();

        if (str_reqType.equals("1"))
        {
            ll_del_desc.setVisibility(View.GONE);
            ll_del_car.setVisibility(View.VISIBLE);
        }
        else if (str_reqType.equals("2"))
        {
            ll_del_desc.setVisibility(View.VISIBLE);
            ll_del_car.setVisibility(View.GONE);
        }

        if (!isNetworkAvailable()) {
            showToast(getString(R.string.please_check_your_network_connection));
            return;
        } else {
            view_details();

        }


    }

    private void values_set(String request_status) {

        //checking button condition
        // request_status:(0=pending,1=completed,2=in progress,3=cancelled)(optional)
        if (request_status.equals("0")) {
            ll_track.setVisibility(View.VISIBLE);
            btn_cancel_order.setVisibility(View.VISIBLE);
            btn_track_order.setVisibility(View.VISIBLE);
        } else if (request_status.equals("3")) {
            ll_track.setVisibility(View.GONE);
        } else if (request_status.equals("1")) {
            ll_track.setVisibility(View.GONE);
            // btn_track_order.setVisibility(View.VISIBLE);
        } else if (request_status.equals("2")) {
            ll_track.setVisibility(View.VISIBLE);
        }
    }

    private void view_details() {
        final API_Services service = Retrofit_fun.getClient().create(API_Services.class);
        Call<JsonElement> call_retrofit = null;
        call_retrofit = service.view_order_details(Constant_keys.API_KEY, languge, st_orderID);
        final ProgressDialog progressDialog;
        progressDialog = new ProgressDialog(OrderDetailsActivity.this);
        progressDialog.setCancelable(true);
        progressDialog.setTitle(R.string.loading);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();
        call_retrofit.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                progressDialog.dismiss();
                if (response.isSuccessful()) {
                    String serach = response.body().toString();
                    Log.e("dasn", serach);
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().toString());
                        int status = jsonObject.getInt("status");
                        String message = jsonObject.getString("message");
                        if (status == 1) {
                           // showToast(message);
                            JSONObject json_order = jsonObject.getJSONObject("order_details");

                            String order_id = json_order.getString("order_id");
                            String pickup_from = json_order.getString("pickup_from");
                            String drop_to = json_order.getString("drop_to");


                            String latitude_pick = json_order.getString("latitude_pick");
                            String longitude_pick = json_order.getString("longitude_pick");
                            String longitude_drop = json_order.getString("longitude_drop");
                            String latitude_drop = json_order.getString("latitude_drop");
                            String user_image = json_order.getString("user_image");
                            String comment = json_order.getString("comment");
                            String request_status = json_order.getString("request_status");
                            String invoice_status = json_order.getString("invoice_status");
                            String payment_status = json_order.getString("payment_status");

                            values_set(request_status);

                            Picasso.get().load(Constant_keys.imagebaseurl + user_image)
                                    .placeholder(android.R.drawable.progress_indeterminate_horizontal)
                                    .error(R.drawable.guest_user).into(profile_icon);

                            if (json_order.has("description")) {
                                String description = json_order.getString("description");
                                txt_order_desc.setText(description);
                            }
                            //data
                            if (json_order.has("delivery_items")) {
                                String delivery_items = json_order.getString("delivery_items");
                                txt_order_del_items.setText(delivery_items);

                            }

                            //type equlas 1
                            if (json_order.has("passengers_num") || json_order.has("car_type")) {
                                String passengers_num = json_order.getString("passengers_num");
                                String car_type = json_order.getString("car_type");
                                txt_pass_num.setText(passengers_num);
                                txt_type_car.setText(car_type);
                            }

                            txt_order_num.setText("#" + " " + " " + order_id);
                            txt__order_address.setText(pickup_from);
                            txt_cust_pickUP.setText(pickup_from);
                            txt_cust_drop.setText(drop_to);
                            txt_comments.setText(comment);

                        } else {
                            showToast(jsonObject.getString("message"));
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                progressDialog.dismiss();
                Log.d("Error Call", ">>>>" + call.toString());
                Log.d("Error", ">>>>" + t.toString());
            }
        });

    }

    private void initializeUI() {
        txt_comments = findViewById(R.id.txt_comments);
        ll_track = findViewById(R.id.ll_track);
        profile_icon = findViewById(R.id.profile_icon);
        btn_track_order = findViewById(R.id.btn_track_order);
        btn_cancel_order = findViewById(R.id.btn_cancel);
        img_pin_location = findViewById(R.id.img_pin_location);
        fragmentManager = getSupportFragmentManager();
        mapFragment = (SupportMapFragment) fragmentManager.findFragmentById(R.id.map);
        mapFragment.getMapAsync(OrderDetailsActivity.this);
        ll_del_desc = findViewById(R.id.ll_del_desc);
        ll_del_car = findViewById(R.id.ll_del_car);
        txt_order_desc = findViewById(R.id.txt_order_desc);
        txt__order_address = findViewById(R.id.txt__order_address);
        txt_order_num = findViewById(R.id.txt_order_num);
        txt_cust_pickUP = findViewById(R.id.txt_cust_pickUP);
        txt_cust_drop = findViewById(R.id.txt_cust_drop);
        txt_order_del_items = findViewById(R.id.txt_order_del_items);
        txt_order_desc = findViewById(R.id.txt_order_desc);
        txt_type_car = findViewById(R.id.txt_type_car);
        txt_pass_num = findViewById(R.id.txt_pass_num);
        back_img_details = findViewById(R.id.back_img_details);
    }

    private void initializeValues() {
        ll_del_desc.setOnClickListener(this);
        btn_track_order.setOnClickListener(this);
        btn_cancel_order.setOnClickListener(this);
        back_img_details.setOnClickListener(this);

        img_pin_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!strGet_latitude.isEmpty() || !strGet_longitude.isEmpty()) {
                    Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                            Uri.parse("http://maps.google.com/maps?daddr=" + Double.parseDouble(strGet_latitude) + "," + Double.parseDouble(strGet_longitude)));
                    startActivity(intent);
                }
            }
        });

    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {

            case R.id.btn_cancel:

                cancel_dialog();
                break;


            case R.id.btn_track_order:

                Intent intent1 = new Intent(OrderDetailsActivity.this, TrackActivity.class);
                intent1.putExtra("req_id", stReqdID);
                intent1.putExtra("action","1");
                startActivity(intent1);
                //finish();

                break;

            case R.id.back_img_details:

                onBackPressed();
                /*startActivity(new Intent(OrderDetailsActivity.this, TrackActivity.class));
                finish();*/

                break;
        }
    }

    private void cancel_dialog() {

        final Dialog dialog = new Dialog(OrderDetailsActivity.this, R.style.MyAlertDialogTheme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.cancel_dialog_layout);
        dialog.getWindow().setBackgroundDrawableResource(R.drawable.bg_dialog);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);
        dialog.show();
        TextView txt_cancel_order = dialog.findViewById(R.id.txt_cancel_order);
        txt_cancel_order.setText(getString(R.string.are_you_sure_you_want_to_cancel_this_order));
        Button btn_no = dialog.findViewById(R.id.btn_no);
        Button btn_yes = dialog.findViewById(R.id.btn_yes);
        btn_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btn_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                cancel_ride();

//                Toast.makeText(OrderDetailsActivity.this, "Your Ride has been cancelled", Toast.LENGTH_SHORT).show();
//                startActivity(new Intent(OrderDetailsActivity.this, MyRequestActivity.class));
            }
        });

    }

    private void cancel_ride() {

        final API_Services service = Retrofit_fun.getClient().create(API_Services.class);
        Call<JsonElement> call_retrofit = null;
        call_retrofit = service.cancel_request(Constant_keys.API_KEY, languge, stReqdID, stUserID, "");
        final ProgressDialog progressDialog;
        progressDialog = new ProgressDialog(OrderDetailsActivity.this);
        progressDialog.setCancelable(true);
        progressDialog.setTitle(R.string.loading);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();
        call_retrofit.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                progressDialog.dismiss();
                if (response.isSuccessful()) {
                    String serach = response.body().toString();
                    Log.e("cancel", serach);
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().toString());
                        int status = jsonObject.getInt("status");

                        if (status == 1) {
                            String message = jsonObject.getString("message");
                            showToast(message);
                            Intent i = new Intent(OrderDetailsActivity.this, User_Home_activity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            finish();

                        } else {
                            showToast(jsonObject.getString("message"));
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                progressDialog.dismiss();
                Log.d("Error Call", ">>>>" + call.toString());
                Log.d("Error", ">>>>" + t.toString());
            }
        });

    }


    private void alerteDialog() {
        final Dialog dialog = new Dialog(OrderDetailsActivity.this, R.style.MyAlertDialogTheme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.alert_dialog);
        dialog.getWindow().setBackgroundDrawableResource(
                R.drawable.bg_dialog);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);
        dialog.show();

//        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        Button no_btn = dialog.findViewById(R.id.no_btn);
        Button yes_btn = dialog.findViewById(R.id.yes_btn);
        no_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        yes_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent intent = new Intent(OrderDetailsActivity.this, User_Home_activity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("Activity", User_Home_activity.activityName);
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent(Intent.ACTION_MAIN);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        finish();
    }

    private BitmapDescriptor bitmapDescriptorFromVector(Context context, int vectorResId) {
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorResId);
        vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        gMap = googleMap;

        LatLng sydney;
        if (ActivityCompat.checkSelfPermission(OrderDetailsActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(OrderDetailsActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        gMap.setMyLocationEnabled(false);
        gMap.getUiSettings().setZoomControlsEnabled(false);
        gMap.getUiSettings().setZoomGesturesEnabled(true);
        gMap.getUiSettings().setCompassEnabled(false);
        gMap.getUiSettings().setRotateGesturesEnabled(true);
        gMap.getUiSettings().setScrollGesturesEnabled(false);


        double no = 12.786;
        DecimalFormat dec = new DecimalFormat("#0.0000");

        try {
            LatLng latLng = new LatLng(gpsTracker.getLatitude(), gpsTracker.getLongitude());
            gMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16.0F));
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16.0F));
        } catch (Exception e) {
            e.printStackTrace();
        }
       /* gMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {
                Log.e("onCameraChange: ", "" + cameraPosition.target.latitude + " : " + cameraPosition.target.longitude);
                DecimalFormat dec = new DecimalFormat("#0.0000");
                strLattitude = dec.format(cameraPosition.target.latitude);
                strLongitude =  dec.format(cameraPosition.target.longitude);

                double_lat = cameraPosition.target.latitude;
                double_lang = cameraPosition.target.longitude;
                getCompleteAddressString(cameraPosition.target.latitude, cameraPosition.target.longitude);
            }
        });*/

    }

    private void mapingpoint() {
        double latitude = Double.parseDouble(strGet_latitude);
        double longitude = Double.parseDouble(strGet_longitude);
        LatLng position = new LatLng(latitude, longitude);

        fragmentManager = getSupportFragmentManager();
        mapFragment = (SupportMapFragment) fragmentManager.findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        //  latlngs_array = new ArrayList<>();
        MarkerOptions options = new MarkerOptions();
        //   latlngs_array.add( new LatLng(latitude, longitude));
        gMap.clear();
        gMap.addMarker(new MarkerOptions()
                .position(position)
                .snippet("user Location")
                .rotation((float) 3.5)
                .icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.pin_location)));
        CameraUpdate updatePosition = CameraUpdateFactory.newLatLng(position);
        CameraUpdate updateZoom = CameraUpdateFactory.zoomBy(4.0F);
        gMap.moveCamera(updatePosition);
        gMap.animateCamera(updateZoom);

    }
}