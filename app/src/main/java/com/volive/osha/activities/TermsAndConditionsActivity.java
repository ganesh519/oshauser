package com.volive.osha.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.JsonElement;
import com.volive.osha.R;
import com.volive.osha.helperclasses.Constant_keys;
import com.volive.osha.util_classes.API_Services;
import com.volive.osha.util_classes.PreferenceUtils;
import com.volive.osha.util_classes.Retrofit_fun;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class TermsAndConditionsActivity extends BaseActivity implements View.OnClickListener {
    ImageView back_img_details;
    TextView txt_terms_cond, toolbar_title;
    String activityName = "";
    PreferenceUtils preferenceUtils;
    String language;
    String UserId;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_and_conditions);
        preferenceUtils = new PreferenceUtils(TermsAndConditionsActivity.this);
        language = preferenceUtils.getStringFromPreference(PreferenceUtils.LANGUAGE,"");

        initializeUI();
        initializeValues();
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            activityName = extras.getString("Activity");
        }
        terms_cond_service();


    }

    private void terms_cond_service()
    {
        final API_Services services  = Retrofit_fun.getClient().create(API_Services.class);
        Call<JsonElement> call_notification= null;
        call_notification = services.terms_conditions(Constant_keys.API_KEY,language);
        final ProgressDialog progressDoalog = new ProgressDialog(TermsAndConditionsActivity.this, R.style.AppCompatAlertDialogStyle);
        progressDoalog.setCancelable(false);
        progressDoalog.setMessage("Please Wait....");
        progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDoalog.show();

        call_notification.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                progressDoalog.dismiss();
                if (response.isSuccessful())
                {
                    Log.e("sjwlb",response.body().toString());
                    try {
                        JSONObject json_obejct =  new JSONObject(response.body().toString());
                        int status = json_obejct.getInt("status");
                        if (status ==1)
                        {
                            JSONObject json_data = json_obejct.getJSONObject("data");

                            String st_data = json_data.getString("text");


                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                txt_terms_cond.setText(Html.fromHtml(st_data, Html.FROM_HTML_MODE_COMPACT));
                            } else {
                                txt_terms_cond.setText(Html.fromHtml(st_data));
                            }


                            txt_terms_cond.setText(st_data);


                        }



                    } catch (Exception e) {
                        progressDoalog.dismiss();
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                progressDoalog.dismiss();
                Log.e("Erro:" ,call.toString());
                Log.e("Error:",t.toString());

            }
        });

    }




    private void initializeValues() {
        back_img_details.setOnClickListener(this);
    }

    private void initializeUI() {
        //ImageView
        back_img_details = findViewById(R.id.back_img_details);
        //TextView
        toolbar_title = findViewById(R.id.toolbar_title);
        txt_terms_cond = findViewById(R.id.txt_terms_cond);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent i = new Intent(TermsAndConditionsActivity.this, User_Home_activity.class);
        i.putExtra("Activity", activityName);
        startActivity(i);
        finish();


    }

    @Override
    public void onClick(View v) {


        switch (v.getId())
        {
            case  R.id.back_img_details:

                Intent i = new Intent(TermsAndConditionsActivity.this, User_Home_activity.class);
                i.putExtra("Activity", activityName);
                startActivity(i);
                finish();


                break;


        }

    }
}