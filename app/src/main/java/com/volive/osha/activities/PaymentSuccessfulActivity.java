package com.volive.osha.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.google.gson.JsonElement;
import com.volive.osha.R;
import com.volive.osha.helperclasses.Constant_keys;
import com.volive.osha.helperclasses.HelperClass;
import com.volive.osha.util_classes.API_Services;
import com.volive.osha.util_classes.PreferenceUtils;
import com.volive.osha.util_classes.Retrofit_fun;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class PaymentSuccessfulActivity extends BaseActivity implements View.OnClickListener {
    ImageView back_img_details, img_star_one, img_star_two, img_star_three, img_star_four, img_star_five;
    TextView toolbar_title;
    EditText comment_edit_text;
    Button btn_confrom_pay;
    PreferenceUtils preferenceUtils;
    HelperClass helperClass;
    String stUserId,language ,stReq_Id = "",st_rating;
    RatingBar rate_bar_customer;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_successful);
        preferenceUtils = new PreferenceUtils(PaymentSuccessfulActivity.this);
        helperClass = new HelperClass(PaymentSuccessfulActivity.this);
        language = preferenceUtils.getStringFromPreference(PreferenceUtils.LANGUAGE,"");
        stUserId = preferenceUtils.getStringFromPreference(PreferenceUtils.USER_ID,"");

        initializeUI();
        initializeValues();
    }

    private void initializeUI() {
        //ImageView
        back_img_details = findViewById(R.id.back_img_details);
        img_star_one = findViewById(R.id.img_star_one);
        img_star_two = findViewById(R.id.img_star_two);
        img_star_three = findViewById(R.id.img_star_three);
        img_star_four = findViewById(R.id.img_star_four);
        img_star_five = findViewById(R.id.img_star_five);
        //TextView
        toolbar_title = findViewById(R.id.toolbar_title);
        //EditText
        comment_edit_text = findViewById(R.id.comment_edit_text);
        //Button
        btn_confrom_pay = findViewById(R.id.btn_confrom_pay);
        rate_bar_customer = findViewById(R.id.rate_bar_customer);
    }

    private void initializeValues() {
        back_img_details.setOnClickListener(this);
        btn_confrom_pay.setOnClickListener(this);
        img_star_one.setOnClickListener(this);
        img_star_two.setOnClickListener(this);
        img_star_three.setOnClickListener(this);
        img_star_four.setOnClickListener(this);
        img_star_five.setOnClickListener(this);
        rate_bar_customer.setOnClickListener(this);
        rate_bar_customer.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            public void onRatingChanged(RatingBar ratingBar, float rating,
                                        boolean fromUser) {

               // txtRatingValue.setText(String.valueOf(rating));
                st_rating =  String.valueOf(rating);
                Log.e("value",st_rating);
            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back_img_details:
                Intent i = new Intent(PaymentSuccessfulActivity.this, User_Home_activity.class);
                startActivity(i);
                finish();
                break;
            case R.id.btn_confrom_pay:

                  /*  st_rating =  String.valueOf(rate_bar_customer.getRating());
                    Log.e("value",st_rating);*/
                   give_rating();
               /* Intent intent = new Intent(PaymentSuccessfulActivity.this, User_Home_activity.class);
                startActivity(intent);
                finish();*/
                break;
            case R.id.img_star_one:
                img_star_one.setImageResource(R.drawable.rating_one_selected);
                img_star_two.setImageResource(R.drawable.rating_two_unselected);
                img_star_three.setImageResource(R.drawable.rating_three_unselected);
                img_star_four.setImageResource(R.drawable.rating_four_unselected);
                img_star_five.setImageResource(R.drawable.rating_five_unselected);

                break;
            case R.id.img_star_two:
                img_star_one.setImageResource(R.drawable.rating_one_selected);
                img_star_two.setImageResource(R.drawable.rating_two_selected);
                img_star_three.setImageResource(R.drawable.rating_three_unselected);
                img_star_four.setImageResource(R.drawable.rating_four_unselected);
                img_star_five.setImageResource(R.drawable.rating_five_unselected);
                break;
            case R.id.img_star_three:
                img_star_one.setImageResource(R.drawable.rating_one_selected);
                img_star_two.setImageResource(R.drawable.rating_two_selected);
                img_star_three.setImageResource(R.drawable.rating_three_selected);
                img_star_four.setImageResource(R.drawable.rating_four_unselected);
                img_star_five.setImageResource(R.drawable.rating_five_unselected);
                break;
            case R.id.img_star_four:
                img_star_one.setImageResource(R.drawable.rating_one_selected);
                img_star_two.setImageResource(R.drawable.rating_two_selected);
                img_star_three.setImageResource(R.drawable.rating_three_selected);
                img_star_four.setImageResource(R.drawable.rating_four_selected);
                img_star_five.setImageResource(R.drawable.rating_five_unselected);
                break;
            case R.id.img_star_five:
                img_star_one.setImageResource(R.drawable.rating_one_selected);
                img_star_two.setImageResource(R.drawable.rating_two_selected);
                img_star_three.setImageResource(R.drawable.rating_three_selected);
                img_star_four.setImageResource(R.drawable.rating_four_selected);
                img_star_five.setImageResource(R.drawable.rating_five_selected);
                break;
        }
    }

    private void give_rating()
    {
            final API_Services service = Retrofit_fun.getClient().create(API_Services.class);
            Call<JsonElement> call_retrofit = null;
            call_retrofit = service.request_rating(Constant_keys.API_KEY, language,PaymentActivity.st_req_id, st_rating);
            final ProgressDialog progressDialog;
            progressDialog = new ProgressDialog(PaymentSuccessfulActivity.this);
            progressDialog.setCancelable(true);
            progressDialog.setTitle(R.string.loading);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
            call_retrofit.enqueue(new Callback<JsonElement>() {
                @Override
                public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                    progressDialog.dismiss();
                    if (response.isSuccessful()) {
                        Log.e("sdkfn", response.body().toString());
                        try {
                            JSONObject jsonObject = new JSONObject(response.body().toString());
                            int status = jsonObject.getInt("status");
                            if (status == 1)
                            {
                                showToast(jsonObject.getString("message"));
                                Intent intent = new Intent(PaymentSuccessfulActivity.this, User_Home_activity.class);
                                startActivity(intent);
                                finish();
                            }
                            else {
                                showToast(jsonObject.getString("message"));
                           /* Intent intent = new Intent(PaymentActivity.this, PaymentSuccessfulActivity.class);
                            startActivity(intent);
                            finish();*/
                            }



                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }


                }

                @Override
                public void onFailure(Call<JsonElement> call, Throwable t) {
                    progressDialog.dismiss();
                    Log.d("Error Call", ">>>>" + call.toString());
                    Log.d("Error", ">>>>" + t.toString());
                }
            });

        }



    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent(PaymentSuccessfulActivity.this, User_Home_activity.class);
        startActivity(i);
        finish();
    }


}
