package com.volive.osha.activities;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.google.gson.JsonElement;
import com.squareup.picasso.Picasso;
import com.volive.osha.R;
import com.volive.osha.helperclasses.Constant_keys;
import com.volive.osha.helperclasses.Offer_status;
import com.volive.osha.helperclasses.Validation_Class;
import com.volive.osha.util_classes.API_Services;
import com.volive.osha.util_classes.PreferenceUtils;
import com.volive.osha.util_classes.Retrofit_fun;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class TrackActivity extends BaseActivity implements View.OnClickListener {

    ImageView back_img_track, img_star_one, img_star_two, img_star_three, img_star_four, img_star_five, chat_img, call_img;
    TextView toolbar_title, txt_service_provider_name, txt_distance, txt_time, txt_order_recieved, txt_order_comp;
    ImageView service_provider_image, img_order_placed, img_order_confirm, img_driver_assigned, img_completed;
    View order_place_view, view_order_confirm, view_order_completed;
    Button btn_order_details;
    String stReqID = " ",stAction = "",  stUserID = " ", language,strSenderID = " ",strRecieverID = " " ,strRecvImage = " ",strSendReq =" ",strProviderCall = "";
    PreferenceUtils preferenceUtils;
    Validation_Class validate;
    Float fl_rating;
    RatingBar rate_bar_fl_row;


    private BroadcastReceiver mHandler = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (!isNetworkAvailable()) {
                showToast(getResources().getString(R.string.please_check_your_network_connection));
                return;
            } else {
                stReqID = getIntent().getStringExtra("req_id");
                stAction = getIntent().getStringExtra("action");
                track_details();


            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_track);
        LocalBroadcastManager.getInstance(this).registerReceiver(mHandler, new IntentFilter("track"));
        preferenceUtils = new PreferenceUtils(TrackActivity.this);
        validate = new Validation_Class(TrackActivity.this);
        language = preferenceUtils.getStringFromPreference(PreferenceUtils.LANGUAGE, "");
        stUserID = preferenceUtils.getStringFromPreference(PreferenceUtils.USER_ID, "");

        Bundle bundle = getIntent().getExtras();

        if (bundle !=null)

        {
            stReqID = getIntent().getStringExtra("req_id");
            stAction = bundle.getString("action");
        }


        initializeUI();
        initializeValues();
        track_details();
        //InvoiceDialog();
    }

    private void initializeUI() {

        //rating bar
        rate_bar_fl_row = findViewById(R.id.rate_bar_fl_row);
        //view part
        order_place_view = findViewById(R.id.order_place_view);
        txt_order_recieved = findViewById(R.id.txt_order_recieved);
        view_order_confirm = findViewById(R.id.view_order_confirm);
        view_order_completed = findViewById(R.id.view_order_completed);

        //ImageView
        img_completed = findViewById(R.id.img_completed);
        img_driver_assigned = findViewById(R.id.img_driver_assigned);
        img_order_confirm = findViewById(R.id.img_order_confirm);
        img_order_placed = findViewById(R.id.img_order_placed);
        back_img_track = findViewById(R.id.back_img_track);
        img_star_one = findViewById(R.id.img_star_one);
        img_star_two = findViewById(R.id.img_star_two);
        img_star_three = findViewById(R.id.img_star_three);
        img_star_four = findViewById(R.id.img_star_four);
        img_star_five = findViewById(R.id.img_star_five);
        chat_img = findViewById(R.id.chat_img);
        call_img = findViewById(R.id.call_img);

        //TextView
        txt_order_comp = findViewById(R.id.txt_order_comp);
        toolbar_title = findViewById(R.id.toolbar_title);
        txt_service_provider_name = findViewById(R.id.txt_service_provider_name);
        txt_distance = findViewById(R.id.txt_distance);
        txt_time = findViewById(R.id.txt_time);

        //CircleImageView
        service_provider_image = findViewById(R.id.service_provider_image);

        //Button
        btn_order_details = findViewById(R.id.btn_order_details);
    }

    private void initializeValues() {
        rate_bar_fl_row.setOnClickListener(this);
        img_order_confirm.setOnClickListener(this);
        img_order_placed.setOnClickListener(this);
        back_img_track.setOnClickListener(this);
        btn_order_details.setOnClickListener(this);
        chat_img.setOnClickListener(this);
        call_img.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.back_img_track:

                if (stAction.equalsIgnoreCase("1"))
                {
                    finish();
                }
                else if (stAction.equalsIgnoreCase("0")){
                    intent = new Intent(TrackActivity.this,User_Home_activity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                }

                break;
            case R.id.btn_order_details:
                intent = new Intent(TrackActivity.this, OrderDetailsActivity.class);
                intent.putExtra("Activity", "Request");
                startActivity(intent);
                // finish();
                break;
            case R.id.chat_img:

                intent = new Intent(TrackActivity.this, ChatActivity.class);

                intent.putExtra("sender",strSenderID);
                intent.putExtra("rec_id",strRecieverID);
                intent.putExtra("rcv_image",strRecvImage);
                intent.putExtra("request",strSendReq);

                startActivity(intent);
                //  finish();
                break;

            case R.id.call_img:

              /*  intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse(strProviderCall));*/
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + strProviderCall));
                startActivity(callIntent);
               /* if (ActivityCompat.checkSelfPermission(TrackActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
*/

                break;
        }
    }

    private void track_details() {
        final API_Services services = Retrofit_fun.getClient().create(API_Services.class);
        Call<JsonElement> call_track = null;
        call_track = services.tracking(Constant_keys.API_KEY, language, stReqID);
        final ProgressDialog progressDoalog = new ProgressDialog(TrackActivity.this, R.style.AppCompatAlertDialogStyle);
        progressDoalog.setCancelable(false);
        progressDoalog.setMessage("Please Wait....");
        progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDoalog.show();
        call_track.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                progressDoalog.dismiss();
                if (response.isSuccessful()) {
                    Log.e("sjwlb", response.body().toString());
                    try {
                        JSONObject json_obejct = new JSONObject(response.body().toString());
                        int status = json_obejct.getInt("status");
                        //String   message = json_obejct.getString("message");
                        if (status == 1) {
                            JSONObject json_tracking = json_obejct.getJSONObject("details");
                            strSendReq = json_tracking.getString("request_id");
                            String distance = json_tracking.getString("distance");
                            String time = json_tracking.getString("time");
                            txt_distance.setText(distance);
                            txt_time.setText(time);
                            String user_track_status = json_tracking.getString("user_track_status");
                            String invoice_status = json_tracking.getString("invoice_status");
                            if (invoice_status.equals("1"))
                            {
                                InvoiceDialog();
                            }

                            String provider_rating = json_tracking.getString("provider_rating");

                            if (!provider_rating.equals("") || !provider_rating.isEmpty())
                            {
                                fl_rating = Float.valueOf(provider_rating);
                            }
                            else {
                                fl_rating = Float.valueOf(0);
                            }
                            rate_bar_fl_row .setRating(fl_rating);
                            //  user_track_status: 0:order placed; 1:order confirmed; 2: Driver assigned; 3:completed

                            if (user_track_status.equals("0"))
                            {
                                img_order_placed.setVisibility(View.VISIBLE);
                            }
                            else if (user_track_status.equals("1"))
                            {
                                //img_order_placed.setVisibility(View.VISIBLE);
                                Picasso.get().load(R.drawable.check)
                                        .placeholder(android.R.drawable.progress_indeterminate_horizontal)
                                        .error(R.drawable.check)
                                        .into(img_order_confirm);
                                view_order_confirm.setBackgroundColor(getResources().getColor(R.color.colorPrimary2));
                            }
                            else if (user_track_status.equals("2"))
                            {
                                Picasso.get().load(R.drawable.check)
                                        .placeholder(android.R.drawable.progress_indeterminate_horizontal)
                                        .error(R.drawable.check)
                                        .into(img_order_confirm);
                                view_order_confirm.setBackgroundColor(getResources().getColor(R.color.colorPrimary2));

                                //img_order_placed.setVisibility(View.VISIBLE);
                                Picasso.get().load(R.drawable.check)
                                        .placeholder(android.R.drawable.progress_indeterminate_horizontal)
                                        .error(R.drawable.check)
                                        .into(img_driver_assigned);
                                view_order_completed.setBackgroundColor(getResources().getColor(R.color.colorPrimary2));
                            } else if (user_track_status.equals("3"))
                            {
                                Picasso.get().load(R.drawable.check)
                                        .placeholder(android.R.drawable.progress_indeterminate_horizontal)
                                        .error(R.drawable.check)
                                        .into(img_order_confirm);
                                view_order_confirm.setBackgroundColor(getResources().getColor(R.color.colorPrimary2));
                                Picasso.get().load(R.drawable.check)
                                        .placeholder(android.R.drawable.progress_indeterminate_horizontal)
                                        .error(R.drawable.check)
                                        .into(img_driver_assigned);
                                view_order_completed.setBackgroundColor(getResources().getColor(R.color.colorPrimary2));
                                //img_order_placed.setVisibility(View.VISIBLE);
                                Picasso.get().load(R.drawable.check)
                                        .placeholder(android.R.drawable.progress_indeterminate_horizontal)
                                        .error(R.drawable.check)
                                        .into(img_completed);
                                //  view_order_confirm.setBackgroundColor(getResources().getColor(R.color.colorPrimary2));
                            }



                            JSONObject user_json = json_tracking.getJSONObject("user_details");

                            String user_id = user_json.getString("user_id");

                            strSenderID = user_json.getString("user_id");

                            String st_name = user_json.getString("name");
                            String username = user_json.getString("username");
                            String st_request_type = user_json.getString("request_type");

                            JSONObject provider_json = json_tracking.getJSONObject("provider_details");

                            strRecieverID = provider_json.getString("user_id");

                            String sp_name = provider_json.getString("name");
                            String sp_username = provider_json.getString("username");
                            String sp_request_type = provider_json.getString("request_type");
                            String profile_pic_driver = provider_json.getString("profile_pic");
                            strProviderCall = provider_json.getString("phone");

                            strRecvImage  =  provider_json.getString("profile_pic");

                            txt_service_provider_name.setText(sp_name);

                            //service provider provider image

                            Picasso.get().load(Constant_keys.imagebaseurl + profile_pic_driver)
                                    .placeholder(android.R.drawable.progress_indeterminate_horizontal)
                                    .error(R.drawable.guest_user)
                                    .into(service_provider_image);
                            //For user_track_status: 0:order placed; 1:order confirmed; 2: Driver assigned; 3:completed

                            //data modification


                        }
                        else {

                        }


                    } catch (Exception e) {
                        progressDoalog.dismiss();
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                progressDoalog.dismiss();
                Log.e("Erro:", call.toString());
                Log.e("Error:", t.toString());

            }
        });

    }


    private void InvoiceDialog() {

        final Dialog dialog = new Dialog(TrackActivity.this, R.style.MyAlertDialogTheme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.invoice_dialog);
        dialog.getWindow().setBackgroundDrawableResource(R.drawable.bg_dialog);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);
        dialog.show();

        Button invoice_btn = dialog.findViewById(R.id.invoice_btn);
        ImageView cancel_dialog = dialog.findViewById(R.id.cancel_dialog);

        cancel_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        invoice_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Intent intent = new Intent(TrackActivity.this, InvoiceDetailsActivity.class);
                intent.putExtra("req_id",strSendReq);
                startActivity(intent);
            }
        });


    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Offer_status.activityPaused();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mHandler);
    }
    @Override
    protected void onResume() {
        super.onResume();
        Offer_status.activityResumed();

    }
}

















/* String order_id = json_tracking.getString("order_id");
                            String pickup_from = json_tracking.getString("pickup_from");
                            String drop_to = json_tracking.getString("drop_to");
                            String passengers_num = json_tracking.getString("passengers_num");
                            String car_type = json_tracking.getString("car_type");
                            String delivery_items = json_tracking.getString("delivery_items");
                            String description = json_tracking.getString("description");
                            String address = json_tracking.getString("address");
                            String latitude_pick = json_tracking.getString("latitude_pick");
                            String longitude_pick = json_tracking.getString("longitude_pick");
                            String latitude_drop = json_tracking.getString("latitude_drop");
                            String longitude_drop = json_tracking.getString("longitude_drop");
                            String request_timer = json_tracking.getString("request_timer");
                            String updated_on = json_tracking.getString("updated_on");
                            String created_on = json_tracking.getString("created_on");
                            String provider_id = json_tracking.getString("provider_id");
                            String invoice_number = json_tracking.getString("invoice_number");
                            String invoice_status = json_tracking.getString("invoice_status");
                            String total_amount = json_tracking.getString("total_amount");
                            String additional_amount = json_tracking.getString("additional_amount");
                            String provider_amount = json_tracking.getString("provider_amount");
                            String coupon_code = json_tracking.getString("coupon_code");
                            String discount_amount = json_tracking.getString("discount_amount");
                            String invoice_description = json_tracking.getString("invoice_description");
                            String request_rating = json_tracking.getString("request_rating");
                            String rating_comments = json_tracking.getString("rating_comments");
                            String request_rating_status = json_tracking.getString("request_rating_status");
                            String wallet_amount = json_tracking.getString("wallet_amount");
                            String date = json_tracking.getString("date");
                            String vat_deduction = json_tracking.getString("vat_deduction");
                            String vat = json_tracking.getString("vat");
                            String name = json_tracking.getString("name");
                            String request_type = json_tracking.getString("request_type");
                            String request_status = json_tracking.getString("request_status");
                            String tracking_status = json_tracking.getString("tracking_status");
                            String offer_price = json_tracking.getString("offer_price");*/