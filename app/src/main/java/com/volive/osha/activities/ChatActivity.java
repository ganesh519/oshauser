package com.volive.osha.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.webkit.MimeTypeMap;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.JsonElement;
import com.muddzdev.styleabletoastlibrary.StyleableToast;
import com.volive.osha.R;
import com.volive.osha.adapters.ChatAdapter;
import com.volive.osha.helperclasses.Constant_keys;
import com.volive.osha.helperclasses.GalleryUriToPath;
import com.volive.osha.helperclasses.Offer_status;
import com.volive.osha.helperclasses.Validation_Class;
import com.volive.osha.models.ChatModel;
import com.volive.osha.models.Chat_Model;
import com.volive.osha.util_classes.API_Services;
import com.volive.osha.util_classes.Chating_screen_status;
import com.volive.osha.util_classes.PreferenceUtils;
import com.volive.osha.util_classes.Retrofit_fun;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.FileNameMap;
import java.net.URLConnection;
import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.provider.MediaStore.ACTION_IMAGE_CAPTURE;
import static com.volive.osha.helperclasses.GalleryUriToPath.getPath;

public class ChatActivity extends BaseActivity implements View.OnClickListener {
    public static String strSender_id, strReciever_id, strMessge, strReqID, str_Chat_key;
    public static String reciever_image, langauge;
    public  static String rcv_image;
    public static ArrayList<Chat_Model> chat_array;

    ImageView back_img_details, img_chat_pic;
    RecyclerView chatRecyclerView;
    LinearLayoutManager mLayoutManager;
    ArrayList<ChatModel> arrayList;
    // ChatAdapter chatAdapter;
    ChatAdapter chatAdapter;
    ImageView image_back, send_image;
    StyleableToast styleableToast;
    PreferenceUtils preferenceUtils;
    Validation_Class validate;
    TextView tv_no_chat;
    RelativeLayout chat_relative;
    EditText et_txt_messsge;
    String pickedImgPath = "",sender_id;
    File file = null;
    int CAMERA_CAPTURE = 1;

    final int PICK_IMAGE = 2;

    private BroadcastReceiver mHandler = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            strSender_id = getIntent().getStringExtra("sender");
            strReqID = getIntent().getStringExtra("request");
            strReciever_id = getIntent().getStringExtra("rec_id");
            reciever_image = getIntent().getStringExtra("rcv_image");
            chat_history(ChatActivity.this);
          /*  if (!isNetworkAvailable()) {
                showToast(getResources().getString(R.string.please_check_your_network_connection));
                return;
            } else {
                strSender_id = getIntent().getStringExtra("sender");
                strReqID = getIntent().getStringExtra("request");
                strReciever_id = getIntent().getStringExtra("rec_id");
                chat_history(ChatActivity.this);
            }*/
        }
    };

    public static String getMimeType(String url) {
        String type = null;
        String extension = MimeTypeMap.getFileExtensionFromUrl(url);
        Log.d("MIME_TYPE_EXT", extension);
        if (extension != null && extension != "") {
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
            //  Log.d("MIME_TYPE", type);
        } else {
            FileNameMap fileNameMap = URLConnection.getFileNameMap();
            type = fileNameMap.getContentTypeFor(url);
        }
        return type;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        LocalBroadcastManager.getInstance(this).registerReceiver(mHandler, new IntentFilter("chat_message"));
        preferenceUtils = new PreferenceUtils(ChatActivity.this);
        validate = new Validation_Class(ChatActivity.this);
        langauge = preferenceUtils.getStringFromPreference(PreferenceUtils.LANGUAGE, "");
         strSender_id = preferenceUtils.getStringFromPreference(PreferenceUtils.USER_ID,"");
       // strSender_id = getIntent().getStringExtra("sender");
        strReciever_id = getIntent().getStringExtra("rec_id");
        strReqID = getIntent().getStringExtra("request");
        reciever_image = getIntent().getStringExtra("rcv_image");
        initializeUI();
        initializeValues();
        chat_history(ChatActivity.this);
    }

    private void initializeUI() {

          /* chatAdapter = new ChatAdapter(ChatActivity.this, arrayList);
        chatRecyclerView.setAdapter(chatAdapter);*/
        chatRecyclerView = findViewById(R.id.chatRecyclerView);
        back_img_details = findViewById(R.id.back_img_details);
        tv_no_chat = findViewById(R.id.tv_no_chat);
        send_image = findViewById(R.id.send_image);
        chat_relative = findViewById(R.id.chat_relative);
        et_txt_messsge = findViewById(R.id.et_txt_messsge);
        img_chat_pic = findViewById(R.id.img_chat_pic);

    }

    private void initializeValues() {

        mLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        chatRecyclerView.setLayoutManager(mLayoutManager);
        back_img_details.setOnClickListener(this);
        LinearLayoutManager manager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        chatRecyclerView.setAdapter(chatAdapter);
        manager.setStackFromEnd(true);
        manager.setReverseLayout(false);
        chatRecyclerView.setLayoutManager(manager);
        chatRecyclerView.setItemAnimator(new DefaultItemAnimator());
        send_image.setOnClickListener(this);
        tv_no_chat.setOnClickListener(this);
        et_txt_messsge.setOnClickListener(this);
        chat_relative.setOnClickListener(this);
        img_chat_pic.setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent(Intent.ACTION_MAIN);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        finish();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.back_img_details:
                onBackPressed();
                break;
            case R.id.send_image:
                strMessge = et_txt_messsge.getText().toString();
                if (strMessge.isEmpty()) {
                    et_txt_messsge.setError(getString(R.string.type_here));
                } else {
                   // et_txt_messsge.setText("");
                    sendMessageService(strMessge, "");
                   // send_message();


                }
                break;
            case R.id.img_chat_pic:

                selectImage();
                /*try {
                    Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(galleryIntent, PICK_IMAGE);



                } catch (IllegalStateException e) {
                    e.printStackTrace();
                }*/
                break;

        }

    }









    private void selectImage() {

        final CharSequence[] options = {getResources().getString(R.string.takeaphoto), getResources().getString(R.string.choosefrmgallery)};

        AlertDialog.Builder builder = new AlertDialog.Builder(ChatActivity.this);
        builder.setTitle(getResources().getString(R.string.photos));
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item)
            {
                if (options[item].toString().equalsIgnoreCase(getResources().getString(R.string.takeaphoto))) {
                    cameraIntent();
                } else if (options[item].toString().equalsIgnoreCase(getResources().getString(R.string.choosefrmgallery))) {
                    choosefromgallery();
                }
                else if (options[item].toString().equalsIgnoreCase(getResources().getString(R.string.cancel))) {
                    dialog.dismiss();
                }

            }
        });
        builder.show();
    }


    private void cameraIntent() {
        Intent intent = new Intent(ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, CAMERA_CAPTURE);
    }

    private void choosefromgallery() {
        try {
            Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(galleryIntent, PICK_IMAGE);
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
    }


    public  void chat_history(final Activity context){

        final API_Services service  = Retrofit_fun.getClient().create(API_Services.class);
        Call<JsonElement> call_retrofit = null;
        call_retrofit = service.view_chat(Constant_keys.API_KEY,langauge,strSender_id,strReciever_id,strReqID);
      /*  final ProgressDialog progressDoalog = new ProgressDialog(ChatActivity.this, R.style.AppCompatAlertDialogStyle);
        progressDoalog.setCancelable(false);
        progressDoalog.setMessage("Please Wait....");
        progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDoalog.show();*/

        call_retrofit.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
               // progressDoalog.dismiss();
                if (response.isSuccessful()) {
                    String searchResponse = response.body().toString();
                    Log.d("chattt", "response  >>" + searchResponse.toString());
                    chat_array = new ArrayList<>();
                    try {
                        JSONObject lObj = new JSONObject(searchResponse);
                        int status = lObj.getInt("status");
                        String message = lObj.getString("message");
                        Log.e("kzjdcvb",message);

                        if (status == 1) {

                            JSONObject json_chat = lObj.getJSONObject("chat_data");
                           // validate.displaySnackbar(chat_relative,message);
                            JSONArray jsonArray = json_chat.getJSONArray("chat_messages");
                            if (jsonArray != null && jsonArray.length() > 0) {
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    Chat_Model chat_model = new Chat_Model();
                                     sender_id = jsonObject.getString("sender_id");
                                    rcv_image = jsonObject.getString("receiver_image");
                                    chat_model.setChat_id(jsonObject.getString("chat_id"));
                                    chat_model.setRequest_id(jsonObject.getString("request_id"));
                                    chat_model.setSender_id(jsonObject.getString("sender_id"));
                                    chat_model.setReciever_id(jsonObject.getString("receiver_id"));
                                    chat_model.setMessage_type(jsonObject.getString("message_type"));


                                    chat_model.setSender_name(jsonObject.getString("sender_name"));
                                    chat_model.setSender_image(jsonObject.getString("sender_image"));
                                    chat_model.setReceiver_name(jsonObject.getString("receiver_name"));
                                    chat_model.setRcv_image(jsonObject.getString("receiver_image"));
                                    chat_model.setDate(jsonObject.getString("time"));


                                    if (jsonObject.getString("message_type").equalsIgnoreCase("image") || jsonObject.getString("message_type").equalsIgnoreCase("0"))
                                    {
                                        chat_model.setImage(jsonObject.getString("message"));
                                    }
                                    else {
                                        chat_model.setMessage(jsonObject.getString("message"));
                                    }


                                    chat_array.add(chat_model);

                                    /*if (sender_id.equalsIgnoreCase(sender_id))
                                    {

                                    }*/
                                }

                                chatRecyclerView.setVisibility(View.VISIBLE);
                                tv_no_chat.setVisibility(View.GONE);
                                chatAdapter = new ChatAdapter(context, chat_array, sender_id, reciever_image);
                                chatAdapter.notifyDataSetChanged();
                                chatRecyclerView.setAdapter(chatAdapter);
                            }
                            else {
                                validate.displaySnackbar(chat_relative,message);
                                chatRecyclerView.setVisibility(View.GONE);
                                tv_no_chat.setVisibility(View.VISIBLE);
                                tv_no_chat.setText(message);
                            }
                        }

                    } catch (JSONException e) {
                        Log.e("error", e.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                //  progressDoalog.dismiss();
                Log.d("Error Call", ">>>>" + call.toString());
                Log.d("Error", ">>>>" + t.toString());
            }
        });
    }





    public void sendMessageService(String strMessage, String pickedImgPath)
    {
        showLoadingToast(ChatActivity.this);
        File file = null;
        MultipartBody.Part image_profile = null;

        if (pickedImgPath != null && !pickedImgPath.isEmpty()) {
            file = new File(pickedImgPath);
            RequestBody requestBody = RequestBody.create(MediaType.parse(getMimeType(pickedImgPath)), file);
            image_profile = MultipartBody.Part.createFormData("chat_file", file.getName(), requestBody);
            Log.d("Image", ">>>>>>>>>>" + image_profile);
        }


        RequestBody r_api_key = RequestBody.create(MediaType.parse("multipart/form-data"), Constant_keys.API_KEY);
        RequestBody r_lang = RequestBody.create(MediaType.parse("multipart/form-data"), langauge);
        RequestBody r_userID = RequestBody.create(MediaType.parse("multipart/form-data"), preferenceUtils.getStringFromPreference(PreferenceUtils.USER_ID, ""));

        RequestBody r_reqID = RequestBody.create(MediaType.parse("multipart/form-data"), strReqID);
        RequestBody r_providerID = RequestBody.create(MediaType.parse("multipart/form-data"), strReciever_id);
        RequestBody r_strMessage = RequestBody.create(MediaType.parse("multipart/form-data"), strMessage);

        final API_Services service = Retrofit_fun.getClient().create(API_Services.class);
        Call<JsonElement> callRetrofit = null;
        // callRetrofit = service.send_messge(Constant_keys.API_KEY, langauge, strSender_id, strReciever_id, strMessge,strReqID);

        callRetrofit = service.send_message(r_api_key, r_lang, r_reqID, r_userID, r_providerID, r_strMessage, image_profile);

        final ProgressDialog progressDoalog = new ProgressDialog(ChatActivity.this, R.style.AppCompatAlertDialogStyle);
        progressDoalog.setCancelable(false);
        progressDoalog.setMessage(getResources().getString(R.string.please_wait));
        progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDoalog.hide();

        callRetrofit.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                progressDoalog.dismiss();
                if (response.isSuccessful()) {
                    hideLoadingToast();
                    String searchResponse = response.body().toString();
                    Log.d("Registration", "response  >>" + searchResponse.toString());

                    try {
                        JSONObject regObject = new JSONObject(searchResponse);
                        int status = regObject.getInt("status");
                        String message = regObject.getString("message");
                        if (status == 1) {
//                            helperClass.displaySnackbar(chatLayout, message);
                            et_txt_messsge.setText("");
                            chat_history(ChatActivity.this);
                        } else {
                            validate.displaySnackbar(chat_relative, message);
                        }
                    } catch (JSONException e) {
                        Log.e("error", e.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                progressDoalog.dismiss();
                hideLoadingToast();
                Log.d("Error Call", ">>>>" + call.toString());
                Log.d("Error", ">>>>" + t.toString());
            }
        });

    }







    @Override
    protected void onResume() {
        super.onResume();
        Offer_status.activityResumed();

    }

    @Override
    protected void onPause() {
        super.onPause();
        Offer_status.activityPaused();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mHandler);
    }

    public void showLoadingToast(Context context) {
        styleableToast = new StyleableToast(context, getString(R.string.loading), 50000);
        styleableToast.setBackgroundColor(Color.parseColor("#C39F52"));
        styleableToast.setTextColor(Color.parseColor("#FFFFFF"));
        styleableToast.setCornerRadius(5);
        styleableToast.setIcon(R.drawable.loading);
        styleableToast.spinIcon();
        styleableToast.show();
    }

    public void hideLoadingToast() {
        if (styleableToast != null) {
            styleableToast.setDuration(100);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
       /* if (requestCode == PICK_IMAGE) {
            if (resultCode == RESULT_OK) {
                onSelectFromGalleryResult(data);
            }
        }
        else*/
        if (requestCode == CAMERA_CAPTURE) {
            if (resultCode == RESULT_OK) {
                onCaptureImageResult(data);
            }
        }


        if (requestCode == PICK_IMAGE)
        {
            if (resultCode == RESULT_OK)
            {

                Uri picUri = data.getData();
                String[] filePath = {MediaStore.Images.Media.DATA};
                Cursor c = getApplicationContext().getContentResolver().query(picUri, filePath, null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePath[0]);

                //  PickedImgPath = GalleryUriToPath.getPath(getApplicationContext(), picUri);


                try {
                    pickedImgPath = GalleryUriToPath.getPath(getApplicationContext(), picUri);
                    Log.e("sadfm",pickedImgPath);
                    if (validate.checkInternetConnection(ChatActivity.this)) {
                        sendMessageService("", pickedImgPath);
                    } else {
                        validate.displaySnackbar(chat_relative, getResources().getString(R.string.please_check_your_network_connection));
                    }
                    //  String file_profile = pickedImgPath.substring(pickedImgPath.lastIndexOf("/") + 1);
                    Bitmap bm = BitmapFactory.decodeStream(getApplicationContext().getContentResolver().openInputStream(picUri));
                    //img_driver_profile.setImageBitmap(bm);

                }
                catch (FileNotFoundException e) {
                    e.printStackTrace();
                }



                c.close();
            }
        }


    }
    private void onCaptureImageResult(Intent data)
    {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");
        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
            //   PickedImgPath = destination.getAbsolutePath();

            pickedImgPath = destination.getAbsolutePath();
            Log.e("pic",pickedImgPath);
            String file_profile = pickedImgPath.substring(pickedImgPath.lastIndexOf("/") + 1);
            if (validate.checkInternetConnection(ChatActivity.this)) {
                sendMessageService("", pickedImgPath);
            } else {
                validate.displaySnackbar(chat_relative, getResources().getString(R.string.please_check_your_network_connection));
            }
            //txt_profile_photo.setText(file_profile);
            //  img_driver_profile.setImageBitmap(thumbnail);

            Log.e("Camera Path", destination.getAbsolutePath());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        //String filename = PickedImgPath.substring(PickedImgPath.lastIndexOf("/") + 1);
        // txt_image_only.setText(filename);
        // txt_image_only.setText("photo Id has selected");

    }


    private void onSelectFromGalleryResult(Intent data) {
        Bitmap bm = null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());

                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                Cursor cursor = getContentResolver().query(selectedImage,
                        filePathColumn, null, null, null);
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                pickedImgPath = getPath(this, selectedImage);
                if (validate.checkInternetConnection(ChatActivity.this)) {
                    sendMessageService("", pickedImgPath);
                } else {
                    validate.displaySnackbar(chat_relative, getResources().getString(R.string.please_check_your_network_connection));
                }
                Log.e("Gallery Path", pickedImgPath);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
//        user_profile_image.setImageBitmap(bm);
    }



}






















