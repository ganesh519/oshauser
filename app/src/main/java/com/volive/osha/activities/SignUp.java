package com.volive.osha.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.JsonElement;
import com.volive.osha.R;
import com.volive.osha.helperclasses.Constant_keys;
import com.volive.osha.helperclasses.Validation_Class;
import com.volive.osha.util_classes.API_Services;
import com.volive.osha.util_classes.PreferenceUtils;
import com.volive.osha.util_classes.Retrofit_fun;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SignUp extends BaseActivity implements  View.OnClickListener{
    TextView termsandcond, txt_signin;
    String st_email,st_name,st_mobile,st_password,st_confirm_pwd,st_agree_terms;
    EditText full_name_edit_text,email_edit_text,mobile_edit_text,create_pswrd_edit_text,confirm_pswrd_edit_text;
    Button btn_signup;
    ImageView back_img_details;
    String activityName = "";
    CheckBox check_box;
    LinearLayout gender_layout;
    AutoCompleteTextView gender_actv;
    String agree_tc,language;
    PreferenceUtils preferenceUtils;
    Validation_Class validate;
     RelativeLayout rel_layout;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        FirebaseApp.initializeApp(this);
        preferenceUtils = new PreferenceUtils(SignUp.this);
        validate = new Validation_Class(SignUp.this);
        language = preferenceUtils.getStringFromPreference(PreferenceUtils.LANGUAGE,"");
/*
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            activityName = extras.getString("Activity");
        }*/

        intializeUI();
        initializeValues();


    }

    private void intializeUI() {
        check_box = (CheckBox) findViewById(R.id.check_box);
        termsandcond = (TextView) findViewById(R.id.termsandcond);
        termsandcond.setText(Html.fromHtml(getString(R.string.terms_and_condition)));
        btn_signup = (Button) findViewById(R.id.btn_signup);
        back_img_details = findViewById(R.id.back_img_details);
        txt_signin = findViewById(R.id.txt_signin);
        full_name_edit_text = findViewById(R.id.full_name_edit_text);
        email_edit_text = findViewById(R.id.email_edit_text);
        mobile_edit_text = findViewById(R.id.mobile_edit_text);
        create_pswrd_edit_text = findViewById(R.id.create_pswrd_edit_text);
        confirm_pswrd_edit_text = findViewById(R.id.confirm_pswrd_edit_text);
        rel_layout = findViewById(R.id.rel_layout);

    }

    private void initializeValues() {


        check_box .setOnClickListener(this);
        btn_signup.setOnClickListener(this);
        termsandcond.setOnClickListener(this);
        back_img_details.setOnClickListener(this);
        rel_layout.setOnClickListener(this);
        txt_signin.setOnClickListener(this);
        if (check_box.isChecked()) {
            agree_tc = "1";
        } else {
            agree_tc = "0";
        }

        check_box.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (check_box.isChecked()) {
                    agree_tc = "1";
                } else {
                    agree_tc = "0";
                }
            }
        });

    }





    @Override
    public void onClick(View v) {

        switch (v.getId())
        {

           /* case R.id.check_box:
                check_box_condition();
                break;*/

            case R.id.btn_signup:
                st_name = full_name_edit_text.getText().toString();
                st_email = email_edit_text.getText().toString();
                st_password = create_pswrd_edit_text.getText().toString();
                st_confirm_pwd = confirm_pswrd_edit_text.getText().toString();
                st_mobile = mobile_edit_text.getText().toString();

                if (st_name.isEmpty())
                {
                    full_name_edit_text.setError(getString(R.string.enter_full_name));
                }
                else if (st_email.isEmpty())
                {
                    email_edit_text.setError(getString(R.string.enter_email));
                }
               else if (!validate.validateEmail(email_edit_text.getText().toString().trim()))
                   {
                    validate.displaySnackbar(rel_layout, getString(R.string.please_enter_your_valid_email));
                }
               else if (st_mobile.isEmpty())
                {
                    mobile_edit_text.setError(getString(R.string.enter_number));
                }
               else if ( st_mobile.length() != 10)
                {
                    mobile_edit_text.setError(getString(R.string.valid_number));
                }
                else if (st_password.isEmpty())
                {
                    create_pswrd_edit_text.setError(getString(R.string.enter_password));
                }else if (st_confirm_pwd.isEmpty())
                {
                    confirm_pswrd_edit_text.setError(getString(R.string.confirm_password));
                }
                else if(!st_password.equals(st_confirm_pwd))
                {
                    Toast.makeText(getApplicationContext(),getString(R.string.pwd_not_matching),Toast.LENGTH_SHORT).show();
                }
                   else if (agree_tc.isEmpty() || agree_tc.equalsIgnoreCase("0"))
                {
                    validate.displaySnackbar(rel_layout,  getString(R.string.agree_terms_cond));
                }
                   else  if (!isNetworkAvailable())
                   {
                       showToast(getString(R.string.please_check_your_network_connection));
                       return;
                   }
                   else {
                    signup_service();
                    /*Intent i = new Intent(SignUp.this, OTPScreen.class);
                    startActivity(i);
                    // close this activity
                    finish();*/
                }
                break;
            case  R.id.termsandcond:
                Intent a = new Intent(SignUp.this, TermsAndConditionsActivity.class);
                startActivity(a);
                break;
            case R.id.back_img_details:
                onBackPressed();
                break;
            case R.id.txt_signin:
                Intent login = new Intent(SignUp.this, LoginActivty.class);
                startActivity(login);
                finish();
                break;
        }

    }

    private void check_box_condition()
    {
        if (check_box.isChecked())
        {
            agree_tc = "1";

        }
        else {
            agree_tc = "0";
        }
        Log.e("dsajkl",agree_tc);
    }



    private void signup_service() {

        final API_Services service =Retrofit_fun.getClient().create(API_Services.class);

        Call<JsonElement> call_retrofit = null;
        call_retrofit = service.sign_up(Constant_keys.API_KEY,language,st_name,st_email,st_mobile,st_password,st_confirm_pwd,agree_tc,Constant_keys.device_type, FirebaseInstanceId.getInstance().getToken());
        final ProgressDialog progressDoalog = new ProgressDialog(this, R.style.AppCompatAlertDialogStyle);
        progressDoalog.setCancelable(false);
        progressDoalog.setMessage(getResources().getString(R.string.please_wait));
        progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDoalog.show();

        call_retrofit .enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response)
            {
                progressDoalog.dismiss();

                if (response.isSuccessful())
                {
                    Log.e("edvan",response.body().toString());

                    try {
                        JSONObject jsonObject = new JSONObject(response.body().toString());

                        int status = jsonObject.getInt("status");
                        String message  = jsonObject.getString("message");
                        showToast(message);

                        JSONObject user_json = jsonObject.getJSONObject("user_details");
                        String user_id = user_json.getString("user_id");
                        String name = user_json.getString("name");
                        String username = user_json.getString("username");
                        String email = user_json.getString("email");
                        String phone = user_json.getString("phone");
                        String dob = user_json.getString("dob");
                        String gender = user_json.getString("gender");
                        String password = user_json.getString("password");
                        String auth_level = user_json.getString("auth_level");
                        String profile_pic = user_json.getString("profile_pic");
                        String user_status = user_json.getString("user_status");
                        String otp = user_json.getString("otp");
                        String otp_status = user_json.getString("otp_status");


                        preferenceUtils.saveString(PreferenceUtils.USER_ID,user_id);
                        preferenceUtils.saveString(PreferenceUtils.User,name);
                        preferenceUtils.saveString(PreferenceUtils.UserName,username);
                        preferenceUtils.saveString(PreferenceUtils.Email,email);
                        preferenceUtils.saveString(PreferenceUtils.Mobile,phone);
                        preferenceUtils.saveString(PreferenceUtils.password_new,password);
                        preferenceUtils.saveString(PreferenceUtils.authlevel,auth_level);
                        preferenceUtils.saveString(PreferenceUtils.OTP_STATUS,otp_status);
                        preferenceUtils.saveBoolean(PreferenceUtils.LOGIN_TYPE, true);
                        Intent i = new Intent(SignUp.this, OTPScreen.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        i.putExtra("mobile",st_mobile);
                        i.putExtra("otp",otp);
                        startActivity(i);
                        // close this activity
                        //  finish();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }



                }



            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                progressDoalog.dismiss();
                Log.e("wsljndf",t.toString());
                Log.e("dkm",call.toString());

            }
        });



    }

    @Override
    public void onBackPressed() {
        finish();
    }


}
