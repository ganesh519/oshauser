package com.volive.osha.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.JsonElement;
import com.volive.osha.R;
import com.volive.osha.helperclasses.Constant_keys;
import com.volive.osha.helperclasses.Validation_Class;
import com.volive.osha.util_classes.API_Services;
import com.volive.osha.util_classes.PreferenceUtils;
import com.volive.osha.util_classes.Retrofit_fun;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class InvoiceDetailsActivity extends BaseActivity implements View.OnClickListener {
    TextView txt_orderId, txt_customerName, toolbar_title, txt_invoiceNo, txt_invoice_date, txt_issue_type, txt_estimatedPrice, txt_addtional_price,
            txt_subTotal, txt_coupon_price, txt_total_price, txt_coupan, txt_vat_percentage;
    ImageView back_img_details;
    EditText edit_coupon_txt;
    Button btn_apply_coupon, btn_confrom_pay,btn_confrom_pay_coupn;
    String stReqID = "", language, stUserID;
    PreferenceUtils preferenceUtils;
    Validation_Class validate;
    String request_id = "",  wallet_amount = "",discount_amount = "";
    String total_amount = "",order_id="";

    String stc_total_amount,stc_dis_amount,stc_coupan;
    TextView txt_pickUP, txt_dropTo, txt_vatTotal, txt_offer_price;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invoice_details);
        preferenceUtils = new PreferenceUtils(InvoiceDetailsActivity.this);
        validate = new Validation_Class(InvoiceDetailsActivity.this);
        language = preferenceUtils.getStringFromPreference(PreferenceUtils.LANGUAGE, "");
        stUserID = preferenceUtils.getStringFromPreference(PreferenceUtils.USER_ID, "");
        initializeUi();
        initializeValues();
        stReqID = getIntent().getStringExtra("req_id");
        if (!isNetworkAvailable()) {
            showToast(getString(R.string.please_check_your_network_connection));
            return;
        } else {

            invoice_details();


        }

    }

    private void initializeUi() {
        //TextView
        txt_offer_price = findViewById(R.id.txt_offer_price);
        txt_vat_percentage = findViewById(R.id.txt_vat_percentage);
        txt_coupan = findViewById(R.id.txt_coupan);
        txt_dropTo = findViewById(R.id.txt_dropTo);
        txt_vatTotal = findViewById(R.id.txt_vatTotal);
        txt_pickUP = findViewById(R.id.txt_pickUP);
        toolbar_title = findViewById(R.id.toolbar_title);
        txt_orderId = findViewById(R.id.txt_orderId);
        txt_customerName = findViewById(R.id.txt_customerName);
        txt_invoiceNo = findViewById(R.id.txt_invoiceNo);
        txt_invoice_date = findViewById(R.id.txt_invoice_date);
        txt_issue_type = findViewById(R.id.txt_issue_type);
        btn_confrom_pay_coupn = findViewById(R.id.btn_confrom_pay_coupn);

        //txt_estimatedPrice = findViewById(R.id.txt_estimatedPrice);
        // txt_addtional_price = findViewById(R.id.txt_addtional_price);
        txt_subTotal = findViewById(R.id.txt_subTotal);
        txt_coupon_price = findViewById(R.id.txt_coupon_price);
        txt_total_price = findViewById(R.id.txt_total_price);
        //ImageView
        back_img_details = findViewById(R.id.back_img_details);

        //Button
        btn_confrom_pay = findViewById(R.id.btn_confrom_pay);
        btn_apply_coupon = findViewById(R.id.btn_apply_coupon);
        //EditText
        edit_coupon_txt = findViewById(R.id.edit_coupon_txt);
    }

    private void initializeValues() {
        txt_vatTotal.setOnClickListener(this);
        txt_dropTo.setOnClickListener(this);
        back_img_details.setOnClickListener(this);
        btn_confrom_pay.setOnClickListener(this);
        btn_apply_coupon.setOnClickListener(this);
        txt_pickUP.setOnClickListener(this);
        btn_confrom_pay_coupn.setOnClickListener(this);
    }

    private void invoice_details() {
        final API_Services services = Retrofit_fun.getClient().create(API_Services.class);
        Call<JsonElement> call_notification = null;
        //  call_notification = services.wallet_summary(Constant_keys.API_KEY,language,preferenceUtils.getStringFromPreference(PreferenceUtils.USER_ID,""),"1");
        call_notification = services.invoice_details(Constant_keys.API_KEY, language, stReqID);
        final ProgressDialog progressDoalog = new ProgressDialog(InvoiceDetailsActivity.this, R.style.AppCompatAlertDialogStyle);
        progressDoalog.setCancelable(false);
        progressDoalog.setMessage("Please Wait....");
        progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDoalog.show();

        call_notification.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                progressDoalog.dismiss();
                if (response.isSuccessful()) {
                    Log.e("sjwlb", response.body().toString());
                    try {
                        JSONObject json_obejct = new JSONObject(response.body().toString());
                        int status = json_obejct.getInt("status");
                        //String   message = json_obejct.getString("message");
                        if (status == 1) {
                            JSONObject json_invoice = json_obejct.getJSONObject("invoice");
                            request_id = json_invoice.getString("request_id");
                             order_id = json_invoice.getString("order_id");
                            String pickup_from = json_invoice.getString("pickup_from");
                            String drop_to = json_invoice.getString("drop_to");
                           /* String passengers_num = json_invoice.getString("passengers_num");
                            String car_type = json_invoice.getString("car_type");
                            String delivery_items = json_invoice.getString("delivery_items");
                            String description = json_invoice.getString("description");
                            String address = json_invoice.getString("address");
                            String latitude_pick = json_invoice.getString("latitude_pick");
                            String longitude_pick = json_invoice.getString("longitude_pick");
                            String latitude_drop = json_invoice.getString("latitude_drop");
                            String longitude_drop = json_invoice.getString("longitude_drop");
                            String request_timer = json_invoice.getString("request_timer");
                            String updated_on = json_invoice.getString("updated_on");
                            String created_on = json_invoice.getString("created_on");
                            String provider_id = json_invoice.getString("provider_id");*/
                            String invoice_number = json_invoice.getString("invoice_number");
                            //String invoice_status = json_invoice.getString("invoice_status");
                             total_amount = json_invoice.getString("total_amount");
                            String additional_amount = json_invoice.getString("additional_amount");
                            String offer_price = json_invoice.getString("offer_price");
                          //  String provider_amount = json_invoice.getString("provider_amount");
                            String coupon_code = json_invoice.getString("coupon_code");
                             discount_amount = json_invoice.getString("discount_amount");
                          /*  String invoice_description = json_invoice.getString("invoice_description");
                            String request_rating = json_invoice.getString("request_rating");
                            String rating_comments = json_invoice.getString("rating_comments");
                            String request_rating_status = json_invoice.getString("request_rating_status");*/
                             wallet_amount = json_invoice.getString("wallet_amount");
                            String date = json_invoice.getString("date");
                            String vat_deduction = json_invoice.getString("vat_deduction");
                            String vat = json_invoice.getString("vat");
                            String name = json_invoice.getString("name");

                            /*btn_confrom_pay_coupn.setVisibility(View.GONE);
                            btn_confrom_pay.setVisibility(View.VISIBLE);*/

                            txt_vat_percentage.setText("(" + " " + vat + "%" + ")");
                            txt_orderId.setText("#" + " " + order_id);
                            txt_customerName.setText(name);
                            txt_invoiceNo.setText(invoice_number);
                            txt_invoice_date.setText(date);
                            txt_pickUP.setText(pickup_from);
                            txt_dropTo.setText(drop_to);
                            txt_offer_price.setText(offer_price + " " + "SAR");
                            txt_vatTotal.setText(vat_deduction + " " + "SAR");
                            txt_subTotal.setText(total_amount + " " + "SAR");
                            txt_total_price.setText(total_amount + " "+"SAR");
                            //txt_coupon_price.setText(discount_amount + " " + "SAR");


                        }


                    } catch (Exception e) {
                        progressDoalog.dismiss();
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                progressDoalog.dismiss();
                Log.e("Erro:", call.toString());
                Log.e("Error:", t.toString());

            }
        });

    }


    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.back_img_details:
                finish();
                break;
            case R.id.btn_confrom_pay_coupn:
                intent = new Intent(InvoiceDetailsActivity.this, PaymentActivity.class);
                intent.putExtra("total_amount",stc_total_amount);
                intent.putExtra("wallet_amount",wallet_amount);
                intent.putExtra("req_id",request_id);
                intent.putExtra("coupon",stc_coupan);
                intent.putExtra("dis_amount",stc_dis_amount);
                intent.putExtra("order_id",order_id);
                startActivity(intent);

                break;
            case R.id.btn_confrom_pay:

                intent = new Intent(InvoiceDetailsActivity.this, PaymentActivity.class);

                intent.putExtra("total_amount",total_amount);
                intent.putExtra("wallet_amount",wallet_amount);
                intent.putExtra("req_id",request_id);
                intent.putExtra("coupon",txt_coupan.getText().toString());
                intent.putExtra("dis_amount",discount_amount);
                intent.putExtra("order_id",order_id);
                startActivity(intent);


                break;
            case R.id.btn_apply_coupon:

                String coupon_value = edit_coupon_txt.getText().toString();

                if (coupon_value.isEmpty()) {
                    edit_coupon_txt.setError(getString(R.string.enter_your_coupon_code));
                } else if (!isNetworkAvailable()) {
                    showToast(getString(R.string.please_check_your_network_connection));
                    return;
                } else {
                    coupon_apply(coupon_value);
                }


                break;
        }
    }

    private void coupon_apply(String coupon_value) {
        final API_Services services = Retrofit_fun.getClient().create(API_Services.class);
        Call<JsonElement> call_notification = null;
        //  call_notification = services.wallet_summary(Constant_keys.API_KEY,language,preferenceUtils.getStringFromPreference(PreferenceUtils.USER_ID,""),"1");
        call_notification = services.apply_coupon(Constant_keys.API_KEY, language, coupon_value, request_id);
        final ProgressDialog progressDoalog = new ProgressDialog(InvoiceDetailsActivity.this, R.style.AppCompatAlertDialogStyle);
        progressDoalog.setCancelable(false);
        progressDoalog.setMessage("Please Wait....");
        progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDoalog.show();

        call_notification.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                progressDoalog.dismiss();
                if (response.isSuccessful()) {
                    Log.e("sjwlb", response.body().toString());
                    try {
                        JSONObject json_obejct = new JSONObject(response.body().toString());
                        int status = json_obejct.getInt("status");
                        //String   message = json_obejct.getString("message");
                        if (status == 1) {

                            edit_coupon_txt.setText("");
                            /*btn_confrom_pay_coupn.setVisibility(View.VISIBLE);
                            btn_confrom_pay.setVisibility(View.GONE);*/

                            String   message = json_obejct.getString("message");
                            showToast(message);
                            JSONObject json_invoice = json_obejct.getJSONObject("details");
                             //stc_total_amount = json_invoice.getString("total_amount");
                             discount_amount = json_invoice.getString("discount_amount");
                             total_amount = json_invoice.getString("final_amount");
                            String final_amount = json_invoice.getString("final_amount");
                            String coupon_applied = json_invoice.getString("coupon_applied");
                            stc_coupan = json_invoice.getString("coupon_applied");
                            txt_coupan.setVisibility(View.VISIBLE);
                            txt_coupan.setText(coupon_applied);
                           // txt_subTotal.setText(total_amount + " " + "SAR");
                            txt_total_price.setText(final_amount + " "+"SAR");
                            txt_coupon_price.setText(discount_amount + " " + "SAR");

                        }
                        else {
                            String message = json_obejct.getString("message");
                            showToast(message);
                        }


                    } catch (Exception e) {
                        progressDoalog.dismiss();
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                progressDoalog.dismiss();
                Log.e("Erro:", call.toString());
                Log.e("Error:", t.toString());

            }
        });

    }


    @Override
    public void onBackPressed() {
        finish();
    }
}
