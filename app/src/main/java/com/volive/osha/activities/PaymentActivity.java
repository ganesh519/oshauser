package com.volive.osha.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonElement;
import com.paytabs.paytabs_sdk.payment.ui.activities.PayTabActivity;
import com.paytabs.paytabs_sdk.utils.PaymentParams;
import com.volive.osha.R;
import com.volive.osha.adapters.ShopAdapter;
import com.volive.osha.helperclasses.Constant_keys;
import com.volive.osha.helperclasses.Validation_Class;
import com.volive.osha.util_classes.API_Services;
import com.volive.osha.util_classes.PreferenceUtils;
import com.volive.osha.util_classes.Retrofit_fun;
import com.yarolegovich.discretescrollview.DSVOrientation;
import com.yarolegovich.discretescrollview.DiscreteScrollView;
import com.yarolegovich.discretescrollview.InfiniteScrollAdapter;
import com.yarolegovich.discretescrollview.transform.ScaleTransformer;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class PaymentActivity extends BaseActivity implements View.OnClickListener, DiscreteScrollView.OnItemChangedListener {
    public static boolean checkedLayout = false;
    public static boolean checkedCashLayout = false;

    TextView cod_txt, txt_payment_amount, toolbar_title, card_txt, txt_add_card, total_wallet, txt_pay_cash;
    ImageView back_img_payment, card_img, cod_img, unselected_radio, selected_radio, money_icon;
    LinearLayout card_layout, card_details_layout, cards_layout_txt, cash_layout;
    RelativeLayout cod_layout;
    ImageView card_radio_btn, cod_radio_btn, card_radio_btn1, cod_radio_btn1;
    EditText et_card_holder_name, et_card_number, et_card_expiry, et_card_cvv;
    Button btn_payment;
    String activityName = "";
    Spinner spinner;
    String strCardId = "", strPaymentType = "wallet", strDiscountPrice = "",order_id="",pt_transaction_id="";


    public  static String st_total_amount, st_wallet_amount, st_req_id, st_discount_amount, st_coupon_code;

    CheckBox cb;

    int[] myImageList = new int[]{R.drawable.card_image, R.drawable.card_image1, R.drawable.card_image};
    PreferenceUtils preferenceUtils;
    Validation_Class validation_class;
    String User_id, language;
    private DiscreteScrollView itemPicker;
    private InfiniteScrollAdapter infiniteAdapter;
    double totalamount;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        preferenceUtils = new PreferenceUtils(PaymentActivity.this);
        validation_class = new Validation_Class(PaymentActivity.this);
        User_id = preferenceUtils.getStringFromPreference(PreferenceUtils.USER_ID, "");
        language = preferenceUtils.getStringFromPreference(PreferenceUtils.LANGUAGE, "");

        st_total_amount = getIntent().getStringExtra("total_amount");
        order_id = getIntent().getStringExtra("order_id");
        st_wallet_amount = getIntent().getStringExtra("wallet_amount");
        st_req_id = getIntent().getStringExtra("req_id");
        st_discount_amount = getIntent().getStringExtra("dis_amount");
        st_coupon_code = getIntent().getStringExtra("coupon");
        Log.e("dfgdfcghfh",st_total_amount);

        totalamount= Double.parseDouble(st_total_amount);

        initializeUI();
        initializeValues();
        set_values();

        checkedCashLayout = true;

        cod_layout.setBackgroundColor(getResources().getColor(R.color.light_yellow));
        cards_layout_txt.setVisibility(View.GONE);
        itemPicker.setVisibility(View.GONE);
        cb = findViewById(R.id.cb);
        itemPicker.setOrientation(DSVOrientation.HORIZONTAL);
        itemPicker.addOnItemChangedListener(PaymentActivity.this);
        infiniteAdapter = InfiniteScrollAdapter.wrap(new ShopAdapter(myImageList));
        itemPicker.setAdapter(infiniteAdapter);
        // itemPicker.setItemTransitionTimeMillis(DiscreteScrollViewOptions.getTransitionTime());
        itemPicker.setItemTransformer(new ScaleTransformer.Builder()
                .setMinScale(0.8f)
                .build());

//        // onItemChanged(data.get(0));
//        cb.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (cb.isChecked()) {
//                    cb.setButtonDrawable(R.drawable.checked_cb);
//                } else {
//                    cb.setButtonDrawable(R.drawable.small_check);
//                }
//            }
//        });
    }

    private void set_values() {
        txt_payment_amount.setText(st_total_amount + " " + "SAR");
        total_wallet.setText(st_wallet_amount + " " + "SAR");
    }

    @Override
    public void onCurrentItemChanged(@Nullable RecyclerView.ViewHolder viewHolder, int adapterPosition) {
        int positionInDataSet = infiniteAdapter.getRealPosition(adapterPosition);
        // onItemChanged(data.get(positionInDataSet));
    }


    private void initializeUI() {


        //TextView
        toolbar_title = findViewById(R.id.toolbar_title);
        cod_txt = findViewById(R.id.cod_txt);
        txt_payment_amount = findViewById(R.id.txt_payment_amount);
        card_txt = findViewById(R.id.card_txt);
        txt_add_card = findViewById(R.id.txt_add_card);
        total_wallet = findViewById(R.id.total_wallet);
        txt_pay_cash = findViewById(R.id.txt_pay_cash);

        //EditText
        et_card_holder_name = findViewById(R.id.et_card_holder_name);
        et_card_number = findViewById(R.id.et_card_number);
        et_card_expiry = findViewById(R.id.et_card_expiry);
        et_card_cvv = findViewById(R.id.et_card_cvv);
        //Button
        btn_payment = findViewById(R.id.btn_payment);
        //LinearLayout
        card_layout = findViewById(R.id.card_layout);
        cod_layout = findViewById(R.id.cod_layout);
        card_details_layout = findViewById(R.id.card_details_layout);
        cards_layout_txt = findViewById(R.id.cards_layout_txt);
        cash_layout = findViewById(R.id.cash_layout);
        //RadioButton
        card_radio_btn = findViewById(R.id.card_radio_btn);
        cod_radio_btn = findViewById(R.id.cod_radio_btn);
        card_radio_btn1 = findViewById(R.id.card_radio_btn1);
        cod_radio_btn1 = findViewById(R.id.cod_radio_btn1);
        //ImageView
        back_img_payment = findViewById(R.id.back_img_payment);
        card_img = findViewById(R.id.card_img);
        cod_img = findViewById(R.id.cod_img);
        unselected_radio = findViewById(R.id.unselected_radio);
        selected_radio = findViewById(R.id.selected_radio);
        money_icon = findViewById(R.id.money_icon);

//        //CheckBox
//        cb_saveCard = findViewById(R.id.cb_saveCard);
        itemPicker = findViewById(R.id.picker);


        //Spinner
        //  spinner = findViewById(R.id.spinner);


    }

    private void initializeValues() {
        cod_img.setImageResource(R.drawable.selected_cash_img);
        back_img_payment.setOnClickListener(this);
        card_layout.setOnClickListener(this);
        cod_layout.setOnClickListener(this);
        txt_add_card.setOnClickListener(this);
        btn_payment.setOnClickListener(this);
        cash_layout.setOnClickListener(this);
//        cb_saveCard.setOnClickListener(this);


        //Spinner

        final ArrayAdapter<String> cardsArray = new ArrayAdapter<String>(PaymentActivity.this, android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.cards_array));
        cardsArray.setDropDownViewResource(android.R.layout.simple_list_item_1);
        //    spinner.setAdapter(cardsArray);


      /*  spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
*/

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.cash_layout:
                checkedCashLayout = true;
                checkedLayout = false;

                if (checkedCashLayout) {
                    cash_layout.setBackgroundColor(getResources().getColor(R.color.light_yellow));
                    card_layout.setBackgroundColor(getResources().getColor(R.color.backgroundColor));
                    cod_layout.setBackgroundColor(getResources().getColor(R.color.backgroundColor));

                    money_icon.setImageResource(R.drawable.selected_money_icon);
                    card_img.setImageResource(R.drawable.unselected_card_img);
                    cod_img.setImageResource(R.drawable.unselected_cash_img);
                    strPaymentType = "cash";
                    unselected_radio.setVisibility(View.GONE);
                    card_radio_btn1.setVisibility(View.GONE);
                    cod_radio_btn.setVisibility(View.GONE);

                    card_radio_btn.setVisibility(View.VISIBLE);
                    cod_radio_btn1.setVisibility(View.VISIBLE);
                    selected_radio.setVisibility(View.VISIBLE);

                    cards_layout_txt.setVisibility(View.GONE);
                    itemPicker.setVisibility(View.GONE);
                    card_details_layout.setVisibility(View.GONE);

                    cod_txt.setTextColor(getResources().getColor(R.color.textColor));
                    total_wallet.setTextColor(getResources().getColor(R.color.textColor));
                    card_txt.setTextColor(getResources().getColor(R.color.textColor));
                    txt_pay_cash.setTextColor(getResources().getColor(R.color.colorPrimary));
                } else {
                    checkedCashLayout = false;
                    cod_radio_btn1.setVisibility(View.GONE);
                    cod_radio_btn.setVisibility(View.VISIBLE);
                    cod_txt.setTextColor(getResources().getColor(R.color.colorBlack));
                    cod_img.setImageResource(R.drawable.unselected_cash_img);
                    cod_layout.setBackgroundColor(getResources().getColor(R.color.backgroundColor));
                    //cards_layout_txt.setVisibility(View.VISIBLE);

                    cards_layout_txt.setVisibility(View.GONE);
                    itemPicker.setVisibility(View.VISIBLE);
                    //strPaymentType = "card";
                }

                break;

            case R.id.back_img_payment:
               /* Intent i = new Intent(PaymentActivity.this, InvoiceDetailsActivity.class);
                startActivity(i);*/
                finish();
                break;

            case R.id.card_layout:
                checkedCashLayout = false;
                checkedLayout = true;
                if (checkedLayout) {
                    cash_layout.setBackgroundColor(getResources().getColor(R.color.backgroundColor));
                    card_layout.setBackgroundColor(getResources().getColor(R.color.light_yellow));
                    cod_layout.setBackgroundColor(getResources().getColor(R.color.backgroundColor));

                    money_icon.setImageResource(R.drawable.unselected_money_icon);
                    card_img.setImageResource(R.drawable.selected_card_img);
                    cod_img.setImageResource(R.drawable.unselected_cash_img);
                    strPaymentType = "card";

                    checkedLayout = false;
                    unselected_radio.setVisibility(View.VISIBLE);
                    card_radio_btn1.setVisibility(View.VISIBLE);
                    cod_radio_btn.setVisibility(View.GONE);

                    card_radio_btn.setVisibility(View.GONE);
                    cod_radio_btn1.setVisibility(View.VISIBLE);
                    selected_radio.setVisibility(View.GONE);

                    cards_layout_txt.setVisibility(View.GONE);
                    itemPicker.setVisibility(View.GONE);
                   // card_details_layout.setVisibility(View.VISIBLE);
                    card_details_layout.setVisibility(View.GONE);

                    cod_txt.setTextColor(getResources().getColor(R.color.textColor));
                    total_wallet.setTextColor(getResources().getColor(R.color.textColor));
                    txt_pay_cash.setTextColor(getResources().getColor(R.color.textColor));
                    card_txt.setTextColor(getResources().getColor(R.color.colorPrimary));


                } else {
                    checkedLayout = false;
                    card_radio_btn.setVisibility(View.VISIBLE);
                    card_radio_btn1.setVisibility(View.GONE);
                    card_txt.setTextColor(getResources().getColor(R.color.colorBlack));
                    card_layout.setBackgroundColor(getResources().getColor(R.color.backgroundColor));
                    card_img.setImageResource(R.drawable.unselected_card_img);
                    cards_layout_txt.setVisibility(View.GONE);
                    itemPicker.setVisibility(View.GONE);
                    //strPaymentType = "cash";
                }

                break;

            case R.id.cod_layout:
                checkedCashLayout = false;
                checkedLayout = true;
                if (checkedLayout) {
                    cash_layout.setBackgroundColor(getResources().getColor(R.color.backgroundColor));
                    card_layout.setBackgroundColor(getResources().getColor(R.color.backgroundColor));
                    cod_layout.setBackgroundColor(getResources().getColor(R.color.light_yellow));
                    strPaymentType = "wallet";
                    money_icon.setImageResource(R.drawable.unselected_money_icon);
                    card_img.setImageResource(R.drawable.unselected_card_img);
                    cod_img.setImageResource(R.drawable.selected_cash_img);

                    unselected_radio.setVisibility(View.VISIBLE);
                    card_radio_btn1.setVisibility(View.GONE);
                    cod_radio_btn.setVisibility(View.VISIBLE);

                    card_radio_btn.setVisibility(View.VISIBLE);
                    cod_radio_btn1.setVisibility(View.GONE);
                    selected_radio.setVisibility(View.GONE);

                    cards_layout_txt.setVisibility(View.GONE);
                    itemPicker.setVisibility(View.GONE);
                    card_details_layout.setVisibility(View.GONE);

                    cod_txt.setTextColor(getResources().getColor(R.color.colorPrimary));
                    total_wallet.setTextColor(getResources().getColor(R.color.colorPrimary));
                    card_txt.setTextColor(getResources().getColor(R.color.textColor));
                    txt_pay_cash.setTextColor(getResources().getColor(R.color.textColor));
                } else {
                    checkedLayout = false;

                    card_radio_btn.setVisibility(View.VISIBLE);
                    cod_radio_btn1.setVisibility(View.GONE);
                    card_txt.setTextColor(getResources().getColor(R.color.colorBlack));
                    cod_layout.setBackgroundColor(getResources().getColor(R.color.backgroundColor));
                    cod_img.setImageResource(R.drawable.unselected_cash_img);
                    cards_layout_txt.setVisibility(View.GONE);
                    itemPicker.setVisibility(View.GONE);
                    //strPaymentType = "cash";
                }

                break;
            case R.id.txt_add_card:
                card_details_layout.setVisibility(View.GONE);
//                btn_payment.setText(R.string.save_and_pay);
                break;
            case R.id.btn_payment:

                if(strPaymentType.equalsIgnoreCase("card"))
                {

                    Intent in = new Intent(getApplicationContext(), PayTabActivity.class);
                    in.putExtra(PaymentParams.MERCHANT_EMAIL, "oshadelivery88@gmail.com"); //this a demo account for testing the sdk
                    in.putExtra(PaymentParams.SECRET_KEY,"IBEFoo3SL6rvrcCKVKYcTBBfWP6QA63iHHX661YS8pnABZkxKjJEUlTRbZHL6L4Os2Uy3SqciOMUN7syrcclx5dI5VucGy09AUHE");//Add your Secret Key Here
                    in.putExtra(PaymentParams.LANGUAGE,language);
                    in.putExtra(PaymentParams.TRANSACTION_TITLE, preferenceUtils.getStringFromPreference(PreferenceUtils.UserName,""));
                    in.putExtra(PaymentParams.AMOUNT, totalamount);

                    in.putExtra(PaymentParams.CURRENCY_CODE, "SAR");
                    in.putExtra(PaymentParams.CUSTOMER_PHONE_NUMBER, preferenceUtils.getStringFromPreference(PreferenceUtils.Mobile,""));
                    in.putExtra(PaymentParams.CUSTOMER_EMAIL, preferenceUtils.getStringFromPreference(PreferenceUtils.Email,""));
                    in.putExtra(PaymentParams.ORDER_ID,order_id);
                    in.putExtra(PaymentParams.PRODUCT_NAME, preferenceUtils.getStringFromPreference(PreferenceUtils.UserName,""));

//Billing Address
                    in.putExtra(PaymentParams.ADDRESS_BILLING, "Flat 1,Building 123, Road 2345");
                    in.putExtra(PaymentParams.CITY_BILLING, "Manama");
                    in.putExtra(PaymentParams.STATE_BILLING, "Manama");
                    in.putExtra(PaymentParams.COUNTRY_BILLING, "SAU");
                    in.putExtra(PaymentParams.POSTAL_CODE_BILLING, "00973"); //Put Country Phone code if Postal code not available '00973'

//Shipping Address
                    in.putExtra(PaymentParams.ADDRESS_SHIPPING, "Flat 1,Building 123, Road 2345");
                    in.putExtra(PaymentParams.CITY_SHIPPING, "Manama");
                    in.putExtra(PaymentParams.STATE_SHIPPING, "Manama");
                    in.putExtra(PaymentParams.COUNTRY_SHIPPING, "SAU");
                    in.putExtra(PaymentParams.POSTAL_CODE_SHIPPING, "00973"); //Put Country Phone code if Postal code not available '00973'

//Payment Page Style
                    in.putExtra(PaymentParams.PAY_BUTTON_COLOR, "#2474bc");

//Tokenization
                    in.putExtra(PaymentParams.IS_TOKENIZATION, true);
                    startActivityForResult(in, PaymentParams.PAYMENT_REQUEST_CODE);


                }else {

                    payment_process();
                }

                break;
//            case R.id.cb_saveCard:
//                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == PaymentParams.PAYMENT_REQUEST_CODE) {

            String pt_responsecode =data.getStringExtra(PaymentParams.RESPONSE_CODE);
             pt_transaction_id = data.getStringExtra(PaymentParams.TRANSACTION_ID);

            Log.e("Tag", data.getStringExtra(PaymentParams.RESPONSE_CODE));
            Log.e("Tag", data.getStringExtra(PaymentParams.TRANSACTION_ID));

            if (pt_responsecode.equals("100"))
            {
                payment_process();
            } else {
                Toast.makeText(this, "Payment not done !", Toast.LENGTH_SHORT).show();
            }


//            if (data.hasExtra(PaymentParams.TOKEN) && !data.getStringExtra(PaymentParams.TOKEN).isEmpty()) {
//                Log.e("Tag", data.getStringExtra(PaymentParams.TOKEN));
//                Log.e("Tag", data.getStringExtra(PaymentParams.CUSTOMER_EMAIL));
//                Log.e("Tag", data.getStringExtra(PaymentParams.CUSTOMER_PASSWORD));
//            }
        }
    }

    private void payment_process() {
        final API_Services service = Retrofit_fun.getClient().create(API_Services.class);
        Call<JsonElement> call_retrofit = null;
        call_retrofit = service.make_payment(Constant_keys.API_KEY, language, st_req_id, strPaymentType, st_total_amount, st_discount_amount, st_coupon_code,pt_transaction_id);
        final ProgressDialog progressDialog;
        progressDialog = new ProgressDialog(PaymentActivity.this);
        progressDialog.setCancelable(true);
        progressDialog.setTitle(R.string.loading);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();
        call_retrofit.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                progressDialog.dismiss();
                if (response.isSuccessful()) {
                    Log.e("sdkfn", response.body().toString());
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().toString());
                        int status = jsonObject.getInt("status");
                        if (status == 1)
                        {
                            showToast(jsonObject.getString("message"));
                            Intent intent = new Intent(PaymentActivity.this, PaymentSuccessfulActivity.class);
                            startActivity(intent);
                            finish();
                        }
                        else {
                            showToast(jsonObject.getString("message"));
                           /* Intent intent = new Intent(PaymentActivity.this, PaymentSuccessfulActivity.class);
                            startActivity(intent);
                            finish();*/

                            Intent intent = new Intent(PaymentActivity.this, PaymentSuccessfulActivity.class);
                            startActivity(intent);
                        }



                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }


            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                progressDialog.dismiss();
                Log.d("Error Call", ">>>>" + call.toString());
                Log.d("Error", ">>>>" + t.toString());
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
    /*
    @Override
    public void onBackPressed() {
        finish();
        Intent i = new Intent(PaymentActivity.this, InvoiceDetailsActivity.class);
        startActivity(i);
        finish();
    }*/
}
