package com.volive.osha.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.github.msarhan.ummalqura.calendar.UmmalquraCalendar;
import com.google.gson.JsonElement;
import com.volive.osha.R;
import com.volive.osha.adapters.RequestAdapter;
import com.volive.osha.helperclasses.Constant_keys;
import com.volive.osha.helperclasses.Validation_Class;
import com.volive.osha.models.My_Req_Model;
import com.volive.osha.util_classes.API_Services;
import com.volive.osha.util_classes.PreferenceUtils;
import com.volive.osha.util_classes.Retrofit_fun;


import net.alhazmy13.hijridatepicker.date.hijri.HijriDatePickerDialog;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyRequestActivity extends BaseActivity implements /*, GregorianDatePickerDialog.OnDateSetListener*/ HijriDatePickerDialog.OnDateSetListener,View.OnClickListener{
    RecyclerView recyclerView;
    ImageView filter_image, image_back;
    String activityName = "";
    ArrayList meReqarrayList;
    EditText et_from_date, et_to_date;
    boolean isfromDate, isTodate = false;
    PreferenceUtils preferenceUtils;
    Validation_Class validate;

    String language,stUserId ;
    String st_seekbarvalue,st_status_filter;
    ArrayList<My_Req_Model> my_req_models = new ArrayList<>();


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_request);
        preferenceUtils = new PreferenceUtils(MyRequestActivity.this);
        validate = new Validation_Class(MyRequestActivity.this);
        language = preferenceUtils.getStringFromPreference(PreferenceUtils.LANGUAGE,"");
        stUserId = preferenceUtils.getStringFromPreference(PreferenceUtils.USER_ID,"");
        intialzeUI();
        intializeValues();
        filter_dialog();
        adapter_attach();
        get_load_myReq();

    }




    private void adapter_attach() {

        meReqarrayList = new ArrayList();
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mLayoutManager);


    }

    private void intialzeUI() {
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        filter_image = findViewById(R.id.filter_image);
        image_back = findViewById(R.id.image_back);
    }

    private void intializeValues() {
        image_back.setOnClickListener(this);

    }

    private void filter_dialog() {

        filter_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                final Dialog dialog = new Dialog(MyRequestActivity.this, R.style.MyAlertDialogTheme);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.requestfilter_dialog);
                dialog.getWindow().setBackgroundDrawableResource(
                        R.drawable.bg_dialog);
                dialog.setCanceledOnTouchOutside(true);
                dialog.setCancelable(true);
                dialog.show();

                // dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

                ImageView back_img_details = dialog.findViewById(R.id.back_img_details);
                Button btn_apply_filter = dialog.findViewById(R.id.btn_apply_filter);
              //  final EditText ed_percentage = dialog.findViewById(R.id.ed_percentage);

                LinearLayout status_layout = dialog.findViewById(R.id.status_layout);
                final TextView filter_by_price = dialog.findViewById(R.id.filter_by_price);
                final AutoCompleteTextView status_actv = dialog.findViewById(R.id.status_actv);



                View status_view = dialog.findViewById(R.id.status_view);
                TextView txt_reset = dialog.findViewById(R.id.txt_reset);
                RelativeLayout percentage_layout=dialog.findViewById(R.id.percentage_layout);
                percentage_layout.setVisibility(View.GONE);
                et_from_date = dialog.findViewById(R.id.et_from_date);
                et_to_date = dialog.findViewById(R.id.et_to_date);

                et_from_date.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        isfromDate = true;
                        isTodate = false;
                        UmmalquraCalendar now = new UmmalquraCalendar();

                        HijriDatePickerDialog dpd = HijriDatePickerDialog.newInstance(
                                MyRequestActivity.this,
                                now.get(Calendar.YEAR),
                                now.get(Calendar.MONTH),
                                now.get(Calendar.DAY_OF_MONTH)
                        );
                        //Change the language to any of supported language
//                dpd.setLocale(new Locale("ar"));
                        dpd.show(getFragmentManager(), "Datepickerdialog");

//                Calendar now = Calendar.getInstance();
//                GregorianDatePickerDialog dpd = GregorianDatePickerDialog.newInstance(
//                        SPSignUp.this,
//                        now.get(Calendar.YEAR),
//                        now.get(Calendar.MONTH),
//                        now.get(Calendar.DAY_OF_MONTH));
//                dpd.show(getFragmentManager(), "GregorianDatePickerDialog");
                    }
                });

                et_to_date.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        isTodate = true;
                        isfromDate = false;
                        UmmalquraCalendar now = new UmmalquraCalendar();

                        HijriDatePickerDialog dpd = HijriDatePickerDialog.newInstance(
                                MyRequestActivity.this,
                                now.get(Calendar.YEAR),
                                now.get(Calendar.MONTH),
                                now.get(Calendar.DAY_OF_MONTH)
                        );
                        //Change the language to any of supported language
//                dpd.setLocale(new Locale("ar"));
                        dpd.show(getFragmentManager(), "Datepickerdialog");
//                Calendar now = Calendar.getInstance();
//                GregorianDatePickerDialog dpd = GregorianDatePickerDialog.newInstance(
//                        SPSignUp.this,
//                        now.get(Calendar.YEAR),
//                        now.get(Calendar.MONTH),
//                        now.get(Calendar.DAY_OF_MONTH));
//                dpd.show(getFragmentManager(), "GregorianDatePickerDialog");
                    }
                });

                final SeekBar rangeSeekbar8 = dialog.findViewById(R.id.seek_bar);
                rangeSeekbar8.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                        filter_by_price.setText(i + " SAR");

                        st_seekbarvalue = filter_by_price.getText().toString();
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {

                    }
                });


                txt_reset.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
//                        filter_by_price.setText("");
                        status_actv.setText("");
                        rangeSeekbar8.setProgress(0);
                        et_from_date.setText("");
                        et_to_date.setText("");
                    }
                });


                status_actv.setVisibility(View.VISIBLE);
                status_view.setVisibility(View.VISIBLE);

           final   EditText ed_percentage = dialog.findViewById(R.id.ed_percentage);

                ed_percentage.setVisibility(View.GONE);


                final ArrayAdapter<String> modelArray = new ArrayAdapter<String>(MyRequestActivity.this, android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.repairStatus_array));
                modelArray.setDropDownViewResource(android.R.layout.simple_list_item_1);
                status_actv.setAdapter(modelArray);
                status_actv.setCursorVisible(false);
                status_actv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        status_actv.showDropDown();
                        if (position == 0)
                        {
                            st_status_filter = "0";
                        }
                        else if (position == 1)
                        {
                            st_status_filter = "2";
                        }
                        else if (position == 2)
                        {
                            st_status_filter = "1";
                        }
                        else if (position == 3)
                        {
                            st_status_filter = "3";
                        }

//                warrenty = String.valueOf(position);
//                System.out.println("djfdsgv " + warrenty);
                    }
                });

                status_actv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(final View arg0) {
                        status_actv.showDropDown();
                    }
                });

                status_layout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(final View arg0) {
                        status_actv.showDropDown();
                    }
                });


                back_img_details.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                btn_apply_filter.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        String st_t_percentage,st_fromDate,st_to_date;

                       // st_t_percentage = ed_percentage.getText().toString();
                        st_fromDate =  et_from_date.getText().toString();
                        st_to_date = et_to_date.getText().toString();
                        apply_filter(st_fromDate,st_to_date);

                    }
                });
            }
        });
    }

    private void apply_filter( String st_fromDate, String st_to_date)
        {

            final API_Services service  = Retrofit_fun.getClient().create(API_Services.class);

            Call<JsonElement> call_json = null;

            call_json = service.user_requests(Constant_keys.API_KEY,language,stUserId,st_seekbarvalue,st_fromDate,st_to_date,st_status_filter);
            final ProgressDialog progressDoalog = new ProgressDialog(this, R.style.AppCompatAlertDialogStyle);
            progressDoalog.setCancelable(false);
            progressDoalog.setMessage(getResources().getString(R.string.please_wait));
            progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDoalog.show();
            call_json.enqueue(new Callback<JsonElement>() {
                @Override
                public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                    progressDoalog.dismiss();
                    Log.e("sknd",response.body().toString());
                    try
                    {
                        if (response.isSuccessful())
                        {
                            JSONObject json_object = new JSONObject(response.body().toString());
                            int status = json_object.getInt("status");
                            my_req_models = new ArrayList<>();
                            if (status == 1)
                            {
                                JSONArray json_req_array = json_object.getJSONArray("requests");

                                for (int i =0; i < json_req_array.length(); i++)
                                {
                                    JSONObject json_req = json_req_array.getJSONObject(i);
                                    My_Req_Model my_req_model = new My_Req_Model();

                                    my_req_model.setMy_req_ID(json_req.getString("request_id"));
                                    my_req_model.setMy_req_order_ID(json_req.getString("order_id"));
                                    my_req_model.setMy_req_pickup(json_req.getString("pickup_from"));
                                    my_req_model.setMy_req_drop(json_req.getString("drop_to"));
                                    my_req_model.setMy_req_passenger_num(json_req.getString("passengers_num"));
                                    my_req_model.setMy_req_car_type(json_req.getString("car_type"));
                                    my_req_model.setMy_req_request_type(json_req.getString("request_type"));
                                    my_req_model.setMy_req_lat_pic(json_req.getString("latitude_pick"));
                                    my_req_model.setMy_req_lang_pick(json_req.getString("longitude_pick"));
                                    my_req_model.setMy_req_lat_drop(json_req.getString("latitude_drop"));
                                    my_req_model.setMy_req_lang_drop(json_req.getString("longitude_drop"));
                                    my_req_model.setMy_req_status(json_req.getString("request_status"));
                                    my_req_model.setMy_req_tracking_sts(json_req.getString("tracking_status"));
                                    my_req_model.setMy_req_rating(json_req.getString("request_rating"));
                                    my_req_model.setMY_req_rating_sts(json_req.getString("request_rating_status"));
                                    my_req_model.setMy_req_date(json_req.getString("date"));
                                    my_req_models.add(my_req_model);


                                }

                            }
                            else {

                                showToast(json_object.getString("message"));

                            }
                            RequestAdapter requestAdapter = new RequestAdapter(MyRequestActivity.this,my_req_models);
                            recyclerView.setAdapter(requestAdapter);

                        }


                    }catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<JsonElement> call, Throwable t) {
                    progressDoalog.dismiss();
                    Log.e("Error" ,call.toString());
                }
            });


        }


    private void get_load_myReq() {

        final API_Services service  = Retrofit_fun.getClient().create(API_Services.class);

        Call<JsonElement> call_json = null;
        call_json = service.user_requests(Constant_keys.API_KEY,language,stUserId,"","","","");
        final ProgressDialog progressDoalog = new ProgressDialog(this, R.style.AppCompatAlertDialogStyle);
        progressDoalog.setCancelable(false);
        progressDoalog.setMessage(getResources().getString(R.string.please_wait));
        progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDoalog.show();
        call_json.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                progressDoalog.dismiss();
                Log.e("sknd",response.body().toString());
               try
               {
                   if (response.isSuccessful())
                   {
                       JSONObject json_object = new JSONObject(response.body().toString());
                       int status = json_object.getInt("status");
                       my_req_models = new ArrayList<>();
                       if (status == 1)
                       {
                           JSONArray json_req_array = json_object.getJSONArray("requests");

                           for (int i =0; i < json_req_array.length(); i++)
                           {
                               JSONObject json_req = json_req_array.getJSONObject(i);
                               My_Req_Model my_req_model = new My_Req_Model();

                               my_req_model.setMy_req_ID(json_req.getString("request_id"));
                               my_req_model.setMy_req_order_ID(json_req.getString("order_id"));
                               my_req_model.setMy_req_pickup(json_req.getString("pickup_from"));
                               my_req_model.setMy_req_drop(json_req.getString("drop_to"));
                               my_req_model.setMy_req_passenger_num(json_req.getString("passengers_num"));
                               my_req_model.setMy_req_car_type(json_req.getString("car_type"));
                               my_req_model.setMy_req_request_type(json_req.getString("request_type"));
                               my_req_model.setMy_req_lat_pic(json_req.getString("latitude_pick"));
                               my_req_model.setMy_req_lang_pick(json_req.getString("longitude_pick"));
                               my_req_model.setMy_req_lat_drop(json_req.getString("latitude_drop"));
                               my_req_model.setMy_req_lang_drop(json_req.getString("longitude_drop"));
                               my_req_model.setMy_req_status(json_req.getString("request_status"));
                               my_req_model.setMy_req_tracking_sts(json_req.getString("tracking_status"));
                               my_req_model.setMy_req_rating(json_req.getString("request_rating"));
                               my_req_model.setMY_req_rating_sts(json_req.getString("request_rating_status"));
                               my_req_model.setMy_req_date(json_req.getString("date"));
                               my_req_model.setMy_re_payment_status(json_req.getString("payment_status"));


                               my_req_models.add(my_req_model);


                           }

                       }
                       else {

                           showToast(json_object.getString("message"));

                       }
                        RequestAdapter requestAdapter = new RequestAdapter(MyRequestActivity.this,my_req_models);
                       recyclerView.setAdapter(requestAdapter);

                   }


               }catch (Exception e)
               {
                   e.printStackTrace();
               }


            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {

            }
        });


    }






    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent(MyRequestActivity.this, User_Home_activity.class);
        startActivity(i);
        finish();
    }

    @Override
    public void onDateSet(HijriDatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        if (isfromDate) {
            String date = "" + dayOfMonth + "/" + (++monthOfYear) + "/" + year;
            et_from_date.setText(date);
        } else if (isTodate) {
            String date = "" + dayOfMonth + "/" + (++monthOfYear) + "/" + year;
            et_to_date.setText(date);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.image_back:
                Intent i = new Intent(MyRequestActivity.this, User_Home_activity.class);
                startActivity(i);
                finish();
                break;
        }
    }
}
