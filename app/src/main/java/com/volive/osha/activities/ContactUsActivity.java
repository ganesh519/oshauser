package com.volive.osha.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.volive.osha.R;


public class ContactUsActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView back_img_details;
    Button submit_details;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);

        initilizeViews();
        initilizeValues();
    }

    private void initilizeValues() {
        submit_details.setOnClickListener(this);
        back_img_details.setOnClickListener(this);
    }

    private void initilizeViews() {
        back_img_details=findViewById(R.id.back_img_details);
        submit_details=findViewById(R.id.submit_details);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.back_img_details:

                callintent();

                break;

            case R.id.submit_details:

                Toast.makeText(this, "Thanks For Your Feedback.", Toast.LENGTH_SHORT).show();

                callintent();
                break;

        }
    }

    private void callintent()
    {
        Intent intent=new Intent(ContactUsActivity.this, User_Home_activity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
