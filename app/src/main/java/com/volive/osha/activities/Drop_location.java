package com.volive.osha.activities;



import android.Manifest;
        import android.app.FragmentTransaction;
        import android.content.Intent;
        import android.content.pm.PackageManager;
        import android.location.Address;
        import android.location.Geocoder;
        import android.location.Location;
        import android.location.LocationManager;
        import android.os.Bundle;
        import android.support.annotation.Nullable;
        import android.support.v4.app.ActivityCompat;
        import android.support.v4.app.FragmentManager;
        import android.support.v7.app.AppCompatActivity;
        import android.util.Log;
        import android.view.MotionEvent;
        import android.view.View;
        import android.widget.Button;
        import android.widget.ImageView;
        import android.widget.RelativeLayout;
        import android.widget.TextView;
        import android.widget.Toast;

        import com.google.android.gms.common.api.GoogleApiClient;
        import com.google.android.gms.common.api.Status;
        import com.google.android.gms.location.LocationServices;
        import com.google.android.gms.maps.CameraUpdateFactory;
        import com.google.android.gms.maps.GoogleMap;
        import com.google.android.gms.maps.OnMapReadyCallback;
        import com.google.android.gms.maps.SupportMapFragment;
        import com.google.android.gms.maps.model.CameraPosition;
        import com.google.android.gms.maps.model.LatLng;
        import com.google.android.gms.maps.model.MarkerOptions;
        import com.google.android.libraries.places.api.model.Place;
        import com.google.android.libraries.places.widget.Autocomplete;
        import com.google.android.libraries.places.widget.AutocompleteActivity;
        import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
        import com.volive.osha.Fragments.HomeFragment;
        import com.volive.osha.R;
        import com.volive.osha.helperclasses.GPSTracker;
        import com.volive.osha.helperclasses.MyApplication;
        import com.volive.osha.util_classes.PreferenceUtils;

        import java.io.IOException;
        import java.util.Arrays;
        import java.util.List;
        import java.util.Locale;




public class Drop_location extends AppCompatActivity {
/*
    ImageView image_back, map_image, img_curent_loc;
    public  static TextView txt_address;
    Button bt_confirm_loc;
    GoogleMap googlemap;
    FragmentManager fragmentManager;
    SupportMapFragment mapFragment;
    RelativeLayout rel_main;
    GPSTracker gpsTracker;
    private GoogleApiClient googleApiClient;
    public static double Flc_longitude, Flc_latitude;
    LocationManager locationManager;
    int AUTOCOMPLETE_REQUEST_CODE = 1;
    private static final String TAG = "Location";
    String action_map,language;
    PreferenceUtils preferenceUtils;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.expand_map_view);
        preferenceUtils = new PreferenceUtils(Drop_location.this);
        language = preferenceUtils.getStringFromPreference(PreferenceUtils.LANGUAGE,"");
        intializeUI();
        intializeValues();
        Intent i = getIntent();
        if (i != null) {
            action_map =  i.getStringExtra("action_map");
            Log.e("fgwh",action_map);
        }

    }

    private void intializeUI() {

        gpsTracker = new GPSTracker(Drop_location.this);
        fragmentManager = getSupportFragmentManager();

        mapFragment = (SupportMapFragment) fragmentManager.findFragmentById(R.id.map);
        mapFragment.getMapAsync(Drop_location.this);

        rel_main = findViewById(R.id.rel_main);
        image_back = findViewById(R.id.image_back);
        map_image = findViewById(R.id.map_image);
        txt_address = findViewById(R.id.txt_address);
        bt_confirm_loc = findViewById(R.id.bt_confirm_loc);
        img_curent_loc = findViewById(R.id.img_curent_loc);

        // Intent i = getIntent();
        // txt_address.setText(strAddress);
    }


    private void intializeValues() {
        image_back.setOnClickListener(this);
        bt_confirm_loc.setOnClickListener(this);
        rel_main.setOnClickListener(this);
        map_image.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                int pos_action = motionEvent.getAction();
                switch (pos_action) {
                    case MotionEvent.ACTION_DOWN:

                        rel_main.requestDisallowInterceptTouchEvent(true);
                        return false;
                    case MotionEvent.ACTION_UP:

                        rel_main.requestDisallowInterceptTouchEvent(false);
                        return true;
                    case MotionEvent.ACTION_MOVE:

                        rel_main.requestDisallowInterceptTouchEvent(true);
                        return false;
                    default:

                        return true;
                }
            }
        });

        img_curent_loc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    LatLng latLng = new LatLng(gpsTracker.getLatitude(), gpsTracker.getLongitude());
                    googlemap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16));
                    Flc_latitude = gpsTracker.getLatitude();
                    Flc_longitude= gpsTracker.getLongitude();
                    getCompleteAddressString(Flc_latitude, Flc_longitude);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

        final List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG);
        txt_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Autocomplete.IntentBuilder(
                        AutocompleteActivityMode.FULLSCREEN, fields)
                        .build(Drop_location.this);
                startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE);
            }
        });

    }
    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.image_back:
                finish();
                break;
            case R.id.bt_confirm_loc:
                if (action_map.equalsIgnoreCase("1"))
                {

                    Intent intent = new Intent(User_location_point.this,User_Home_activity.class);
                    intent.putExtra("Action","pick");
                    startActivity(intent);
                }
                else if (action_map.equals("2"))
                {

                    Intent intent = new Intent(Drop_location.this,User_Home_activity.class);
                    intent.putExtra("Action","drop");
                    startActivity(intent);
                }
                break;


        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                Log.i(TAG, "Place: " + place.getName() + ", " + place.getId());

                HomeFragment.loc_addresss = place.getAddress().toString();
                HomeFragment.strlatitide = place.getLatLng().latitude;
                HomeFragment.strlongitude = place.getLatLng().longitude;
                googlemap.clear();
                LatLng sydney = new LatLng(place.getLatLng().latitude, place.getLatLng().longitude);
                Log.e("LatLopng", String.valueOf(sydney));
//                mMap.addMarker(new MarkerOptions().position(sydney).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_pin))).showInfoWindow();
                googlemap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, 16));
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
                Log.i(TAG, status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }





    @Override
    public void onMapReady(GoogleMap googleMap) {
        googlemap = googleMap;
        LatLng sydney;
        if (ActivityCompat.checkSelfPermission(User_location_point.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(User_location_point.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        googlemap.setMyLocationEnabled(false);
        googlemap.getUiSettings().setZoomControlsEnabled(false);
        googlemap.getUiSettings().setZoomGesturesEnabled(true);
        googlemap.getUiSettings().setCompassEnabled(false);
        googlemap.getUiSettings().setRotateGesturesEnabled(true);

        try {

            LatLng latLng = new LatLng(gpsTracker.getLatitude(), gpsTracker.getLongitude());
            googlemap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16));
            getCompleteAddressString(gpsTracker.getLatitude(), gpsTracker.getLongitude());

           *//* if (action_map.equals("1")){
                LatLng latLng = new LatLng(HomeFragment.double_lat, HomeFragment.double_lang);
                googlemap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16));
                getCompleteAddressString(HomeFragment.double_lat, HomeFragment.double_lang);

            }


            else if (action_map.equals("2")){
                LatLng latLng = new LatLng(HomeFragment.double_lat, HomeFragment.double_lang);
                googlemap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16));
                getCompleteAddressString(HomeFragment.double_lat, HomeFragment.double_lang);

            }
*//*

        } catch (Exception e) {
            e.printStackTrace();
        }
        // gMap.setMyLocationEnabled(true);
        //  gMap.getUiSettings().setMyLocationButtonEnabled(true);
        googlemap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {
                Log.e("onCameraChange: ", "" + cameraPosition.target.latitude + " : " + cameraPosition.target.longitude);
                Flc_latitude = cameraPosition.target.latitude;
                Flc_longitude = cameraPosition.target.longitude;
                getCompleteAddressString(cameraPosition.target.latitude, cameraPosition.target.longitude);
            }
        });

    }



    public String getCompleteAddressString(Double LATITUDE, Double LONGITUDE) {
        String strAdd = "";
        txt_address.setText("");
        Geocoder geocoder = new Geocoder(User_location_point.this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");

                for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();
                txt_address.setText(strAdd);
                if (strAdd.trim().isEmpty()) {
                    String address = addresses.get(0).getAddressLine(0);
                    strAdd = address;
                    txt_address.setText(strAdd);
                }
                Log.d("Current location add", "" + strReturnedAddress.toString());
            } else {
                Log.d("My Current location add", "No Address returned!");
            }
        } catch (Exception e) {
            Log.e("getCompleteAddressSt: ", e.getMessage());
        }
        return strAdd;
    }

    //Getting current location
    private void getCurrentLocation() {
        googlemap.clear();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Location location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
        if (location != null) {
            //Getting longitude and latitude
            Flc_longitude = location.getLongitude();
            Flc_latitude = location.getLatitude();

            //moving the map to location
            //getMyLocation();
        }
    }
    private void moveMap() {
        *//**
         * Creating the latlng object to store lat, long coordinates
         * adding marker to map
         * move the camera with animation
         *//*
        LatLng latLng = new LatLng(Flc_latitude, Flc_longitude);
        googlemap.addMarker(new MarkerOptions()
                .position(latLng)
                .draggable(true)
                .title("Marker in point"));

        googlemap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        googlemap.animateCamera(CameraUpdateFactory.zoomTo(15));
        googlemap.getUiSettings().setZoomControlsEnabled(true);


    }
    public void getMyLocation() {
        // create class object
        gpsTracker = new GPSTracker(User_location_point.this);
        // check if GPS enabled
        if (gpsTracker.canGetLocation()) {
            Flc_latitude = gpsTracker.getLatitude();
            Flc_longitude = gpsTracker.getLongitude();
            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(this, Locale.getDefault());
            try {
                addresses = geocoder.getFromLocation(Flc_latitude, Flc_longitude, 1);
                txt_address .setText( addresses.get(0).getAddressLine(0));
               *//* postalCode = addresses.get(0).getPostalCode();
                city = addresses.get(0).getLocality();
                address = addresses.get(0).getAddressLine(0);
                state = addresses.get(0).getAdminArea();
                country = addresses.get(0).getCountryName();
                knownName = addresses.get(0).getFeatureName();
                Log.e("Location",postalCode+" "+city+" "+address+" "+state+" "+knownName);*//*

            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            gpsTracker.showSettingsAlert();
        }
    }
    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    public void onResume() {
        super.onResume();
//        mapFragment.onResume();
        MyApplication.isActivityVisible();
        if (googlemap != null) {
            googlemap.clear();
            mapFragment.onStart();
            // add the markers just like how you did the first time
        }
    }*/
}






























