package com.volive.osha.activities;

import android.app.Dialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.volive.osha.R;
import com.volive.osha.adapters.ReqSpAdapter;
import com.volive.osha.models.ReqSPModel_list;

import java.util.ArrayList;

public class DetailsRequest extends AppCompatActivity {
    LinearLayout linear_details, del_layout, desc_layout;
    ImageView image_up, image_down, back_img_details;
    RecyclerView providers_Rview;
    LinearLayoutManager layoutManager;
    ArrayList<ReqSPModel_list> spModelArrayList = new ArrayList<>();
    ReqSpAdapter reqSpAdapter;
    String acionName = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_request);

        //additionalPriceDialog();

        back_img_details = findViewById(R.id.back_img_details);
        back_img_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        //RecyclerView
        providers_Rview = findViewById(R.id.providers_rview);
        layoutManager = new LinearLayoutManager(DetailsRequest.this, LinearLayoutManager.VERTICAL, false);
        providers_Rview.setLayoutManager(layoutManager);
//        providers_Rview.setNestedScrollingEnabled(false);


        linear_details = (LinearLayout) findViewById(R.id.sub_layout);

//        image_up = (ImageView) findViewById(R.id.image_up);
//        image_down = findViewById(R.id.image_down);

//        image_up.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                linear_details.setVisibility(View.VISIBLE);
//                image_up.setVisibility(View.GONE);
//                image_down.setVisibility(View.VISIBLE);
//
//
//            }
//        });
//
//        image_down.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                linear_details.setVisibility(View.GONE);
//                image_down.setVisibility(View.GONE);
//                image_up.setVisibility(View.VISIBLE);
//            }
//        });

        loadData();

        reqSpAdapter = new ReqSpAdapter(DetailsRequest.this, spModelArrayList);
        providers_Rview.setAdapter(reqSpAdapter);
    }

    private void additionalPriceDialog() {
        final Dialog dialog = new Dialog(DetailsRequest.this, R.style.MyAlertDialogTheme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.price_added_dialog);
        dialog.getWindow().setBackgroundDrawableResource(
                R.drawable.bg_dialog);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(true);
        dialog.show();

//        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        ImageView cancel_image = dialog.findViewById(R.id.cancel_image);
        Button agree_btn = dialog.findViewById(R.id.agree_btn);
        agree_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        cancel_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                finish();
            }
        });
    }




    public void loadData() {
        int[] header = {R.string.f_q, R.string.ggg, R.string.kkkk, R.string.iiiii};
        int[] desc = {R.string.soundhi, R.string.soundhi, R.string.soundhi, R.string.soundhi};
        int[] imageId = {R.drawable.provider_one, R.drawable.provider_two, R.drawable.provider_three, R.drawable.provider_four};
        int[] currency = {R.string.b, R.string.c, R.string.d, R.string.a};

        for (int i = 0; i < imageId.length; i++) {
            ReqSPModel_list item = new ReqSPModel_list();
            item.setImage(imageId[i]);
            item.setName(getResources().getString(header[i]));
            item.setLocation(getResources().getString(desc[i]));
            item.setCurrency(getResources().getString(currency[i]));

            spModelArrayList.add(item);

        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
