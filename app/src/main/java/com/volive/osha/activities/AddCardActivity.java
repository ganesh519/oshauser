package com.volive.osha.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.volive.osha.R;


public class AddCardActivity extends AppCompatActivity implements View.OnClickListener {
    TextView txt_saveCard, toolbar_title;
    ImageView back_img_details;
    EditText et_card_holder_name, et_card_number, et_card_expiry, et_card_cvv;
    Button btn_payment;
    Spinner spinner;
    CheckBox checkBox;
    RelativeLayout cards_layout;
    String activityName = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_card);
        initializeUI();
        initializeValues();
    }

    private void initializeValues() {

        back_img_details.setOnClickListener(this);
        btn_payment.setOnClickListener(this);


        final ArrayAdapter<String> cardsArray = new ArrayAdapter<String>(AddCardActivity.this, android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.cards_array));
        cardsArray.setDropDownViewResource(android.R.layout.simple_list_item_1);
        spinner.setAdapter(cardsArray);


        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void initializeUI() {
        //TextView
        toolbar_title = findViewById(R.id.toolbar_title);
        txt_saveCard = findViewById(R.id.txt_saveCard);

        //EditText
        et_card_holder_name = findViewById(R.id.et_card_holder_name);
        et_card_number = findViewById(R.id.et_card_number);
        et_card_expiry = findViewById(R.id.et_card_expiry);
        et_card_cvv = findViewById(R.id.et_card_cvv);

        //Button
        btn_payment = findViewById(R.id.btn_payment);

        //ImageView
        back_img_details = findViewById(R.id.back_img_details);

//        //CheckBox
        checkBox = findViewById(R.id.checkBox);


        //Spinner
        spinner = findViewById(R.id.spinner);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back_img_details:
                finish();
                break;
            case R.id.btn_payment:
                Intent intent = new Intent(AddCardActivity.this, UserProfileActivity.class);
                startActivity(intent);
                finish();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
