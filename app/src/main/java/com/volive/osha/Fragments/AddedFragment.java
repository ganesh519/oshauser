package com.volive.osha.Fragments;


import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonElement;
import com.paytabs.paytabs_sdk.payment.ui.activities.PayTabActivity;
import com.paytabs.paytabs_sdk.utils.PaymentParams;
import com.volive.osha.R;
import com.volive.osha.activities.User_Home_activity;
import com.volive.osha.adapters.WalletAdapter;
import com.volive.osha.helperclasses.Constant_keys;
import com.volive.osha.helperclasses.Validation_Class;
import com.volive.osha.models.WalletModels;
import com.volive.osha.util_classes.API_Services;
import com.volive.osha.util_classes.PreferenceUtils;
import com.volive.osha.util_classes.Retrofit_fun;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;


/**
 * A simple {@link Fragment} subclass.
 */
public class AddedFragment extends Fragment implements View.OnClickListener{

    ArrayList<WalletModels> arrayList;
    RecyclerView mRecyclerView;
    View view = null;
    TextView txt_wallet_amount,txt_add_money,txt_month_date;
    PreferenceUtils preferenceUtils;
    Validation_Class helperClass;
    String language,stUserId;
    ArrayList<WalletModels> wallet_array_list = new ArrayList<>();
    WalletAdapter walletAdapter;


    String st_add_money_amount ="" ,pt_transaction_id="";
    int  money_add = 0 ;

    double totalamount;

    public AddedFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_added, container, false);
        preferenceUtils = new PreferenceUtils(getActivity());
        helperClass = new Validation_Class(getActivity());
        language = preferenceUtils.getStringFromPreference(PreferenceUtils.LANGUAGE,"");
        stUserId  = preferenceUtils.getStringFromPreference(PreferenceUtils.USER_ID,"");
        // arrayList=new ArrayList<>();
       // intializeUI();
        mRecyclerView =view.findViewById(R.id.wallet_recycler);
        txt_wallet_amount = view.findViewById(R.id.txt_wallet_amount);
        txt_add_money = view.findViewById(R.id.txt_add_money);
        txt_month_date = view.findViewById(R.id.txt_month_date);
        intilaizeValues();
        // loadData();
        adapter_attach();
        wallet_service();

        return view;

    }
    private void intializeUI()
    {
        mRecyclerView =view.findViewById(R.id.wallet_recycler);
        txt_wallet_amount = view.findViewById(R.id.txt_wallet_amount);
        txt_add_money = view.findViewById(R.id.txt_add_money);
    }

    private void intilaizeValues() {

        txt_month_date.setText(getString(R.string.money_add));
        txt_add_money.setOnClickListener(this);
    }
    private void adapter_attach() {

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(mLayoutManager);


    }




    private void wallet_service()
    {
        final API_Services services  = Retrofit_fun.getClient().create(API_Services.class);
        Call<JsonElement> call_notification= null;
        //  call_notification = services.wallet_summary(Constant_keys.API_KEY,language,preferenceUtils.getStringFromPreference(PreferenceUtils.USER_ID,""),"1");
        call_notification = services.wallet_summary(Constant_keys.API_KEY,language,stUserId,"3");
        final ProgressDialog progressDoalog = new ProgressDialog(getActivity(), R.style.AppCompatAlertDialogStyle);
        progressDoalog.setCancelable(false);
        progressDoalog.setMessage("Please Wait....");
        progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDoalog.show();

        call_notification.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                progressDoalog.dismiss();
                if (response.isSuccessful())
                {
                    Log.e("sjwlb",response.body().toString());
                    try {
                        JSONObject json_obejct =  new JSONObject(response.body().toString());
                        int status = json_obejct.getInt("status");
                        //String   message = json_obejct.getString("message");

                        wallet_array_list = new ArrayList<>();
                        if (status == 1)
                        {
                            String wallet_amount = json_obejct.getString("wallet_amount");

                            txt_wallet_amount.setText(wallet_amount + " " + "SAR");

                            JSONArray json_array =json_obejct.getJSONArray("wallet_summary");

                            for (int i=0; i< json_array.length();i++)
                            {
                                JSONObject json_payment = json_array.getJSONObject(i);
                                WalletModels walletModel = new WalletModels();
                                walletModel.setWallet_user_id(json_payment.getString("user_id"));
                                walletModel.setWallet_amount(json_payment.getString("amount"));
                                walletModel.setWallet_date(json_payment.getString("created_at"));
                                walletModel.setWallet_order_id(json_payment.getString("order_id"));
                                walletModel.setWallet_type(json_payment.getString("type"));
                                wallet_array_list.add(walletModel);

                            }
                        }
                        else
                        {
                            //showToast(message);
                            Toast.makeText(getActivity(),json_obejct.getString("message"),Toast.LENGTH_SHORT).show();
                        }
                        walletAdapter = new WalletAdapter(getActivity(), wallet_array_list);
                        mRecyclerView.setAdapter(walletAdapter);
                    } catch (Exception e) {
                        progressDoalog.dismiss();
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                progressDoalog.dismiss();
                Log.e("Erro:" ,call.toString());
                Log.e("Error:",t.toString());

            }
        });

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.txt_add_money:
                add_money_dialog();
                break;
        }


    }

    private void add_money_dialog() {

        final Dialog dialog = new Dialog(getActivity(), R.style.MyAlertDialogTheme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.add_money_dialog);
        dialog.getWindow().setBackgroundDrawableResource(
                R.drawable.bg_dialog);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);
        dialog.show();
//        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        final EditText edit_amount = dialog.findViewById(R.id.edit_amount);
        Button bt_close = dialog.findViewById(R.id.bt_close);
        Button bt_submit = dialog.findViewById(R.id.bt_submit);
        bt_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                st_add_money_amount = edit_amount.getText().toString();
                totalamount= Double.parseDouble(st_add_money_amount);
                if (st_add_money_amount.isEmpty()) {
                    Toast.makeText(getActivity(), getString(R.string.please_enter_amount), Toast.LENGTH_SHORT).show();
                } else {
                    money_add = Integer.parseInt(st_add_money_amount);
                    money_cheking();
                }

                //and amount

            }
        });
        bt_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });

    }

    private void money_cheking() {

        if (money_add < 50) {
            Toast.makeText(getActivity(), getString(R.string.mini_amount), Toast.LENGTH_SHORT).show();
        } else if (money_add > 1000) {
            Toast.makeText(getActivity(), getString(R.string.max_amount), Toast.LENGTH_SHORT).show();
        } else {

            Random random = new Random();
            String random_number = String.format("%04d", random.nextInt(10000));
            Log.d("MyApp", "Generated Password : " + random_number);

            pay_tab(random_number);
            //   Toast.makeText(getActivity(),getString(R.string.submit),Toast.LENGTH_SHORT).show();


        }

    }

    private void pay_tab(String random_number) {
        Intent in = new Intent(getActivity(), PayTabActivity.class);
        in.putExtra(PaymentParams.MERCHANT_EMAIL, "oshadelivery88@gmail.com"); //this a demo account for testing the sdk
        in.putExtra(PaymentParams.SECRET_KEY, "IBEFoo3SL6rvrcCKVKYcTBBfWP6QA63iHHX661YS8pnABZkxKjJEUlTRbZHL6L4Os2Uy3SqciOMUN7syrcclx5dI5VucGy09AUHE");//Add your Secret Key Here
        in.putExtra(PaymentParams.LANGUAGE, language);
        in.putExtra(PaymentParams.TRANSACTION_TITLE, preferenceUtils.getStringFromPreference(PreferenceUtils.UserName, ""));
        in.putExtra(PaymentParams.AMOUNT, totalamount);

        in.putExtra(PaymentParams.CURRENCY_CODE, "SAR");
        in.putExtra(PaymentParams.CUSTOMER_PHONE_NUMBER, preferenceUtils.getStringFromPreference(PreferenceUtils.Mobile, ""));
        in.putExtra(PaymentParams.CUSTOMER_EMAIL, preferenceUtils.getStringFromPreference(PreferenceUtils.Email, ""));
        in.putExtra(PaymentParams.ORDER_ID, random_number);
        in.putExtra(PaymentParams.PRODUCT_NAME, preferenceUtils.getStringFromPreference(PreferenceUtils.UserName, ""));
//Billing Address
        in.putExtra(PaymentParams.ADDRESS_BILLING, "Flat 1,Building 123, Road 2345");
        in.putExtra(PaymentParams.CITY_BILLING, "Manama");
        in.putExtra(PaymentParams.STATE_BILLING, "Manama");
        in.putExtra(PaymentParams.COUNTRY_BILLING, "SAU");
        in.putExtra(PaymentParams.POSTAL_CODE_BILLING, "00973"); //Put Country Phone code if Postal code not available '00973'

//Shipping Address
        in.putExtra(PaymentParams.ADDRESS_SHIPPING, "Flat 1,Building 123, Road 2345");
        in.putExtra(PaymentParams.CITY_SHIPPING, "Manama");
        in.putExtra(PaymentParams.STATE_SHIPPING, "Manama");
        in.putExtra(PaymentParams.COUNTRY_SHIPPING, "SAU");
        in.putExtra(PaymentParams.POSTAL_CODE_SHIPPING, "00973"); //Put Country Phone code if Postal code not available '00973'

//Payment Page Style
        in.putExtra(PaymentParams.PAY_BUTTON_COLOR, "#2474bc");

//Tokenization
        in.putExtra(PaymentParams.IS_TOKENIZATION, true);
        startActivityForResult(in, PaymentParams.PAYMENT_REQUEST_CODE);

    }

    private void add_money_service() {
        final API_Services services = Retrofit_fun.getClient().create(API_Services.class);
        Call<JsonElement> call_notification = null;
        //  call_notification = services.wallet_summary(Constant_keys.API_KEY,language,preferenceUtils.getStringFromPreference(PreferenceUtils.USER_ID,""),"1");
        call_notification = services.add_money(Constant_keys.API_KEY, language, stUserId, st_add_money_amount, pt_transaction_id);
        final ProgressDialog progressDoalog = new ProgressDialog(getActivity(), R.style.AppCompatAlertDialogStyle);
        progressDoalog.setCancelable(false);
        progressDoalog.setMessage("Please Wait....");
        progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDoalog.show();

        call_notification.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                progressDoalog.dismiss();
                if (response.isSuccessful()) {
                    Log.e("sjwlb", response.body().toString());
                    try {
                        JSONObject json_obejct = new JSONObject(response.body().toString());
                        int status = json_obejct.getInt("status");
                        //String   message = json_obejct.getString("message");
                        wallet_array_list = new ArrayList<>();
                        if (status == 1) {
                            Toast.makeText(getActivity(), json_obejct.getString("message"), Toast.LENGTH_SHORT).show();
                            Intent a = new Intent(getActivity(), User_Home_activity.class);
                            a.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(a);
                            getActivity().finish();

                        } else {
                            //showToast(message);
                            Toast.makeText(getActivity(), json_obejct.getString("message"), Toast.LENGTH_SHORT).show();
                        }

                    } catch (Exception e) {
                        progressDoalog.dismiss();
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                progressDoalog.dismiss();
                Log.e("Erro:", call.toString());
                Log.e("Error:", t.toString());

            }
        });

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == PaymentParams.PAYMENT_REQUEST_CODE) {

            String pt_responsecode = data.getStringExtra(PaymentParams.RESPONSE_CODE);
            pt_transaction_id = data.getStringExtra(PaymentParams.TRANSACTION_ID);
            Log.e("Tag", data.getStringExtra(PaymentParams.RESPONSE_CODE));
            Log.e("Tag", data.getStringExtra(PaymentParams.TRANSACTION_ID));

            if (pt_responsecode.equals("100")) {
                add_money_service();
            } else {
                Toast.makeText(getActivity(), "Payment not done !", Toast.LENGTH_SHORT).show();
            }


//            if (data.hasExtra(PaymentParams.TOKEN) && !data.getStringExtra(PaymentParams.TOKEN).isEmpty()) {
//                Log.e("Tag", data.getStringExtra(PaymentParams.TOKEN));
//                Log.e("Tag", data.getStringExtra(PaymentParams.CUSTOMER_EMAIL));
//                Log.e("Tag", data.getStringExtra(PaymentParams.CUSTOMER_PASSWORD));
//            }
        }
    }

}