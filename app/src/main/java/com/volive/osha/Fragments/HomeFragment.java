package com.volive.osha.Fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonElement;
import com.volive.osha.R;
import com.volive.osha.activities.LoginActivty;
import com.volive.osha.activities.RequestDetailsActivity;
import com.volive.osha.activities.User_Home_activity;
import com.volive.osha.activities.User_location_point;
import com.volive.osha.adapters.ReqSpAdapter;
import com.volive.osha.helperclasses.Constant_keys;
import com.volive.osha.helperclasses.GPSTracker;
import com.volive.osha.helperclasses.HelperClass;
import com.volive.osha.helperclasses.Validation_Class;
import com.volive.osha.models.ReqSPModel_list;
import com.volive.osha.util_classes.API_Services;
import com.volive.osha.util_classes.PreferenceUtils;
import com.volive.osha.util_classes.Retrofit_fun;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.volive.osha.activities.User_Home_activity.rel_notification;


public class HomeFragment extends Fragment implements View.OnClickListener/*, OnMapReadyCallback */
{

    private static final String TAG = "";
    public static double double_lat, double_lang;
    public static double strlatitide, strlongitude;
    public static String loc_addresss = "";
    View rootView;
    Button btn_send_request, btn_send_details;
    HelperClass helperClass;
    GPSTracker gpsTracker;
    ImageView img_pickup_loc, img_drop_loc;
    TextView pass_del, items_delivery, no_of_passengers, type_of_car, del_items, desc, pickup_location, drop_location;
    EditText ed_delvery_items,ed_num_passengers,ed_description,ed_type_car,ed_add_comment;
    LinearLayout sub_items_del_layout, pass_type;
    PreferenceUtils preferenceUtils;
    Validation_Class validate;
    String stUserId, langauge,st_pickLocation = "0",st_dropLocation = "0",st_pick_latitude,st_pick_longitude,st_drop_latitude,st_comment,st_drop_longitude;
    LinearLayout ll_num_pasnger,ll_del_items,ll_desc,ll_type_car;

    String pending_request = "",pending_request_type = "";

    ConnectivityManager connMgr ;

    NetworkInfo networkInfo ;

    public HomeFragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.frag_home, container, false);
        preferenceUtils = new PreferenceUtils(getActivity());
        helperClass = new HelperClass(getActivity());
        gpsTracker = new GPSTracker(getActivity());
        validate = new Validation_Class(getActivity());
        stUserId = preferenceUtils.getStringFromPreference(PreferenceUtils.USER_ID, "");
        langauge = preferenceUtils.getStringFromPreference(PreferenceUtils.LANGUAGE, "");
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        initializeValues(rootView);
        initializeClicking();

        connMgr = (ConnectivityManager) getActivity()
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        networkInfo = connMgr.getActiveNetworkInfo();

        Log.e("kdnsf",stUserId);
        //user_check_pending(stUserId);
        return rootView;

    }




    private void initializeClicking() {
        rel_notification.setVisibility(View.VISIBLE);
        pass_del.setOnClickListener(this);
        items_delivery.setOnClickListener(this);
        btn_send_request.setOnClickListener(this);
        btn_send_details.setOnClickListener(this);
        img_drop_loc.setOnClickListener(this);
        img_pickup_loc.setOnClickListener(this);
        ll_desc.setOnClickListener(this);
        ll_num_pasnger.setOnClickListener(this);
        ll_del_items.setOnClickListener(this);
        ll_type_car.setOnClickListener(this);


    }

    private void initializeValues(View rootView) {

        pass_del = rootView.findViewById(R.id.pass_del);
        items_delivery = rootView.findViewById(R.id.items_delivery);
        sub_items_del_layout = rootView.findViewById(R.id.sub_items_del_layout);
        btn_send_request = rootView.findViewById(R.id.btn_send_request);
        no_of_passengers = rootView.findViewById(R.id.no_of_passengers);
        type_of_car = rootView.findViewById(R.id.type_of_car);
        del_items = rootView.findViewById(R.id.del_items);
        desc = rootView.findViewById(R.id.description);
        btn_send_details = rootView.findViewById(R.id.btn_send_request_details);
        img_drop_loc = rootView.findViewById(R.id.img_drop_loc);
        img_pickup_loc = rootView.findViewById(R.id.img_pickup_loc);

        pickup_location = rootView.findViewById(R.id.pickup_location);
        drop_location = rootView.findViewById(R.id.drop_location);

        pass_del.setTextColor(getResources().getColor(R.color.colorPrimary));
        items_delivery.setTextColor(getResources().getColor(R.color.lightblack));


        ll_type_car = rootView.findViewById(R.id.ll_type_car);
        ll_del_items = rootView.findViewById(R.id.ll_del_items);
        ll_desc = rootView.findViewById(R.id.ll_desc);
        ll_num_pasnger = rootView .findViewById(R.id.ll_num_pasnger);


        ed_delvery_items = rootView.findViewById(R.id.ed_delvery_items);
        ed_description = rootView.findViewById(R.id.ed_description);
        ed_num_passengers = rootView.findViewById(R.id.ed_num_passengers);
        ed_type_car = rootView.findViewById(R.id.ed_type_car);
        ed_add_comment = rootView.findViewById(R.id.ed_add_comment);

    }

    @Override
    public void onClick(View view)
    {
        Intent intent = new Intent();

        switch (view.getId())
        {


            case R.id.pass_del:
                pass_del.setBackgroundColor(getResources().getColor(R.color.light_yellow));
                items_delivery.setBackgroundColor(getResources().getColor(R.color.lightgrey));
                ll_del_items.setVisibility(View.GONE);
                ll_desc.setVisibility(View.GONE);
                ll_type_car.setVisibility(View.VISIBLE);
                ll_num_pasnger.setVisibility(View.VISIBLE);
                btn_send_details.setVisibility(View.GONE);
                btn_send_request.setVisibility(View.VISIBLE);

                pass_del.setTextColor(getResources().getColor(R.color.colorPrimary));
                items_delivery.setTextColor(getResources().getColor(R.color.lightblack));

                break;

            case R.id.items_delivery:
                items_delivery.setBackgroundColor(getResources().getColor(R.color.light_yellow));
                pass_del.setBackgroundColor(getResources().getColor(R.color.lightgrey));

                items_delivery.setTextColor(getResources().getColor(R.color.colorPrimary));
                pass_del.setTextColor(getResources().getColor(R.color.lightblack));

                ll_del_items.setVisibility(View.VISIBLE);
                ll_desc.setVisibility(View.VISIBLE);
                ll_type_car.setVisibility(View.GONE);
                ll_num_pasnger.setVisibility(View.GONE);

                btn_send_request.setVisibility(View.GONE);
                btn_send_details.setVisibility(View.VISIBLE);

                break;

            case R.id.btn_send_request:

                String st_num_passngers = ed_num_passengers.getText().toString();
                String st_type_car    = ed_type_car.getText().toString();
                String st_comment = ed_add_comment.getText().toString();

                if (st_pickLocation.equals("0"))
                {
                    pickup_location.setError(getString(R.string.pick_up_choose));
                }
                else if (st_dropLocation.equals("0"))
                {
                    drop_location.setError(getString(R.string.pick_up_choose));
                }
                else if (st_num_passngers.isEmpty())
                {
                    ed_num_passengers.setError(getString(R.string.enter_passengers));
                }
                else if (st_type_car.isEmpty())
                {
                 ed_type_car.setError(getString(R.string.enter_type_car));
                }
                else if (st_comment.isEmpty())
                {
                    ed_add_comment.setError(getString(R.string.comment_required));
                }
                else
                    if (networkInfo != null && networkInfo.isConnected())
                   {
                    /*intent = new Intent(getActivity(), RequestDetailsActivity.class);
                    startActivity(intent);*/
                    send_req_pass(st_num_passngers ,st_type_car,st_comment);
                  }
                else {
                      Toast.makeText(getActivity(),getString(R.string.please_check_your_network_connection),Toast.LENGTH_SHORT).show();
                    }

                break;
            case R.id.btn_send_request_details:

                String st_desc = ed_description.getText().toString();
                String st_del_items = ed_delvery_items.getText().toString();
                String st_item_comment = ed_add_comment.getText().toString();

                if (st_pickLocation.equals("0"))
                {
                    pickup_location.setError(getString(R.string.pick_up_choose));
                }
                else if (st_dropLocation.equals("0"))
                {
                    drop_location.setError(getString(R.string.pick_up_choose));
                }
                else if (st_del_items.isEmpty())
                {
                    ed_delvery_items.setError(getString(R.string.enter_delivery));
                }
                else if (st_desc.isEmpty())
                {
                    ed_description.setError(getString(R.string.enter_desc));
                }else if (st_item_comment.isEmpty())
                {
                    ed_add_comment.setError(getString(R.string.comment_required));
                }
                else if (networkInfo != null && networkInfo.isConnected())
                {

                    send_req_items(st_desc ,st_del_items,st_item_comment);
                  /*  intent = new Intent(getActivity(), DetailsRequest.class);
                    startActivity(intent);*/

                } else {
                    Toast.makeText(getActivity(),getString(R.string.please_check_your_network_connection),Toast.LENGTH_SHORT).show();
                }




                break;

            case R.id.img_pickup_loc:

                intent = new Intent(getActivity(), User_location_point.class);
                intent.putExtra("action_map", "1");
                intent.putExtra("Drop", User_location_point.isSelectPick);
                startActivityForResult(intent, 1);

                break;

            case R.id.img_drop_loc:
                intent = new Intent(getActivity(), User_location_point.class);
                intent.putExtra("action_map", "2");
                intent.putExtra("Drop", User_location_point.isPickDrop);
                startActivityForResult(intent, 2);
                break;


            case R.id.pickup_location:
                intent = new Intent(getActivity(), User_location_point.class);
                intent.putExtra("action_map", "1");
                intent.putExtra("Drop", User_location_point.isSelectPick);
                startActivityForResult(intent, 1);
                break;

            case R.id.drop_location:
                intent = new Intent(getActivity(), User_location_point.class);
                intent.putExtra("action_map", "1");
                intent.putExtra("Drop", User_location_point.isPickDrop);
                startActivityForResult(intent, 2);
                break;
        }
    }

    private void send_req_pass(String st_num_passngers, String st_type_car, String st_comment)
    {

            final API_Services services = Retrofit_fun.getClient().create(API_Services.class);
            Call<JsonElement> call_login = null;
            call_login = services.send_request(Constant_keys.API_KEY, langauge,stUserId ,st_pickLocation,
                    st_dropLocation,"","","1",st_num_passngers,st_type_car,st_pick_latitude,
                    st_pick_longitude,st_drop_latitude,st_drop_longitude, st_comment);
            final ProgressDialog progressDialog = new ProgressDialog(getActivity(), R.style.AppCompatAlertDialogStyle);
            progressDialog.setCancelable(false);
            progressDialog.setMessage(getResources().getString(R.string.please_wait));
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();

            call_login.enqueue(new Callback<JsonElement>() {
                @Override
                public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                    progressDialog.dismiss();
                    Log.e("sidfhi", response.body().toString());
                    if (response.isSuccessful()) {
                        try {
                            JSONObject jsonObject = new JSONObject(response.body().toString());

                            int status = jsonObject.getInt("status");
                            String message = jsonObject.getString("message");

                            if (status == 1)
                            {

                                Toast.makeText(getActivity(),message,Toast.LENGTH_SHORT).show();

                                JSONObject user_json = jsonObject.getJSONObject("req_details");
                                String request_id = user_json.getString("request_id");
                                  Intent  intent = new Intent(getActivity(), RequestDetailsActivity.class);
                                  intent.putExtra("reqID",request_id);
                                 intent.putExtra("req_type","1");
                                  startActivity(intent);



                            } else {
                                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Call<JsonElement> call, Throwable t) {
                    progressDialog.dismiss();
                    Log.e("error", call.toString());
                    Log.e("error", t.toString());
                }
            });


        }

    private void send_req_items(String desc, String delivery_items, String st_item_comment)
    {
        final API_Services services = Retrofit_fun.getClient().create(API_Services.class);
        Call<JsonElement> call_login = null;
        call_login = services.send_request(Constant_keys.API_KEY, langauge,stUserId ,st_pickLocation,
                st_dropLocation,desc,delivery_items,"2","","",st_pick_latitude,
                st_pick_longitude,st_drop_latitude,st_drop_longitude,st_item_comment);

        final ProgressDialog progressDialog = new ProgressDialog(getActivity(), R.style.AppCompatAlertDialogStyle);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(getResources().getString(R.string.please_wait));
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();
        call_login.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                progressDialog.dismiss();
                Log.e("dropk", response.body().toString());

                if (response.isSuccessful()) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().toString());

                        int status = jsonObject.getInt("status");
                        String message = jsonObject.getString("message");

                        if (status == 1)
                        {

                            Toast.makeText(getActivity(),message,Toast.LENGTH_SHORT).show();
                            JSONObject user_json = jsonObject.getJSONObject("req_details");
                            String request_id = user_json.getString("request_id");
                            Intent  intent = new Intent(getActivity(), RequestDetailsActivity.class);
                            intent.putExtra("reqID",request_id);
                            intent.putExtra("req_type","2");
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                        }
                        else if (status == 0)
                            {
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                progressDialog.dismiss();
                Log.e("error", call.toString());
                Log.e("error", t.toString());
            }
        });


    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1)
        {
            String message = data.getStringExtra("locationPick");
            pickup_location.setText(message);
            st_pickLocation = pickup_location.getText().toString();
            st_pick_latitude = String.valueOf(strlatitide);
            st_pick_longitude = String.valueOf(strlongitude);

        } else if (requestCode == 2) {
            String message = data.getStringExtra("locationPickDrop");
            drop_location.setText(message);
            st_dropLocation = drop_location.getText().toString();
            st_drop_latitude = String.valueOf(double_lat);
            st_drop_longitude = String.valueOf(double_lang);

        }
    }



    private void user_check_pending(String stUserId)
    {
        final API_Services services = Retrofit_fun.getClient().create(API_Services.class);
        Call<JsonElement> call_req_details = null;
        call_req_details = services.get_pending(Constant_keys.API_KEY,preferenceUtils.getStringFromPreference(PreferenceUtils.LANGUAGE,""), stUserId);
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setCancelable(false);
        progressDialog.setMessage(getResources().getString(R.string.loading));
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();
        call_req_details.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(@NonNull Call<JsonElement> call, @NonNull Response<JsonElement> response)
            {
                progressDialog.dismiss();
                Log.e("data_eror",response.body().toString());
                if (response.isSuccessful()) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().toString());

                        int status = jsonObject.getInt("status");

                        if (status == 1)
                        {
                           // String mesaage = jsonObject.getString("message");
                            //JSONObject json_data  = jsonObject.getJSONObject("request_details");
                            if (jsonObject.has("request_id"))
                            {
                                pending_request = jsonObject.getString("request_id");
                                pending_request_type = jsonObject.getString("pending_request_type");
                            }
                            else {
                                pending_request = "0";
                            }

                            if (!pending_request.equalsIgnoreCase("0"))
                            {
                                Intent  intent = new Intent(getActivity(), RequestDetailsActivity.class);
                                intent.putExtra("reqID",pending_request);
                                intent.putExtra("req_type",pending_request_type);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);

                            }
                            else {

                              /*  Intent a = new Intent(getActivity(), User_Home_activity.class);
                                Bundle bundle = new Bundle();
                                bundle.putString("Activity", "login");
                                a.putExtras(bundle);
                                //  a.putExtra("Activity","login");
                                startActivity(a);*/
                            }

                        }

                        else {

                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public void onFailure(@NonNull Call<JsonElement> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                Log.e("Error:" ,call.toString());
                Log.e("Eror:",t.toString());

            }
        });

    }

}