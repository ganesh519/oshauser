package com.volive.osha.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.volive.osha.R;
import com.volive.osha.helperclasses.Constant_keys;
import com.volive.osha.helperclasses.HelperClass;
import com.volive.osha.models.DriverImages;
import com.volive.osha.util_classes.PreferenceUtils;

import java.util.ArrayList;


public class Vechicle_ImagesAdapter extends RecyclerView.Adapter<Vechicle_ImagesAdapter.ViewHolder> {

    private ArrayList<DriverImages> arrayList;
    Context context;
    private PreferenceUtils preferenceUtils;
    private HelperClass helperClass;

    public Vechicle_ImagesAdapter(Context context, ArrayList<DriverImages> arrayListIssues) {
        this.context = context;
        this.arrayList = arrayListIssues;
        this.helperClass = new HelperClass(context);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.vechi_images_list, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        preferenceUtils = new PreferenceUtils(context);

      //  helperClass.loadImage(true, context, Constant_keys.imagebaseurl + arrayList.get(position).getImage_pic(), holder.certification_img);
        Log.e("onBindViewHolder: ", arrayList.get(position).getCar_id());

        Picasso.get().load(Constant_keys.imagebaseurl + arrayList.get(position).getCar_image()).
                placeholder(android.R.drawable.progress_indeterminate_horizontal)
                .error(R.drawable.car_front)
                .into(holder.img_car);

    }

   /* public void removeItem(int position){
        arrayList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, arrayList.size());
        notifyDataSetChanged();
    }
*/

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {



        ImageView img_car;

        public ViewHolder(View itemView) {
            super(itemView);


            img_car = itemView.findViewById(R.id.img_car);
        }
    }
}
