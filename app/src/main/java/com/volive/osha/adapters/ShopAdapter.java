package com.volive.osha.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.volive.osha.R;


public class ShopAdapter extends RecyclerView.Adapter<ShopAdapter.ViewHolder> {
    int[] myImageList;

    public ShopAdapter(int[] myImageList) {
        this.myImageList = myImageList;
    }
    @NonNull
    @Override
    public ShopAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.item_cardview, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ShopAdapter.ViewHolder holder, int position) {
        /*Picasso.with(holder.itemView.getContext())
                .load(myImageList[position])
                .into(holder.image);*/

        holder.image.setImageResource(myImageList[position]);
    }

    @Override
    public int getItemCount() {
        return myImageList.length;
    }
    class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView image;

        public ViewHolder(View itemView) {
            super(itemView);
            image = (ImageView) itemView.findViewById(R.id.image);
        }
    }
}
