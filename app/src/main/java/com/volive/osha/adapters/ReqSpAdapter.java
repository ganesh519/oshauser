package com.volive.osha.adapters;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.JsonElement;
import com.squareup.picasso.Picasso;
import com.volive.osha.R;
import com.volive.osha.activities.TrackActivity;
import com.volive.osha.helperclasses.Constant_keys;
import com.volive.osha.models.DriverImages;
import com.volive.osha.models.ReqSPModel_list;
import com.volive.osha.util_classes.API_Services;
import com.volive.osha.util_classes.PreferenceUtils;
import com.volive.osha.util_classes.Retrofit_fun;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by volive on 1/2/2019.
 */




public class ReqSpAdapter extends RecyclerView.Adapter<ReqSpAdapter.ViewHolder> {
    Context context;
    ArrayList<ReqSPModel_list> arrayList;
    PreferenceUtils preferenceUtils;
    String language ;
    Float float_rating;
    String provider_name,driver_image,req_price;
    ArrayList<DriverImages> driverImages = new ArrayList<>();
    Vechicle_ImagesAdapter vechicle_imagesAdapter;
    RecyclerView rec_driver_car_images;
    String rating_driver;

    public ReqSpAdapter(Context context, ArrayList<ReqSPModel_list> spModelArrayList) {
        this.context = context;
        this.arrayList = spModelArrayList;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView;

        itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.sp_list_item, parent, false);
        return new ViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        preferenceUtils =  new PreferenceUtils(context);
        language = preferenceUtils.getStringFromPreference(PreferenceUtils.LANGUAGE,"");
        holder.sp_name_txt.setText(arrayList.get(position).getSt_provider_name());
        holder.location_txt.setText(arrayList.get(position).getSt_driver_address());
        holder.txt_currency.setText(arrayList.get(position).getSt_offer_price() + " " + "SAR");


        final String req_id = arrayList.get(position).getSt_request_id();
        final String provider_id = arrayList.get(position).getSt_provider_id();
        final String offer_price = arrayList.get(position).getSt_offer_price();

        String rating = arrayList.get(position).getSt_provider_rating();
        float_rating =  Float.parseFloat(rating);

       if (rating.equals(""))
       {
           holder.rate_bar_fl_row.setRating(0);
       }
       else {

           holder.rate_bar_fl_row.setRating(float_rating);
       }

       // Glide.with(context).load(arrayList.get(position).getImage()).into(holder.profile_sp_img);

        Picasso.get().load(Constant_keys.imagebaseurl + arrayList.get(position).getSt_driver_pic())
                .placeholder(android.R.drawable.progress_indeterminate_horizontal)
                .error(R.drawable.guest_user)
                .into(holder.profile_sp_img);

        holder.itemView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                driver_image =  arrayList.get(position).getSt_driver_pic();
                provider_name  = arrayList.get(position).getSt_provider_name();
                req_price = arrayList.get(position).getSt_offer_price();
                 rating_driver = arrayList.get(position).getSt_provider_rating();
                dialog_provider_view(req_id,provider_id);

            }
        });

    }



    private void dialog_provider_view(final String req_id, final String provider_id)
    {

        final Dialog dialog = new Dialog(context, R.style.MyAlertDialogTheme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_layout);
        dialog.getWindow().setBackgroundDrawableResource(R.drawable.bg_dialog);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);
        dialog.show();
      // dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        ImageView dialog_cancel_img = dialog.findViewById(R.id.dialog_cancel_img);
        ImageView img_driver_dialog = dialog.findViewById(R.id.img_driver_dialog);
         rec_driver_car_images = dialog.findViewById(R.id.rec_driver_car_images);
        TextView txt_price_value = dialog.findViewById(R.id.txt_price_value);
        TextView txt_dis_value = dialog.findViewById(R.id.txt_dis_value);
        TextView txt_name_driver  = dialog.findViewById(R.id.txt_name_driver);
        RatingBar rate_bar_timing = dialog.findViewById(R.id.rate_bar_timing);
        view_details_provider(req_id,provider_id,txt_dis_value);
        Float float_rating =  Float.parseFloat(rating_driver);
        rate_bar_timing.setRating(float_rating);
          Picasso.get().load(Constant_keys.imagebaseurl + driver_image)
                .placeholder(android.R.drawable.progress_indeterminate_horizontal)
                .error(R.drawable.guest_user)
                .into(img_driver_dialog);

        txt_name_driver.setText(provider_name);
        txt_price_value.setText(req_price  + " "  + "SAR");
       // txt_dis_value.setText();
        rec_driver_car_images.setHasFixedSize(true);
        rec_driver_car_images.setLayoutManager(new LinearLayoutManager(context, LinearLayout.HORIZONTAL,false));
        Button btn_accept = dialog.findViewById(R.id.btn_accept);
        dialog_cancel_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        btn_accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                accept_offer(req_id ,provider_id,req_price);
            }
        });


    }

    private void view_details_provider(String req_id, String provider_id, final TextView txt_dis_value) {

     final  API_Services service  = Retrofit_fun.getClient().create(API_Services.class);
        Call<JsonElement> call_details = null;
        final ProgressDialog progressDialog = new ProgressDialog(context, R.style.AppCompatAlertDialogStyle);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(context.getResources().getString(R.string.please_wait));
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();
        call_details = service.provider_service_request_details(Constant_keys.API_KEY,language,req_id,provider_id);
        call_details.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                progressDialog.dismiss();
                if (response.isSuccessful())
                {
                    Log.e("sfdm",response.body().toString());
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().toString());
                        int status = jsonObject.getInt("status");
                        if (status == 1)
                        {
                            JSONObject json_data  = jsonObject.getJSONObject("request_details");
                            String distance = json_data.getString("distance");
                            txt_dis_value.setText(distance + context.getString(R.string.km));
                            //JSONObject driver_details = json_data.getJSONObject("driver_details");
                           // String distance = driver_details.getString("distance");

                            JSONArray images = jsonObject.getJSONArray("images");
                            driverImages = new ArrayList<>();
                            for (int i =0; i<images.length();i++)
                            {
                                JSONObject json_d =  images.getJSONObject(i);
                                DriverImages driver_pics = new DriverImages();
                                //image_type
                                driver_pics.setCar_id(json_d.getString("image_id"));
                                driver_pics.setCar_image(json_d.getString("image_name"));
                                driverImages.add(driver_pics);

                            }

                            vechicle_imagesAdapter = new Vechicle_ImagesAdapter(context,driverImages);
                            rec_driver_car_images.setAdapter(vechicle_imagesAdapter);

                        }

                        else
                        {
                            Toast.makeText(context,jsonObject.getString("message"),Toast.LENGTH_SHORT).show();
                        }



                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                progressDialog.dismiss();

            }
        });




    }

    private void accept_offer(final String req_id, String provider_id, String offer_price) {
        final API_Services service = Retrofit_fun.getClient().create(API_Services.class);
        Call<JsonElement> call_accept =  null;
        call_accept = service.accept_offer(Constant_keys.API_KEY,language,req_id,provider_id,offer_price);
        final ProgressDialog progressDialog = new ProgressDialog(context, R.style.AppCompatAlertDialogStyle);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(context.getResources().getString(R.string.please_wait));
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();
         call_accept.enqueue(new Callback<JsonElement>() {
           @Override
           public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {

               progressDialog.dismiss();
               if (response.isSuccessful())
               {
                   Log.e("ljsdf",response.body().toString());
                   try {
                       JSONObject json_object = new JSONObject(response.body().toString());

                       int status = json_object.getInt("status");
                       String message =  json_object.getString("message");
                       if (status == 1)
                       {
                           Toast.makeText(context,message,Toast.LENGTH_LONG).show();
                           Intent intent = new Intent(context, TrackActivity.class);
                           intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                           intent.putExtra("req_id",req_id);
                           intent.putExtra("action","0");
                           context.startActivity(intent);


                       }
                       else {
                           Toast.makeText(context,message,Toast.LENGTH_LONG).show();
                       }




                   }catch (Exception e)
                   {
                       e.printStackTrace();
                   }



               }



           }

           @Override
           public void onFailure(Call<JsonElement> call, Throwable t) {

           }
       });



    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView sp_name_txt, location_txt, txt_currency;
        ImageView profile_sp_img;
        RatingBar rate_bar_fl_row;

        public ViewHolder(View itemView) {
            super(itemView);
            profile_sp_img = itemView.findViewById(R.id.profile_sp_img);
            sp_name_txt = itemView.findViewById(R.id.sp_name_txt);
            location_txt = itemView.findViewById(R.id.location_txt);
            txt_currency = itemView.findViewById(R.id.txt_currency);
            rate_bar_fl_row = itemView.findViewById(R.id.rate_bar_fl_row);
        }
    }
}
