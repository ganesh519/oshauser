package com.volive.osha.adapters;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.volive.osha.R;
import com.volive.osha.activities.CreateInvoiceActivity;
import com.volive.osha.activities.InvoiceDetailsActivity;
import com.volive.osha.activities.OrderDetailsActivity;
import com.volive.osha.activities.TrackActivity;
import com.volive.osha.models.My_Req_Model;
import com.volive.osha.util_classes.PreferenceUtils;

import java.util.ArrayList;


/**
 * Created by volive on 1/2/2019.
 */

public class RequestAdapter extends RecyclerView.Adapter<RequestAdapter.ViewHolder> {
    Context context;
    int[] req_status_images;
    ArrayList<My_Req_Model> my_req_models;
    PreferenceUtils preferenceUtils;
    String language,stUserId;
     String st_request_status = "",st_payment_status = "";

    public RequestAdapter(Context context, ArrayList<My_Req_Model> my_req_models) {
        this.context = context;
        this.my_req_models=my_req_models;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView;

        itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.request_list_item, parent, false);
        return new ViewHolder(itemView);

    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position)
    {
        preferenceUtils = new PreferenceUtils(context);
        language = preferenceUtils.getStringFromPreference(PreferenceUtils.LANGUAGE,"");
        stUserId = preferenceUtils.getStringFromPreference(PreferenceUtils.USER_ID,"");
        My_Req_Model my_req_model =  new My_Req_Model();
        my_req_model = my_req_models.get(position);

        holder.txt_order_num.setText("#" + " " + my_req_models.get(position).getMy_req_order_ID());
        holder.txt_req_date.setText(my_req_models.get(position).getMy_req_date());
        holder.txt_req_drop.setText(my_req_models.get(position).getMy_req_drop());


         st_request_status = my_req_models.get(position).getMy_req_status();
        Log.e("a;kn",st_request_status);

        /*if (st_request_status.isEmpty())
        {
            holder.txt_req_status.setText(context.getString(R.string.pending));
            holder.img_request_status.setImageResource(R.drawable.list_pending);
            holder.btn_invoice.setText(context.getString(R.string.track_order));
            holder.btn_invoice.setVisibility(View.VISIBLE);
        }*/
         if (st_request_status.equals("0"))
        {
            holder.txt_req_status.setText(context.getString(R.string.pending));
            holder.img_request_status.setImageResource(R.drawable.list_pending);
           // holder.btn_invoice.setText(context.getString(R.string.track_order));
            holder.btn_invoice.setVisibility(View.GONE);
        }
        else if (st_request_status.equals("1"))
        {
            holder.txt_req_status.setText(context.getString(R.string.completed));
            holder.img_request_status.setImageResource(R.drawable.completed_list);
            holder.btn_invoice.setText(context.getString(R.string.view_invoice));
            st_payment_status = my_req_models.get(position).getMy_re_payment_status();
            if (st_payment_status.equals("1"))
            {
                holder.btn_invoice.setVisibility(View.GONE);
            }
            else {
                holder.btn_invoice.setVisibility(View.VISIBLE);
            }

        }
        else if (st_request_status.equals("2"))
        {
            holder.txt_req_status.setText(context.getString(R.string.in_progress));
            holder.img_request_status.setImageResource(R.drawable.list_pending);
           // holder.btn_invoice.setText(context.getString(R.string.view_details));
            holder.btn_invoice.setVisibility(View.GONE);
        }
        else if (st_request_status.equals("3"))
        {
            holder.txt_req_status.setText(context.getString(R.string.req_sts_cancel));
            holder.img_request_status.setImageResource(R.drawable.rejectimage);
            holder.btn_invoice.setVisibility(View.GONE);
        }


        /*if(position==0){
            holder.btn_invoice.setText("Track Order");
        }else if(position==1){
            holder.btn_invoice.setText("Give Rating");
        }else {
            holder.btn_invoice.setVisibility(View.INVISIBLE);
        }*/


        holder.btn_view_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                String  st_request_type = my_req_models.get(position).getMy_req_request_type();
                String st_order = my_req_models.get(position).getMy_req_order_ID();

                Intent intent = new Intent(context, OrderDetailsActivity.class);

                intent.putExtra("Activity", "Request");
                intent.putExtra("req_type",st_request_type);
                intent.putExtra("req_id",my_req_models.get(position).getMy_req_ID());
                intent.putExtra("order",st_order);
                intent.putExtra("req_sts",my_req_models.get(position).getMy_req_status());

                context.startActivity(intent);
            }
        });

        holder.btn_invoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                    String st_req_id = my_req_models.get(position).getMy_req_ID();
                    Intent intent = new Intent(context, InvoiceDetailsActivity.class);
                    intent.putExtra("req_id",st_req_id);
                    context.startActivity(intent);
            }
        });


    }

    @Override
    public int getItemCount() {
        return my_req_models.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        Button btn_invoice, btn_view_details;
        ImageView img_request_status;
        TextView txt_order_num ,txt_req_date,txt_req_status,txt_req_drop;

        public ViewHolder(View itemView) {
            super(itemView);
            btn_invoice = itemView.findViewById(R.id.btn_invoice);
            btn_view_details = itemView.findViewById(R.id.btn_view_details);
            img_request_status=itemView.findViewById(R.id.img_request_status);
            txt_order_num = itemView.findViewById(R.id.txt_order_num);
            txt_req_date = itemView.findViewById(R.id.txt_req_date);
            txt_req_status = itemView.findViewById(R.id.txt_req_status);
            txt_req_drop = itemView.findViewById(R.id.txt_req_drop);
        }
    }
}



















   /* final Dialog dialog = new Dialog(context, R.style.MyAlertDialogTheme);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.invoice_dialog);
                dialog.getWindow().setBackgroundDrawableResource(
                        R.drawable.bg_dialog);
                dialog.setCanceledOnTouchOutside(true);
                dialog.setCancelable(true);
                dialog.show();

//                dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

                Button invoice_btn = dialog.findViewById(R.id.invoice_btn);
                invoice_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();

                        Intent intent = new Intent(context, InvoiceDetailsActivity.class);
                        context.startActivity(intent);

                    }
                });*/

              /* if(position==0)
               {
                   Intent intent = new Intent(context, TrackActivity.class);
                   context.startActivity(intent);
               }else if(position==1){

               }*/
