package com.volive.osha.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.volive.osha.R;
import com.volive.osha.models.WalletModels;

import java.util.ArrayList;

/**
 * Created by volive on 7/23/2019.
 */

public class WalletAdapter extends RecyclerView.Adapter<WalletAdapter.ViewHolder> {
    Context context;
    ArrayList<WalletModels> wallet_models;

    public WalletAdapter(Context context, ArrayList<WalletModels> wallet_models) {
        this.context = context;
        this.wallet_models = wallet_models;
    }

    @NonNull
    @Override
    public WalletAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView;
        itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.wallet_adapter, viewGroup, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull WalletAdapter.ViewHolder viewHolder, int i)
    {
       // viewHolder.paid.setText(wallet_models.get(i).getPaid());
        viewHolder.order_no.setText("#" + " " +wallet_models.get(i).getWallet_order_id());
        viewHolder.dates.setText(wallet_models.get(i).getWallet_date());
        viewHolder.amount.setText(wallet_models.get(i).getWallet_amount());
        String type = wallet_models.get(i).getWallet_type();

        String wallet_amount = wallet_models.get(i).getWallet_amount();





        if (!type.isEmpty())
        {

        }

       // viewHolder.image.setImageResource(wallet_models.get(i).getImage());
    }

    @Override
    public int getItemCount() {
        return wallet_models.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView order_no, amount, dates, paid;
        ImageView image;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            image = itemView.findViewById(R.id.wallet_img);
            order_no = itemView.findViewById(R.id.wallet_order);
            amount = itemView.findViewById(R.id.wallet_amount);
            dates = itemView.findViewById(R.id.wallet_date);
            paid = itemView.findViewById(R.id.wallet_paid);

        }
    }
}
