package com.volive.osha.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.volive.osha.R;
import com.volive.osha.helperclasses.Constant_keys;
import com.volive.osha.models.ChatModel;
import com.volive.osha.util_classes.PreferenceUtils;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by volive on 11/16/2017.
 */

public class Chating_adapter extends RecyclerView.Adapter<Chating_adapter.MyViewHolder> {

    Activity context;
    int count;
    String userid, reciever_image, base_path, sender_image;
    PreferenceUtils preferenceUtils;
    ArrayList<ChatModel> chatModels = new ArrayList<>();

    public Chating_adapter(Activity chating_activity, ArrayList<ChatModel> chat_model, String user_id, String reciever_image) {

        this.context = chating_activity;
        this.chatModels = chat_model;
        this.userid = user_id;
        count = this.chatModels.size();
        this.reciever_image = reciever_image;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;

        itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_chat_right_view, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        preferenceUtils = new PreferenceUtils(context);
        base_path = preferenceUtils.getStringFromPreference(PreferenceUtils.BASE_URL, null);
        sender_image = preferenceUtils.getStringFromPreference(PreferenceUtils.IMAGE, sender_image);
        userid = preferenceUtils.getStringFromPreference(PreferenceUtils.USER_ID,"");
         Log.e("akdsjnikla",userid);
        if (chatModels.get(position).getSender_id().equalsIgnoreCase(userid))
        {
            holder.bubble_right.setVisibility(View.VISIBLE);
            holder.bubble_left.setVisibility(View.GONE);
            holder.message_right.setVisibility(View.VISIBLE);
            holder.message_left.setVisibility(View.GONE);
            holder.message_right.setText(chatModels.get(position).getMsg());
            holder.profile_pic_right.setVisibility(View.VISIBLE);
            holder.profile_pic_left.setVisibility(View.GONE);
            holder.date_right.setText(chatModels.get(position).getDte());

            if (preferenceUtils.getStringFromPreference(PreferenceUtils.IMAGE, "").isEmpty()) {
                holder.profile_pic_right.setImageResource(R.drawable.guest_user);

            } else {
                Glide.with(context).load(preferenceUtils.getStringFromPreference(PreferenceUtils.IMAGE, "")).into(holder.profile_pic_right);
                //validationClass.loadImage(true, HomeActivity.this, preferenceUtils.getStringFromPreference(PreferenceUtils.IMAGE,""), header_profile_pic);

            }

          /*  if (!sender_image.equalsIgnoreCase("") && !(sender_image == null)) {
                Glide.with(context)
                        .load(Constants.imagebaseurl + sender_image)
                        .into(holder.profile_pic_right);
            }*/

        } else {
            holder.bubble_right.setVisibility(View.GONE);
            holder.bubble_left.setVisibility(View.VISIBLE);
            holder.message_right.setVisibility(View.GONE);
            holder.message_left.setVisibility(View.VISIBLE);
            holder.message_left.setText(chatModels.get(position).getMsg());
            holder.profile_pic_left.setVisibility(View.VISIBLE);
            holder.profile_pic_right.setVisibility(View.GONE);
            holder.date_left.setText(chatModels.get(position).getDte());


           /* if (preferenceUtils.getStringFromPreference(PreferenceUtils.IMAGE, "").isEmpty()) {
                holder.profile_pic_left.setImageResource(R.drawable.headerimage);

            } else {
                Glide.with(context).load(preferenceUtils.getStringFromPreference(PreferenceUtils.IMAGE, "")).into(holder.profile_pic_left);
                //validationClass.loadImage(true, HomeActivity.this, preferenceUtils.getStringFromPreference(PreferenceUtils.IMAGE,""), header_profile_pic);

            }*/
            /*if (!sender_image.equalsIgnoreCase("") && !(sender_image == null)) {
                Glide.with(context)
                        .load(Constants.imagebaseurl + sender_image)
                        .into(holder.profile_pic_left);
            }*/
            if (reciever_image.isEmpty())
            {
                holder.profile_pic_left.setImageResource(R.drawable.guest_user);
            }
            else {
                Glide.with(context)
                        .load(Constant_keys.imagebaseurl + reciever_image)
                        .into(holder.profile_pic_left);

            }
          /*  if (!reciever_image.equalsIgnoreCase("") && !(reciever_image == null)) {

            }*/
        }
    }

    @Override
    public int getItemCount() {

        return count;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView message_left, message_right, date_left, date_right;
        ImageView image_left, image_right;
        RelativeLayout bubble_left, bubble_right;
        CircleImageView profile_pic_right, profile_pic_left;

        public MyViewHolder(View view) {
            super(view);
            image_left = (ImageView) view.findViewById(R.id.image_left);
            message_left = (TextView) view.findViewById(R.id.text_left);
            bubble_left = (RelativeLayout) view.findViewById(R.id.bubble_left);
            profile_pic_left = view.findViewById(R.id.profile_pic_left);

            image_right = (ImageView) view.findViewById(R.id.image_right);
            message_right = (TextView) view.findViewById(R.id.text_right);
            bubble_right = (RelativeLayout) view.findViewById(R.id.bubble_right);
            profile_pic_right = view.findViewById(R.id.profile_pic_right);
            date_left = view.findViewById(R.id.date_left);
            date_right = view.findViewById(R.id.date_right);

        }
    }

}

