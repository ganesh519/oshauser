package com.volive.osha.util_classes;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.util.Set;

public class PreferenceUtils

{
    private SharedPreferences preferences;
    private SharedPreferences.Editor edit;

    public static final String USER_ID = "user_id";
    public static String UserName = "user_name";
    public  static String User = "user";
    public static String Mobile = "phone_number";
    public static String ADD = "address";
    public static String BASE_URL = "base_path";
    public static String Email = "email";
    public static String PROFILE_IMAGE = "photo_id";
    public static  String Address = "address";
    public static String password_new = "password_new";
    public static  String Conf_password = "";
    public static String NOTIFICATION = "notification";
    public static final String OTP_STATUS = "otp_status";
    public static final String DOCUMENTS = "user_documents";
    public static final String ACTIVITYNAME = "Activity";
    public static final String LOGIN_TYPE = "logintype";
    public static final String SIGN_UP = "signup";

    public static final String authlevel = "authlevel";
    public static final String Role = "role";
    public static final String fb_login = "fb_login";
    public static final String gmail_login = "gmail";

    public static final String CITY = "city";
    public static final String Rating = "rating";
    public static String IMAGE = "photo_id";
    public static String LOG_IN = "login";
    public static String LOG_OUT = "logout";
    public static String issue_id = "issue_id";
    public  static String MY_PROFILE = "profile";


    public  static String active_level = "status";
    public static String acces_token = "token";


    public static String selected_Catid = "";
    public static String selected_sub_cat = "";

   public static  String image_save = "image";




    public static final String LANGUAGE = "en";
    public static final String DEVICE_ID = "device_id";

//    public static final String APP_RUN_FIRST_TIME = "KEY_SET_APP_RUN_FIRST_TIME";

    public static String getDeviceId() {
        return DEVICE_ID;
    }

    public PreferenceUtils(Context context) {
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
        edit = preferences.edit();
    }

    public void saveString(String strKey, String strValue) {
        edit.putString(strKey, strValue);
        edit.commit();
    }

    public void saveInt(String strKey, int value) {
        edit.putInt(strKey, value);
        edit.commit();
    }

    public void saveLong(String strKey, Long value) {
        edit.putLong(strKey, value);
        edit.commit();
    }

    public void saveFloat(String strKey, float value) {
        edit.putFloat(strKey, value);
        edit.commit();
    }

    public void saveDouble(String strKey, String value) {
        edit.putString(strKey, value);
        edit.commit();
    }

    public void saveBoolean(String strKey, boolean value) {
        edit.putBoolean(strKey, value);
        edit.commit();
    }

    public void removeFromPreference(String strKey) {
        edit.remove(strKey);
    }

    public String getStringFromPreference(String strKey, String defaultValue) {
        return preferences.getString(strKey, defaultValue);
    }

    public boolean getbooleanFromPreference(String strKey, boolean defaultValue) {
        return preferences.getBoolean(strKey, defaultValue);
    }
   /* public boolean getboolea(String strKey, boolean defaultValue) {
        return preferences.getBoolean(strKey, defaultValue);
    }*/
    public int getIntFromPreference(String strKey, int defaultValue) {
        return preferences.getInt(strKey, defaultValue);
    }

    public long getLongFromPreference(String strKey) {
        return preferences.getLong(strKey, 0);
    }

    public float getFloatFromPreference(String strKey, float defaultValue) {
        return preferences.getFloat(strKey, defaultValue);
    }

    public double getDoubleFromPreference(String strKey, double defaultValue) {
        return Double.parseDouble(preferences.getString(strKey, "" + defaultValue));
    }

    public String getSpinner(Set<String> strKey, String defaultValue) {
        return preferences.getString(String.valueOf(strKey), defaultValue);
    }

    public void logOut() {

        edit.clear();
        edit.commit();
//        setApp_runFirst("NO");


    }
    public void setProfileUri(String profile_uri) {
        edit.putString(MY_PROFILE,profile_uri);
        edit.commit();
    }

    public String getProfileUri(){

        return preferences.getString(MY_PROFILE,"");
    }

//    public String selectedLanguage() {
//        String Language = "";
//
//        if (preferences.getString(LANGUAGE, "0").equalsIgnoreCase("0")) {
//            Language = "ar";
//        } else {
//            Language = "en";
//
//        }
//        return Language;
//
//    }
//
    public String getLanguage() {

        return preferences.getString(LANGUAGE, "");
    }

    public void setLanguage(String language) {
        edit.putString(LANGUAGE, language);
        edit.commit();
    }

    public void setDeviceId(String DeviceId) {
        edit.putString(DEVICE_ID, DeviceId);
        edit.commit();
    }
}